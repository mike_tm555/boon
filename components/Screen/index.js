import React from "react";
import {
    KeyboardAvoidingView,
    Platform,
} from "react-native";
import {Text, View} from "../Themed";
import {SvgIcon} from "../SvgIcon";
import {Button} from "../Button";
import Assets from "../../constants/definitions/Assets";
import styles from "./styles";
import Gradient from "../Gradient";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

const Screen = (props) => {
    const {
        navigation,
        route,
        title,
        back,
        image,
        bottomComponent,
        rightComponent,
        leftComponent,
        style = {},
        align = 'center',
        description,
        children,
        ...otherProps
    } = props;

    const keyboardStyle = {...styles.keyboard, ...style.keyboard};
    const containerStyle = {...styles.container, ...style.container};
    const headerStyle = {...styles.header, ...style.header};
    const contentStyle = {...styles.content, ...style.content};
    const headerLayerStyle = {...styles.headerLayer, ...style.headerLayer};
    const headerArcStyle = {...styles.headerArc, ...style.headerArc};
    const headerWrapperStyle = {...styles.headerWrapper, ...style.headerWrapper};
    const headerBottomStyle = {...styles.headerBottom, ...style.headerBottom};
    const headerLeftSectionStyle = {...styles.headerLeftSection, ...style.headerLeftSection};
    const headerMiddleSectionStyle = {...styles.headerMiddleSSection, ...style.headerMiddleSSection};
    const headerRightSectionStyle = {...styles.headerRightSection, ...style.headerRightSection};
    const imageStyle = {...styles.image, ...style.image};
    const titleStyle = {...styles.title, ...style.title};
    const backStyle = {...styles.back, ...style.back};

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS !== "ios" ? "height" : "padding"}
            style={keyboardStyle}
        >
            <View
                style={containerStyle}
                {...otherProps}>
                <View style={headerStyle}>
                    <Gradient
                        colors={theme.headerLayer}
                        style={headerLayerStyle}/>
                    <View style={headerWrapperStyle}>
                        <SvgIcon
                            icon={Assets.patterns.arc}
                            style={headerArcStyle}
                        />
                        <View style={headerLeftSectionStyle}>
                            {back && (
                                <Button
                                    style={backStyle}
                                    type="simple"
                                    icon={{
                                        name: 'angle-left',
                                        type: 'font-awesome',
                                    }}
                                    onPress={() => {
                                        navigation.goBack(null);
                                    }}
                                />
                            )}
                            {leftComponent && (leftComponent)}
                        </View>
                        <View style={headerMiddleSectionStyle}>
                            {title && (
                                <Text type="h3" style={titleStyle}>{title}</Text>
                            )}
                            {image && (
                                <SvgIcon
                                    style={imageStyle}
                                    icon={image}
                                />
                            )}
                        </View>
                        <View style={headerRightSectionStyle}>
                            {rightComponent && (rightComponent)}
                        </View>
                    </View>
                    {bottomComponent && (
                        <View style={headerBottomStyle}>
                            {bottomComponent}
                        </View>
                    )}
                </View>
                <View style={contentStyle}>
                    {children}
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

export default Screen;
