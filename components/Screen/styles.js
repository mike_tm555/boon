import useTheme from "../../hooks/useTheme";
import Shadows from "../../constants/styles/Shadows";

const theme = useTheme();

export default {
    keyboard: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        backgroundColor: theme.screen
    },
    header: {
        width: '100%',
        justifyContent: 'center',
        position: 'relative',
        elevation: 5,
        zIndex: 5,
    },
    headerWrapper: {
        width: '100%',
        position: 'relative',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        zIndex: 11,
        elevation: 11,
    },
    headerBottom: {
        width: '100%',
        position: 'relative',
        zIndex: 10,
        elevation: 10,
    },
    headerLayer: {
        width: '30%',
        height: '100%',
        maxHeight: 250,
        top: 0,
        alignSelf: 'center',
        position: 'absolute',
        backgroundColor: theme.headerLayer,
        borderBottomLeftRadius: 200,
        borderBottomRightRadius: 200,
        transform: [
            {
                scaleX: 4
            },
        ],
        zIndex: 10,
        ...Shadows.dark
    },
    headerArc: {
        width: '100%',
        height: 60,
        top: 35,
        position: 'absolute',
    },
    headerLeftSection: {
        minWidth: 100,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    headerMiddleSSection: {
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    headerRightSection: {
        minWidth: 100,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    title: {
        height: 40,
        lineHeight: 40,
        marginTop: 60,
        marginBottom: 30,
        color: theme.headerTitle,
    },
    back: {
        width: 100,
        height: 60,
        size:  30,
        marginTop: 50,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 0,
        paddingVertical: 0,
        paddingHorizontal: 10,
        color: theme.headerTitle,
    },
    image: {
        width: 100,
        height: 50,
        marginTop: 60,
        marginBottom: 30,
        resizeMode: 'contain',
        position: 'relative',
    },
    content: {
        flex: 1,
        width: "100%",
        height: "100%",
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    }
};
