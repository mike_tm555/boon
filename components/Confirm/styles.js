import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    dialog: {
        maxWidth: 300,
    },
    dialogContent:  {
        width: '100%',
        padding: 30,
    },
    dialogButtons: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    dialogButton: {
        width: '50%',
        minWidth: 0,
        marginVertical: 0,
    },
    confirmButton: {
        backgroundColor: theme.actionButton.bg,
        borderColor: theme.actionButton.bg,
        color: theme.actionButton.color,
    },
    cancelButton: {

    }
}
