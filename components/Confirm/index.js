import React from "react";
import {View} from "../Themed";
import I18n from "../../utils/i18n";
import {Button} from "../Button";
import Dialog from "../Dialog";
import styles from "./styles";

export default function Confirm(props) {
    const {
        children,
        visible,
        close,
        cancel,
        confirm,
        cancelTitle,
        confirmTitle
    } = props;

    const closeDialog = () => {
        if (typeof close === 'function') {
            close();
        }
    }

    const cancelDialog = () => {
        closeDialog();

        if (typeof cancel === 'function') {
            cancel();
        }
    }

    const confirmDialog = () => {
        closeDialog();

        if (typeof confirm === 'function') {
            confirm();
        }
    }

    return (
        <Dialog
            visible={visible}
            close={close}
            onRequestClose={close}
            style={styles.dialog}
        >
            <View style={styles.dialogContent}>
                {children}
            </View>
            <View style={styles.dialogButtons}>
                <Button
                    style={{
                        ...styles.dialogButton,
                        ...styles.cancelButton
                    }}
                    type="square"
                    title={cancelTitle}
                    onPress={cancelDialog}
                />
                <Button
                    style={{
                        ...styles.dialogButton,
                        ...styles.confirmButton
                    }}
                    type="square"
                    title={confirmTitle}
                    onPress={confirmDialog}
                />
            </View>
        </Dialog>
    )
}
