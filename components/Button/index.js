import React from "react";
import {TouchableOpacity} from "react-native";
import {Icon, ImageBackground, Text} from "../Themed";
import styles from './styles';
import {SvgIcon} from "../SvgIcon";

export function Button(props) {
    const {
        style = {},
        title,
        disabled,
        iconColor,
        icon,
        type,
        ...otherProps
    } = props;
    let buttonStyle = {...styles.default, ...styles[type], ...style};

    let textStyle = {
        fontSize: buttonStyle.fontSize,
        fontWeight: buttonStyle.fontWeight,
        color: buttonStyle.color
    };

    let iconStyle = {
        size: buttonStyle.size || buttonStyle.fontSize,
        color: buttonStyle.color
    };

    if (title) {
        iconStyle.marginRight = 15;
    }

    let buttonIcon = null;

    if(icon) {
        buttonIcon = (
            <Icon
                style={iconStyle}
                color={iconStyle.color}
                size={iconStyle.size}
                type={icon.type}
                name={icon.name}/>
        )
    }

    if (disabled) {
        buttonStyle.opacity = 0.5;
    }

    return (
        <TouchableOpacity
            pointerEvents="none"
            style={buttonStyle}
            disabled={disabled}
            {...otherProps}>
            {buttonIcon}
            <Text style={textStyle}>{title}</Text>
        </TouchableOpacity>
    );
}

export function IconButton(props) {
    const { style = {}, title, iconColor, source, ...otherProps } = props;
    const buttonStyle = {...styles.icon, ...style};
    const iconStyle = {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        resizeMode: 'stretch',
        borderRadius: buttonStyle.borderRadius,
        overflow: 'hidden',
        backgroundColor: buttonStyle.backgroundColor,
    };

    return (
        <TouchableOpacity
            style={buttonStyle}
            {...otherProps}>
            <ImageBackground
                source={source}
                style={iconStyle}/>
        </TouchableOpacity>
    );
}

export function SvgButton(props) {
    const { style = {}, icon, ...otherProps } = props;
    const buttonStyle = {...styles.icon, ...style};
    const iconStyle = {
        width: style.width,
        height: style.height,
    };

    return (
        <TouchableOpacity
            style={buttonStyle}
            {...otherProps}>
            <SvgIcon
                fill={buttonStyle.color}
                icon={icon}
                style={iconStyle}
            />
        </TouchableOpacity>
    );
}
