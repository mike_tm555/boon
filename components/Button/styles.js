import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    default: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 50,
        paddingVertical: 12,
        paddingHorizontal: 20,
        marginVertical: 5,
        opacity: 1,
        size: 18,
        fontSize: 15
    },
    action: {
        minWidth: 200,
        backgroundColor: theme.actionButton.bg,
        color: theme.actionButton.color,
        opacity: 1,
        ...Shadows.default
    },
    dark: {
        minWidth: 200,
        backgroundColor: theme.darkButton.bg,
        color: theme.darkButton.color,
        ...Shadows.default
    },
    light: {
        minWidth: 200,
        backgroundColor: theme.lightButton.bg,
        color: theme.lightButton.color,
        ...Shadows.default
    },
    simple: {
        backgroundColor: 'transparent',
        paddingHorizontal: 0,
        borderRadius: 0,
        color: theme.simpleButton.color,
    },
    square: {
        borderRadius: 0,
        backgroundColor: 'transparent',
        paddingHorizontal: 0,
        borderWidth: 1,
        borderColor: theme.squareButton.border,
        color: theme.squareButton.color,
    },
    icon: {
        width: 70,
        height: 70,
        padding: 0,
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 100,
        position: 'relative',
        backgroundColor: theme.simpleButton.bg,
        ...Shadows.dark,
    }
};
