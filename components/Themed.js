import * as React from 'react';
import {
  Text as DefaultText,
  View as DefaultView,
  ImageBackground as DefaultImageBackground,
} from 'react-native';
import {
  Icon as DefaultIcon,
  Tooltip as DefaultTooltip,
} from 'react-native-elements';
import { BlurView as DefaultBlurView } from 'expo-blur';
import Typos from "../constants/styles/Typos";
import Tooltips from "../constants/styles/Tooltips";

export function ImageBackground(props) {
  const { style, imageStyle, source } = props;
  return (
      // @ts-ignore
      <DefaultImageBackground
          source={source}
          imageStyle={imageStyle}
          style={style}
      />
  );
}

export function Icon(props) {
  const { style, color = style.color, size = style.fontSize, ...otherProps } = props;
  // @ts-ignore
  return (
      <DefaultIcon
      style={style}
      color={color}
      size={size}
      {...otherProps}
      />
  )
}

export function Text(props) {
  const { style, type = 'p', ...otherProps } = props;
  // @ts-ignore
  return <DefaultText style={{...Typos[type], ...style}} {...otherProps} />;
}

export function View(props) {
  const { style, ...otherProps } = props;
  // @ts-ignore
  return <DefaultView style={style} {...otherProps} />;
}

export function BlurView(props) {
  const {
      style,
      children,
      intensity = 50,
      ...otherProps
  } = props;
  return (
      <View
          style={{
            width: style.width,
            height: style.height,
            backgroundColor: style.backgroundColor,
          }}
      >
        <DefaultBlurView
            intensity={intensity}
            style={style}
            {...otherProps}
        >
          {children}
        </DefaultBlurView>
      </View>
  );
}

export function Tooltip(props) {
  const {
    tooltipRef,
    style,
    type = 'simple',
    popover,
    children,
    ...otherProps
  } = props;
  // @ts-ignore
  return (
      <DefaultTooltip
          ref={tooltipRef}
          containerStyle={{...Tooltips[type], ...style}}
          withOverlay={false}
          withPointer={false}
          skipAndroidStatusBar={false}
          popover={popover}
          {...otherProps}
      >
        {children}
      </DefaultTooltip>
  )
}
