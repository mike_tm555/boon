import React, {useState} from "react";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { View } from "../Themed";
import Layout from "../../constants/styles/Layout";
import styles from "./styles";

const Slider = (props) => {
    const [activeSlide, setActiveSlide] = useState(props.active || 0);

    const {
        data,
        loop = true,
        space = Layout.content.space,
        pagination = true,
        swipeThreshold = 50,
        enableSnap = true,
        lockScrollWhileSnapping = true,
        hasParallaxImages = true,
        inactiveSlideScale = 0.9,
        inactiveSlideOpacity = 1,
        loopClonesPerSide = 2,
        inactiveSlideShift = 0,
        width = Layout.window.width,
        itemWidth = width - space * 2,
        layout = 'default',
        autoplay,
        renderItem,
        changeSlide,
        ...otherProps
    } = props;

    const onSnapToItem = (index) => {
        setActiveSlide(index);

        if (typeof changeSlide === 'function') {
            changeSlide(index);
        }
    };

    return (
        <View style={styles.wrapper}>
            <Carousel
                data={data}
                loop={loop}
                enableSnap={enableSnap}
                swipeThreshold={swipeThreshold}
                lockScrollWhileSnapping={lockScrollWhileSnapping}
                hasParallaxImages={hasParallaxImages}
                contentContainerCustomStyle={styles.container}
                loopClonesPerSide={loopClonesPerSide}
                inactiveSlideScale={inactiveSlideScale}
                inactiveSlideOpacity={inactiveSlideOpacity}
                inactiveSlideShift={inactiveSlideShift}
                sliderWidth={width}
                itemWidth={itemWidth}
                layout={layout}
                autoplay={autoplay}
                renderItem={renderItem}
                onSnapToItem={onSnapToItem}
                slideStyle={styles.slide}
                {...otherProps}
            />
            {(pagination) && (
                <Pagination
                    dotsLength={data.length}
                    activeDotIndex={activeSlide}
                    containerStyle={styles.pagination}
                    dotStyle={styles.activeDot}
                    inactiveDotStyle={styles.dot}
                    inactiveDotOpacity={0.8}
                    inactiveDotScale={0.8}
                />
            )}
        </View>
    )
};

export default Slider;
