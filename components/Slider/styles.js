import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    wrapper: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        position: 'relative',
        paddingBottom: 10,
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: theme.dot,
    },
    activeDot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: theme.activeDot,
    },
    pagination: {
        marginTop: -5,
    }
}
