export default {
    generatorSheet: {
        width: '100%',
        position: 'relative',
        marginVertical: 10,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    generator: {
        width: '100%',
        justifyContent: 'center',
    },
    generatorIcon: {
        width: 150,
        height: 150,
        marginVertical: 10,
        alignSelf: 'center',
    },
    body: {
        marginBottom: 20
    },
    footer: {
        paddingVertical: 10
    }
};
