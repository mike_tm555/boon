import React, {useState} from "react";
import { Share } from "react-native";
import {ImageBackground, Text, View} from "../Themed";
import Sheet from "../Sheet";
import styles from "./styles";
import Assets from "../../constants/definitions/Assets";
import RotateAnimate from "../Animations/Rotate";
import {Button} from "../Button";
import I18n from '../../utils/i18n';
import CodeInput from "../Fields/CodeInput";
import {TOAST_TYPES, Toaster} from "../Toasts/toaster";

const Generator = (props) => {
    const {
        title,
        description,
        code,
        generate,
        share,
        generatorRef,
    } = props;

    const [generating, setGenerating] = useState(false);

    const generateCode = () => {
        if (typeof generate === 'function') {
            setGenerating(true);
            generate().then((response) => {
                setGenerating(false);
            })
        }
    };

    const shareCode = async () => {
        try {
            const result = await Share.share({
                message: I18n.t('intercomCode', {
                    code: code
                }),
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    generatorRef.current?.close();
                } else {
                    generatorRef.current?.close();
                }
            }
        } catch (error) {
            Toaster.show({
                message: error.message,
                type: TOAST_TYPES.error,
            });
        }
    };

    return (
        <Sheet
            sheetRef={generatorRef}>
            <View style={styles.generatorSheet}>
                {title && (
                    <Text type="h1">{title}</Text>
                )}
                {description && (
                    <Text type="p">{description}</Text>
                )}
                <View style={styles.generator}>
                    <RotateAnimate
                        active={generating}
                    >
                        <ImageBackground
                            source={Assets.icons.generator}
                            style={styles.generatorIcon}
                        />
                    </RotateAnimate>
                </View>
                <View style={styles.body}>
                    <CodeInput length={6} value={code}/>
                </View>
                <View style={styles.footer}>
                    {code ? (
                        <Button
                            type="action"
                            icon={{
                                name: 'share',
                                type: 'material'
                            }}
                            title={I18n.t('shareCode')}
                            onPress={shareCode}
                        />
                    ) : (
                        <Button
                            type="action"
                            title={I18n.t('generateCode')}
                            onPress={generateCode}
                        />
                    )}
                </View>
            </View>
        </Sheet>
    )
};

export default Generator;
