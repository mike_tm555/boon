import {SvgXml} from "react-native-svg";
import * as React from "react";
import useTheme from "../hooks/useTheme";

const theme = useTheme();

export function SvgIcon(props) {
    const { style, icon } = props;

    const colors = {
        primary: theme.primary,
        secondary: theme.secondary,
    };

    if (props.fill) {
        colors.fill = props.fill;
    }

    let xmlIcon = icon;

    xmlIcon = xmlIcon.replace(/colors.fill/g, colors.fill);
    xmlIcon = xmlIcon.replace(/colors.primary/g, colors.primary);
    xmlIcon = xmlIcon.replace(/colors.secondary/g, colors.secondary);

    return (
        <SvgXml
            xml={xmlIcon}
            style={style}
            width={style.width}
            height={style.height}
        />
    );
}
