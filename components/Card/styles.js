import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";
const theme = useTheme();

export default {
    container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        backgroundColor: theme.content,
        borderRadius: 10,
        ...Shadows.mild,
    },
    title: {
        color: theme.text,
        paddingVertical: 5,
    },
    icon: {
        width: 50,
        height: 50,
        fill: theme.text
    }
}
