import React from "react";
import {View} from "../Themed";
import {TouchableOpacity} from "react-native";
import {Text} from "../Themed";
import {SvgIcon} from "../SvgIcon";
import styles from "./styles";

const Card = (props) => {
    const {
        navigation,
        style = {},
        type,
        title,
        icon,
        ...otherProps
    } = props;

    return (
        <TouchableOpacity
            {...otherProps}
            style={styles.container}
            onPress={() =>{
                props.onPress(navigation);
            }}>
            <SvgIcon
                icon={icon}
                style={styles.icon}
                fill={styles.icon.fill}
            />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
    )
};

export default Card;
