import * as React from "react";
import RBSheet from "react-native-raw-bottom-sheet";
import {Text} from "../Themed";
import styles from "./styles";

export default function Index(props) {
    const {
        sheetRef,
        title,
        children,
        ...otherProps
    } = props;

    return (
        // @ts-ignore
        <RBSheet ref={sheetRef}
                 closeDuration={0}
                 closeOnDragDown={true}
                 closeOnPressMask={true}
                 customStyles={styles}
                 {...otherProps}>
            {title && (
                <Text style={styles.title}>{title}</Text>
            )}
            {children}
        </RBSheet>
    )
}
