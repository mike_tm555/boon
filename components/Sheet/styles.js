import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    overlay: {
        backgroundColor: 'rgba(0,0,0,0.2)',
        flex: 1,
        justifyContent: 'flex-end',
    },
    container: {
        width: '100%',
        height: 'auto',
        backgroundColor: 'white',
        paddingTop: 10,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
    },
    draggableIcon: {
        width: 50,
        backgroundColor: theme.draggable
    },
    title: {
        marginTop: 10,
        fontSize: 16,
        backgroundColor: 'transparent',
        alignSelf: 'center',
    }
}
