import Layout from "../../constants/styles/Layout";
import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    container: {
        width: '100%',
        backgroundColor: theme.content,
        padding: Layout.listItem.padding,
        borderRadius: Layout.listItem.borderRadius,
        marginBottom: Layout.listItem.marginBottom,
        ...Shadows.mild,
    }
}
