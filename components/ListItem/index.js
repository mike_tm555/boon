import React from "react";
import { TouchableOpacity } from "react-native";
import styles from "./styles";
import {View} from "../Themed";

const ListItem = ({ children, style, press }) => {
    const onPress = () => {
        if (typeof press === 'function') {
            press();
        }
    };

    if (press) {
        return (
            <TouchableOpacity
                disabled={(!!!press)}
                style={{
                    ...styles.container,
                    ...style
                }}
                onPress={onPress}
            >
                {children}
            </TouchableOpacity>
        )
    }

    return (
        <View
            style={{
                ...styles.container,
                ...style
            }}
        >
            {children}
        </View>
    )
};

export default ListItem;
