import React from "react";
import {connect} from "react-redux";
import {View} from "../Themed";
import styles from "./styles";
import Toast from "./toast";

const Toasts = ({ toasts }) => {
    if (toasts.length) {
        return (
            <View style={styles.container}>
                {toasts.map((item) => {
                    return (
                        <Toast
                            key={item.id}
                            {...item}
                        />
                    );
                })}
            </View>
        )
    }

    return null;
};

const mapStateToProps = (state) => {
    return {
        toasts: state.toasts,
    };
};

export default connect(mapStateToProps)(Toasts);
