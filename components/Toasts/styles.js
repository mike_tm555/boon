import useTheme from "../../hooks/useTheme";
import Shadows from "../../constants/styles/Shadows";

const theme = useTheme();

export default {
    container: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        padding: 20,
        flexDirection: 'column',
    },
    toast: {
        width: '100%',
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        borderRadius: 10,
        padding: 5,
        overflow: 'hidden',
        ...Shadows.mild,
    },
    iconBox: {
        width: 40,
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    info: {
        flex: 1,
        width: '100%',
        height: '100%',
        paddingLeft: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    action: {
        width: 40,
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: 30,
        size: 30,
    },
    title: {
        width: '100%',
        padding: 5,
        textAlign: 'left',
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: 13,
    },
    message: {
        width: '100%',
        padding: 5,
        textAlign: 'left',
        marginBottom: 0,
        fontSize: 13
    },
    button: {
        width: 25,
        height: 25,
        size: 14,
        borderRadius: 30,
        paddingVertical: 0,
        paddingHorizontal: 0,
        marginVertical: 0,
    },
    success: {
        toast: {
            backgroundColor: theme.toast.success,
        },
        icon: {
            color: theme.lightText,
        },
        title: {
            color: theme.lightText,
        },
        message: {
            color: theme.lightText,
        },
        button: {
            color: theme.lightText
        }
    },
    error: {
        toast: {
            backgroundColor: theme.toast.error,
        },
        icon: {
            color: theme.lightText,
        },
        title: {
            color: theme.lightText,
        },
        message: {
            color: theme.lightText,
        },
        button: {
            color: theme.lightText
        }
    }
}
