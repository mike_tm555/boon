import React from "react";
import {Icon, Text, View} from "../Themed";
import styles from "./styles";
import {Button} from "../Button";
import {Toaster} from "./toaster";
import SlideAnimate from "../Animations/Slide";

export default function Toast(props) {
    const {
        id,
        type,
        title,
        message
    } = props;

    const hideToast = (id) => {
        Toaster.hide(id);
    };

    const specificStyles = styles[type.name];

    return (
        <SlideAnimate>
            <View style={{
                ...styles.toast,
                ...specificStyles.toast
            }}>
                <View style={styles.iconBox}>
                    <Icon
                        color={specificStyles.icon.color}
                        size={styles.icon.size}
                        type='font-awesome'
                        name={type.icon}/>
                </View>
                <View style={styles.info}>
                    {title && (
                        <Text type="h4" style={{
                            ...styles.title,
                            ...specificStyles.title
                        }}>
                            {title}
                        </Text>
                    )}
                    {message && (
                        <Text type="title" style={{
                            ...styles.message,
                            ...specificStyles.message
                        }}>
                            {message}
                        </Text>
                    )}
                </View>
                <View style={styles.action}>
                    <Button
                        style={{
                            ...styles.button,
                            ...specificStyles.button
                        }}
                        type="simple"
                        icon={{
                            name: 'remove',
                            type: 'font-awesome'
                        }}
                        onPress={() => {
                            hideToast(id);
                        }}
                    />
                </View>
            </View>
        </SlideAnimate>
    )
}
