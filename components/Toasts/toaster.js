import Store from '../../store';
import {addToast, deleteToast} from "../../store/actions";

export const Toaster = {
    delay: 5000,
    timers: {},
    show: (toast) => {
        const toastData = {
            id: ((new Date()).getTime()).toString(),
            ...toast,
        };

        Store.dispatch(addToast([toastData]));

        Toaster.timers[toastData.id] = setTimeout(() => {
            Toaster.hide(toastData.id);
        }, toast.delay || Toaster.delay);
    },
    hide: (id) => {
        Store.dispatch(deleteToast(id));

        if (Toaster.timers[id]) {
            clearTimeout(Toaster.timers[id]);
        }
    }
};

export const TOAST_TYPES = {
    success: {
        name: 'success',
        icon: 'check-circle'
    },
    error: {
        name: 'error',
        icon: 'exclamation-circle'
    }
};
