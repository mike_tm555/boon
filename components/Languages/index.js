import React, {useState} from "react";
import I18n from "../../utils/i18n";
import {View} from "../Themed";
import {IconButton} from "../Button";
import Sheet from "../Sheet";
import styles from "./styles";

const Languages = (props) => {
    const {
        langRef,
        change,
        languages
    } = props;

    const [language, setLanguage] = useState(I18n.locale);

    const selectLanguage = (lang) => {
        I18n.changeLanguage(lang);
        setLanguage(lang);
        langRef.current?.close();

        if (typeof change === 'function') {
            change(lang);
        }
    };

    return (
        <Sheet
            title={I18n.t("selectLanguage")}
            sheetRef={langRef}>
            <View style={styles.languageSheet}>
                {Object.keys(languages).map(key => {
                    const lang = languages[key];
                    const buttonStyle = {...styles.languageButton};

                    if (lang.key === language) {
                        buttonStyle.opacity = 0.5;
                    }

                    return (
                        <IconButton
                            key={lang.key}
                            style={buttonStyle}
                            source={lang.image}
                            onPress={() => {
                                selectLanguage(lang.key);
                            }}
                        />
                    );
                })}
            </View>
        </Sheet>
    )
};

export default Languages;
