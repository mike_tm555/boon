export default {
    languageSheet: {
        marginVertical: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
    languageButton: {
        width: 40,
        height: 40,
        marginHorizontal: 15,
    }
};
