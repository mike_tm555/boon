import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    dialog: {
        maxWidth: 300,
        overflow: 'hidden',
    },
    dialogContent:  {
        width: '100%',
        padding: 30,
    },
    dialogButtons: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    dialogButton: {
        width: '100%',
        minWidth: 0,
        marginVertical: 0,
        borderWidth: 0,
        color: theme.actionButton.color,
        backgroundColor: theme.actionButton.bg,
    },
}
