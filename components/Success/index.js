import React from "react";
import {View} from "../Themed";
import I18n from "../../utils/i18n";
import {Button} from "../Button";
import styles from "./styles";
import Dialog from "../Dialog";

export default function Success(props) {
    const {
        children,
        visible,
        buttonTitle,
        close,
    } = props;

    const closeDialog = () => {
        if (typeof close === 'function') {
            close();
        }
    }

    return (
        <Dialog
            visible={visible}
            close={close}
            onRequestClose={close}
            style={styles.dialog}
        >
            <View style={styles.dialogContent}>
                {children}
            </View>
            <View style={styles.dialogButtons}>
                <Button
                    style={styles.dialogButton}
                    type="square"
                    title={buttonTitle}
                    onPress={closeDialog}
                />
            </View>
        </Dialog>
    )
}
