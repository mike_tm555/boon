import React, { useState } from 'react';
import {Alert, StyleSheet} from 'react-native';
import * as Facebook from 'expo-facebook';
import I18n from "../utils/i18n";
import {Button} from "./Button";

console.disableYellowBox = true;
const APP_ID = '318977712734692';
export default function FBLoginButton(props) {

    const facebookLogIn = async () => {
        try {
            await Facebook.initializeAsync(APP_ID);

            const {
                type,
                token,
            } = await Facebook.logInWithReadPermissionsAsync(APP_ID, {
                permissions: ['public_profile', 'email', 'phone'],
            });
            if (type === 'success') {
                fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`)
                    .then(response => response.json())
                    .then(data => {
                        props.onPress(data, token);
                    })
                    .catch(e => console.log(e))
            } else {

            }
        } catch ({message}) {
            Alert.alert('Facebook Login Error', `${message}`);
        }
    };

    return (
        <Button
            type="light"
            icon={{
                name: 'facebook',
                type: 'font-awesome'
            }}
            title={I18n.t('continueFB')}
            onPress={() => {
                facebookLogIn()
            }}
            style={styles.button}
        />
    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#5890FF',
        color: '#ffffff'
    }
});

