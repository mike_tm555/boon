import React, {useState} from "react";
import {Text} from "../Themed";
import {TouchableOpacity, StyleSheet} from "react-native";
import Inputs from "../../constants/styles/Inputs";
import {SvgIcon} from "../SvgIcon";
import Assets from "../../constants/definitions/Assets";
import useTheme from "../../hooks/useTheme";
import I18n from '../../utils/i18n';

const theme = useTheme();

export default function (props) {
    const [value, setValue] = useState(props.value);

    const valueChange = () => {
        setValue(value);

        if (typeof props.onChange === 'function') {
            props.onChange(value);
        }
    };

    return (
        <TouchableOpacity
            onPress={valueChange}
            style={styles.container}
        >
            {(value) ? (
                <SvgIcon
                    style={styles.icon}
                    icon={Assets.locks.unlocked}
                />
            ) : (
                <SvgIcon
                    style={styles.icon}
                    icon={Assets.locks.locked}
                />
            )}

            <Text
                type="caption"
                style={styles.label}
            >
                {(value) ? I18n.t('close') : I18n.t('open')}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 2,
        borderColor: theme.borderButton.border,
        backgroundColor: theme.borderButton.bg,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        width: 50,
        height: 25,
        marginBottom: 5,
    },
    label: {
        fontSize: Inputs.field.fontSize,
        color: theme.borderButton.color,
    }
});
