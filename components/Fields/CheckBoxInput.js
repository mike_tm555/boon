import React, {useState} from "react";
import {View} from "../Themed";
import { CheckBox } from 'react-native-elements';
import {StyleSheet} from "react-native";
import Inputs from "../../constants/styles/Inputs";
import useErrorColorScheme from "../../hooks/useErrorColorScheme";

export default function (props) {
    const {
        error,
    } = props;
    const [checked, setChecked] = useState(props.value);

    const onChange = () => {
        setChecked(!checked);
        if (typeof props.onChange === 'function') {
            props.onChange(!checked);
        }
    };

    const errorStyles = useErrorColorScheme(error);

    return (
        <CheckBox
            containerStyle={styles.container}
            textStyle={{
                ...styles.label,
                ...errorStyles
            }}
            checked={checked}
            style={styles.input}
            onPress={onChange}
            {...props}/>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        height: Inputs.field.height,
        borderWidth: 0
    },
    label: {
        fontSize: Inputs.field.fontSize,
        color: Inputs.field.color,
        fontWeight: '300',
        lineHeight: 20,
    }
});
