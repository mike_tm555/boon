import React, {useState} from "react";
import Colors from "../../constants/styles/Colors";
import {Switch} from "react-native-switch";
import {Text} from "../Themed";
import {TouchableOpacity, StyleSheet} from "react-native";
import Inputs from "../../constants/styles/Inputs";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default function (props) {
    const [value, setValue] = useState(props.value);

    const valueChange = () => {
        setValue(!value);

        if (typeof props.onChange === 'function') {
            props.onChange(!value);
        }
    };

    return (
        <TouchableOpacity
            onPress={valueChange}
            style={styles.container}
        >
            {props.title && (
                <Text style={styles.label}>{props.title}</Text>
            )}
            <Switch
                activeText={props.activeText ?? null}
                inActiveText={props.inActiveText ?? null}
                circleSize={props.circleSize ?? 28}
                barHeight={props.circleSize ? props.circleSize + 2 : 30}
                switchLeftPx={props.switchLeftPx ??  3}
                switchRightPx={props.switchRightPx ?? 3}
                circleBorderWidth={props.circleBorderWidth ?? 1}
                switchBorderRadius={props.activeText ?? 20}
                backgroundActive={props.backgroundActive ?? theme.primary}
                backgroundInactive={props.backgroundInactive ?? theme.tabIconDefault}
                circleActiveColor={props.circleActiveColor ?? theme.content}
                circleInActiveColor={props.circleInActiveColor ?? theme.content}
                onValueChange={valueChange}
                value={props.value}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: Inputs.field.height,
    },
    label: {
        width: 'auto',
        fontSize: Inputs.field.fontSizeLarge,
        color: Inputs.field.labelColor,
        marginRight: 10
    }
});
