import React, {useState} from "react";
import {TextInput, StyleSheet} from "react-native";
import Inputs from "../../constants/styles/Inputs";
import useErrorColorScheme from "../../hooks/useErrorColorScheme";

export default function (props) {
    const { placeholder, error, onChange } = props;
    const [value, setValue] = useState(props.value);

    const onChangeText = (text) => {
        setValue(text);

        if (typeof onChange === 'function') {
            onChange(text);
        }
    };

    const errorStyles = useErrorColorScheme(error);

    return (
        <TextInput
            style={{
                ...styles.input,
                ...errorStyles,
            }}
            onChangeText={onChangeText}
            placeholder={placeholder}
            keyboardType="email-address"
            textContentType="emailAddress"
            autoCompleteType="email"
            multiline={false}
            value={value}
        />
    )
}

const styles = StyleSheet.create({
    input: {
        width: '100%',
        borderBottomWidth: Inputs.field.borderWidth,
        borderColor: Inputs.field.borderColor,
        height: Inputs.field.height,
        fontSize: Inputs.field.fontSize,
    }
});
