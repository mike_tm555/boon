import React, {useState} from "react";
import I18n from '../../utils/i18n';
import IntlPhoneInput from 'react-native-intl-phone-input';
import {
    FlatList,
    TouchableWithoutFeedback, StyleSheet
} from 'react-native';
import {View, Text} from "../Themed";
import Modal from "../Modal";
import Inputs from "../../constants/styles/Inputs";
import useErrorColorScheme from "../../hooks/useErrorColorScheme";
import Search from "../Search";
import {Button} from "../Button";
import Content from "../Content";
import useTheme from "../../hooks/useTheme";

const  theme = useTheme();

class PhoneInput extends React.Component {
    phoneInput;

    constructor(props) {
        super(props);
        this.state = {};
    }

    renderCustomModal = (modalVisible, countries, onCountryChange) => {
        return (
            <Modal
                visible={modalVisible}
                header={(
                    <View style={styles.modalHeader}>
                        <Search
                            placeholder={I18n.t('search')}
                            search={(e) => {
                                this.setState({
                                    searchText: e,
                                })
                            }}
                        />
                    </View>
                )}
                footer={(
                    <Button
                        type="action"
                        style={styles.closeButtonStyle}
                        title={I18n.t('close')}
                        onPress={() => {
                            this.phoneInput.hideModal()
                        }}
                    />
                )}
            >
                <Content>
                    <FlatList
                        style={styles.flatList}
                        data={countries.filter((item) => {
                            if (!this.state.searchText) {
                                return true;
                            }
                            return (item.en.indexOf(this.state.searchText) > -1);
                        })}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={
                            ({ item }) => (
                                <TouchableWithoutFeedback onPress={() => onCountryChange(item.code)}>
                                    <View style={[styles.countryModalStyle]}>
                                        <Text style={[styles.modalFlagStyle]}>{item.flag}</Text>
                                        <View style={styles.modalCountryItemContainer}>
                                            <Text style={[styles.modalCountryItemCountryNameStyle]}>{item.en}</Text>
                                            <Text style={[styles.modalCountryItemCountryDialCodeStyle]}>{item.dialCode}</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        }
                    />
                </Content>
            </Modal>
        )
    };

    onChange(value) {
        if (typeof this.props.onChange === 'function') {
            this.props.onChange(value)
        }
    }

    render() {
        const { error } = this.props;
        const errorStyles = useErrorColorScheme(error);

        return (
            <IntlPhoneInput
                ref={(ref) => this.phoneInput = ref}
                customModal={this.renderCustomModal}
                flagStyle={styles.flagStyle}
                containerStyle={styles.container}
                dialCodeTextStyle={styles.dialCodeTextStyle}
                phoneInputStyle={{
                    ...styles.phoneInputStyle,
                    ...errorStyles
                }}
                onChangeText={(value) => {
                    this.onChange({
                        dialCode: value.dialCode,
                        phoneNumber: value.phoneNumber.replace(" ", ""),
                    });
                }}
                {...this.props}
            />
        );
    }
}

const styles = StyleSheet.create({
    closeTextStyle: {
        padding: 5,
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold'
    },
    modalHeader: {
        width: '100%',
        paddingVertical: 5,
        paddingHorizontal: 20,
        backgroundColor: theme.primary,
    },
    modalCountryItemCountryDialCodeStyle: {
        fontSize: 15
    },
    modalCountryItemCountryNameStyle: {
        flex: 1,
        fontSize: 15
    },
    modalCountryItemContainer: {
        flex: 1,
        paddingLeft: 5,
        flexDirection: 'row'
    },
    flatList: {
        flex: 1,
    },
    modalFlagStyle: {
        fontSize: 20,
        marginRight: 10,
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    flagStyle: {
        display: 'none',
        height: Inputs.field.height,
        lineHeight: Inputs.field.height,
        textAlign: 'center',
        fontSize: Inputs.icon.size,
        backgroundColor: '#f1f1f1',
    },
    dialCodeTextStyle: {
        padding: 0,
        marginRight: 10,
        height: Inputs.field.height,
        lineHeight: Inputs.field.lineHeight,
        borderBottomWidth: Inputs.field.borderWidth,
        borderColor: Inputs.field.borderColor,
        fontSize: Inputs.field.fontSize,
        alignContent: "center",
        justifyContent: "center",
    },
    countryModalStyle: {
        flex: 1,
        borderColor: '#f1f1f1',
        borderTopWidth: 1,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    openDialogView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    filterInputStyle: {
        flex: 1,
        backgroundColor: '#fff',
        color: Inputs.field.color,
    },
    phoneInputStyle: {
        width: '100%',
        height: Inputs.field.height,
        fontSize: Inputs.field.fontSize,
        color: Inputs.field.color,
        flex: 1,
        borderColor: Inputs.field.borderColor,
        borderBottomWidth: Inputs.field.borderWidth,
        alignContent: "center",
        justifyContent: "center",
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        paddingHorizontal: 0,
    },
    searchIconStyle: {
        color: 'black',
        fontSize: 15,
        marginLeft: 15
    },
    buttonStyle: {
        alignItems: 'center',
        padding: 14,
        marginBottom: 10,
        borderRadius: 3,
    },
    buttonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black',
    },
    countryStyle: {
        flex: 1,
        borderColor: 'black',
        borderTopWidth: 1,
        padding: 12,
    },
    closeButtonStyle: {
        marginVertical: 15,
    },
});


export default PhoneInput;
