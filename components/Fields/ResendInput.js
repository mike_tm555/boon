import React from "react";
import {View} from "../Themed";
import {Button} from "../Button";
import {StyleSheet} from "react-native";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default function (props) {
    return (
        <View style={styles.container}>
            <Button
                type="simple"
                title={props.title}
                icon={{
                    name: 'repeat',
                    type: 'font-awesome'
                }}
                style={styles.button}
                onPress={props.onChange}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center'
    },
    button: {
        color: theme.note
    }
});
