import React, { Component } from "react";
import { StyleSheet, Text, View, TextInput, TouchableWithoutFeedback } from "react-native";
import Inputs from "../../constants/styles/Inputs";
import useErrorColorScheme from "../../hooks/useErrorColorScheme";
const DIGITS_COUNT = 6;

export default class CodeInput extends Component {
    input = React.createRef();
    state = {
        value: this.props.value || "",
        focused: false,
    };

    constructor(props) {
        super(props);
        this.codeLength = new Array(props.length || DIGITS_COUNT).fill(0);
    }

    componentDidUpdate(prevState, prevProps) {
        if (prevState.value !== this.props.value) {
            this.setState({
                value: this.props.value || ""
            });
        }
    }

    handleClick = () => {
        this.input.current.focus();
    };

    handleFocus = () => {
        this.setState({ focused: true });
    };

    handleBlur = () => {
        this.setState({
            focused: false,
        });
    };

    handleKeyPress = e => {
        if (e.nativeEvent.key === "Backspace") {
            const value = this.state.value.slice(0, this.state.value.length - 1);
            this.setState({
                value: value
            });
        }
    };

    handleChange = value => {
        this.setState(state => {
            if (state.value.length >= this.codeLength.length) return null;
            return {
                value: (state.value + value).slice(0, this.codeLength.length),
            };
        }, () => {
            if (typeof this.props.onChange === 'function') {
                this.props.onChange(this.state.value);
            }
        });
    };

    render() {
        let { value, focused } = this.state;
        const { error } = this.props;

        const values = value.split("");

        const selectedIndex = values.length < this.codeLength.length ? values.length : this.codeLength.length - 1;

        const hideInput = !(values.length < this.codeLength.length);

        const errorStyles = useErrorColorScheme(error);

        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.handleClick}>
                    <View style={styles.wrap}>
                        <TextInput
                            value=""
                            ref={this.input}
                            textContentType="oneTimeCode"
                            onChangeText={this.handleChange}
                            onKeyPress={this.handleKeyPress}
                            onFocus={this.handleFocus}
                            onBlur={this.handleBlur}
                            style={[
                                styles.input,
                                {
                                    left: selectedIndex * 60,
                                    opacity: hideInput ? 0 : 1,
                                },
                            ]}
                        />
                        {this.codeLength.map((v, index) => {
                            const selected = values.length === index;
                            const filled = values.length === this.codeLength.length && index === this.codeLength.length - 1;
                            const removeBorder = index === this.codeLength.length - 1 ? styles.noBorder : undefined;

                            return (
                                <View style={[styles.display, removeBorder, errorStyles]} key={index}>
                                    <Text style={styles.text}>{values[index] || ""}</Text>
                                    {(selected || filled) && focused && (<View style={styles.shadows} />)}
                                </View>
                            );
                        })}
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: "center",
        justifyContent: "center",
    },
    wrap: {
        position: "relative",
        flexDirection: "row",
    },
    input: {
        flex: 1,
        width: 50,
        height: Inputs.field.height,
        position: "absolute",
        fontSize: 35,
        textAlign: "center",
        borderColor: Inputs.field.borderColor,
        backgroundColor: "transparent",
        borderBottomWidth: Inputs.field.borderWidth,
        top: 0,
        bottom: 0,
    },
    display: {
        borderBottomWidth: Inputs.field.borderWidth,
        borderColor: Inputs.field.borderColor,
        width: 50,
        height: Inputs.field.height,
        marginRight: 10,
        alignItems: "center",
        justifyContent: "center",
        overflow: "visible",
        textAlign: 'center',
    },
    text: {
        fontSize: 35,
    },
    noBorder: {
        borderRightWidth: 0,
    },
    shadows: {
        position: "absolute",
        left: -4,
        top: -4,
        bottom: -4,
        right: -4,
        borderColor: Inputs.field.focused,
        borderWidth: 4,
    },
});
