import React, {useRef, useState} from "react";
import {Text, View} from "../Themed";
import {TouchableOpacity, StyleSheet} from "react-native";
import Inputs from "../../constants/styles/Inputs";
import Languages from "../Languages";
import I18n from '../../utils/i18n';

export default function (props) {
    const { onChange } = props;
    const languageSheetRef = useRef();

    const [value, setValue] = useState(props.value);

    const changeLanguage = (lang) => {
        setValue(lang);
        onChange(lang);
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.wrapper}
                onPress={() => {
                    languageSheetRef.current?.open();
                }}
            >
                {props.title && (
                    <Text style={styles.label}>{props.title}</Text>
                )}

                <Text style={styles.language}>{I18n.languages[value].name}</Text>
            </TouchableOpacity>

            <Languages
                languages={I18n.languages}
                change={changeLanguage}
                langRef={languageSheetRef}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    wrapper: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: Inputs.field.height,
    },
    label: {
        width: 'auto',
        fontSize: Inputs.field.fontSizeLarge,
        color: Inputs.field.labelColor,
        marginRight: 10,
    }
});
