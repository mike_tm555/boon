import React from "react";
import Drawer from 'react-native-drawer';
import styles from "./styles";

export default function Index(props) {
    const {
        onClose,
        content,
        children,
        tweenDuration = 200,
        open,
        side = 'left'
    } = props;

    return (
        <Drawer
            type="overlay"
            open={open}
            side={side}
            onClose={onClose}
            content={content}
            tapToClose={true}
            openDrawerOffset={0}
            panCloseMask={0.2}
            panOpenMask={0.2}
            closedDrawerOffset={0}
            styles={{
                drawer: styles.drawer,
                main: styles.main
            }}
            tweenDuration={tweenDuration}
        >
            {children}
        </Drawer>
    );
}
