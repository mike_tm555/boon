import Shadows from "../../constants/styles/Shadows";

export default {
    drawer: {
        width: '100%',
        height: '100%',
        position: 'relative',
        zIndex: 1,
        marginLeft: -5,
        ...Shadows.mild,
    },
    main: {
        opacity: 1,
    },
}
