import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    videoContainer: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: theme.overlay,
        overflow: 'hidden',
        flex: 1,
    },
    video: {
        width: '100%',
        height: '100%',
    },
    indicator: {
        color: theme.indicator,
        position: 'absolute',
    },
    error: {
        position: 'absolute',
        color: theme.lightText,
    },
}
