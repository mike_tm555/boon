import React, {useEffect, useState} from "react";
import { Video } from 'expo-av';
import {Text, View} from "../Themed";
import {ActivityIndicator} from "react-native";
import I18n from '../../utils/i18n';
import styles from "./styles";

const VideoPlayer = (props) => {
    const {
        url,
        live = false,
    } = props;

    const [loaded, setLoaded] = useState(false);
    const [error, setError] = useState(null);
    const [videoRef, setVideoRef] = useState(null);
    const [videoProps, setVideoProps] = useState({
        shouldCorrectPitch: true,
        shouldPlay: live,
        isLooping: false,
        rate: 1,
        volume: 1,
        useNativeControls: !live
    });

    useEffect(() => {
        if (videoRef) {
            mountVideo();
        }
    }, [videoRef]);

    const mountVideo = () => {
        if (videoRef) {
            videoRef.loadAsync({
                uri: url,
                overrideFileExtensionAndroid: 'm3u8'
            }, {
                androidImplementation: 'MediaPlayer',
                shouldCorrectPitch: videoProps.shouldCorrectPitch,
                shouldPlay: videoProps.shouldPlay,
                rate: videoProps.rate,
                volume: videoProps.volume,
                isLooping: videoProps.isLooping,
            }, false).then(() => {
                if (videoRef) {
                    setLoaded(true);
                    setError(null);
                }
            }).catch(() => {
                if (videoRef) {
                    setLoaded(true);
                    setError(I18n.t('errors.noSignal'));
                }
            });
        }
    };

    const onLoadStart = () => {
        if (videoRef) {
            setLoaded(false);
        }
    };

    const onLoad = () => {
        if (videoRef) {
            setLoaded(true);
        }
    };

    const onError = (error) => {
        console.log(error, '');
        setLoaded(true);
        setError(I18n.t('errors.noSignal'));
    };

    const handleVideoRef = (ref) => {
        setVideoRef(ref);
    };

    return (
        <View style={styles.videoContainer}>
            {!loaded && (
                <ActivityIndicator
                    color={styles.indicator.color}
                    style={styles.indicator}
                    size="large"
                />
            )}

            {error && (
                <Text type="caption" style={styles.error}>{error}</Text>
            )}

            <Video
                style={styles.video}
                ref={handleVideoRef}
                useNativeControls={videoProps.useNativeControls}
                resizeMode={Video.RESIZE_MODE_CONTAIN}
                onLoadStart={onLoadStart}
                onLoad={onLoad}
                onError={onError}
            />
        </View>
    )
};

export default VideoPlayer;
