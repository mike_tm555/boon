import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";
import Layout from "../../constants/styles/Layout";

const theme = useTheme();

export default {
    container: {
        width: '100%',
        height: '100%',
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        width: '100%',
        marginTop: -50,
        position: 'relative',
        paddingHorizontal: Layout.content.space,
        zIndex: 1,
    },
    headerWrapper: {
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        paddingTop: 50,
        marginBottom: 10,
        backgroundColor: theme.border,
        ...Shadows.default
    },
    footer: {
        width: '100%',
        background: 'transparent',
        padding: Layout.content.space,
        zIndex: 1,
    },
    footerWrapper: {
        background: 'transparent',
    },
    content: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    scroll: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        paddingHorizontal: Layout.content.space,
        marginBottom: -100,
        marginTop: -100,
    },
    scrollContent: {
        flexGrow: 1,
        width: '100%',
        height: 'auto',
        paddingHorizontal: Layout.scroll.space,
        paddingVertical: 100,
        flexDirection: 'column',
    },
};
