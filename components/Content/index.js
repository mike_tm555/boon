import React, {useEffect, useState} from "react";
import {ScrollView} from "react-native";
import {View} from "../Themed";
import styles from "./styles";

const Content = (props) => {
    const {
        header,
        footer,
        children,
        space,
        onScrollBottom,
        style = {}
    } = props;

    useEffect(() => {
        setScrollToBottom(false);
    }, [children]);

    const [scrolledToBottom, setScrollToBottom] = useState(false);

    const onScroll = (e) => {
        if (e.nativeEvent.contentOffset.y >= e.nativeEvent.layoutMeasurement.height) {
            if (scrolledToBottom === false) {
                setScrollToBottom(true);
                if (typeof onScrollBottom === 'function') {
                    onScrollBottom()
                }
            }
        } else {
            setScrollToBottom(false);
        }
    };

    const spaceStyles = {};

    if (space === null) {
        spaceStyles.paddingHorizontal = 0;
    }

    return (
        <View style={[styles.container, style]}>
            {header && (
                <View style={styles.header}>
                    <View style={styles.headerWrapper}>
                        {header}
                    </View>
                </View>
            )}
            <View style={styles.content}>
                <ScrollView
                    scrollEventThrottle={1}
                    onScroll={onScroll}
                    style={{
                        ...styles.scroll,
                        ...spaceStyles
                    }}
                    contentContainerStyle={{
                        ...styles.scrollContent,
                        ...spaceStyles
                    }}
                >
                    {children}
                </ScrollView>
            </View>
            {footer && (
                <View style={styles.footer}>
                    <View style={styles.footerWrapper}>
                        {footer}
                    </View>
                </View>
            )}
        </View>
    )
};

export default Content;
