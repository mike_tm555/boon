import React from "react";
import {Input} from "react-native-elements";
import {Icon, View} from "../Themed";
import styles from "./styles";

const Search = (props) => {
    const { search, placeholder, value } = props;
    return (
        <View style={styles.wrapper}>
            <Input
                leftIcon={
                    <View style={styles.leftIcon}>
                        <Icon
                            name='search'
                            size={24}
                            color={styles.leftIcon.color}
                        />
                    </View>
                }
                placeholder={placeholder}
                containerStyle={styles.container}
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.input}
                errorStyle={styles.error}
                onChangeText={search}
                value={value}/>
        </View>
    )
};

export default Search;
