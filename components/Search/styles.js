import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";
import Inputs from "../../constants/styles/Inputs";

const theme = useTheme();

export default {
    wrapper: {
        width: '100%',
        padding: 10,
    },
    container: {
        width: '100%',
        height: 40,
        borderWidth: 1,
        backgroundColor: theme.content,
        borderColor: theme.border,
        paddingHorizontal: 0,
        paddingVertical: 0,
        borderRadius: 50,
        overflow: 'hidden'
    },
    inputContainer: {
        height: '100%',
        borderBottomWidth: 0,
        paddingHorizontal: 0,
    },
    input: {
        width: '100%',
        height: '100%',
        paddingHorizontal: 5,
        borderWidth: 0,
        fontSize: Inputs.field.fontSize
    },
    error: {
      display: 'none',
    },
    leftIcon: {
        width: 40,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        color: theme.label,
    },
}
