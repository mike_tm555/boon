import React from "react";
import {LinearGradient} from 'expo-linear-gradient';
import {View} from "../Themed";
import styles from "./styles";

const Gradient = ({ children, colors, style }) => {
    return (
        <View
            style={{
                ...style,
                ...styles.container,
            }}
        >
            <LinearGradient
                style={{
                    ...style,
                    ...styles.gradient,
                }}
                colors={colors}>
                {children}
            </LinearGradient>
        </View>
    )
};

export default Gradient;
