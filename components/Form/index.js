import React, {useEffect, useRef, useState} from "react";
import {View, Text} from "../Themed";
import Field from "./field";
import {Button} from "../Button";
import useFormValidator, {useFormIsValid} from "../../hooks/useFormValidator";
import styles from "./styles";

const Form = (props) => {
    const {
        form,
        autoSubmit,
        actions,
        change,
        submit,
        cancel
    } = props;

    const formRef = useRef(null);
    const [data, setData] = useState(props.data || {});
    const initialFields = JSON.parse(JSON.stringify(form.fields));

    Object.keys(data).forEach(key => {
        if(initialFields[key]) {
            initialFields[key].params.value = data[key];
        }
    });

    const [loading, setLoading] = useState(false);
    const [fields, setFields] = useState(initialFields);
    const [changed, setChanged] = useState(props.changed);

    useEffect(() => {
        formRef.current = true;

        return () => {
            formRef.current = null;
        }
    }, []);

    useEffect(() => {
        if (formRef && formRef.current) {
            setChanged(props.changed);
        }
    }, [props.changed]);

    const formActions = {
        change: (name, value) => {
            const newData = {};
            const newFields = JSON.parse(JSON.stringify(fields));
            newFields[name].params.value = value;

            Object.keys(newFields).forEach(key => {
                if (newFields[key].params.value) {
                    newData[newFields[key].name] = newFields[key].params.value;
                }
            });

            setData(newData);
            setFields(newFields);

            setChanged(JSON.stringify(props.data) !== JSON.stringify(newData));

            if (typeof change === 'function') {
                change(name, value);
            }

            if (typeof autoSubmit === 'function') {
                autoSubmit(newData);
            }
        },
        submit: () => {
            if (Object.keys(data).length) {
                if (typeof submit === 'function') {
                    const newFields = useFormValidator(fields);
                    const validForm = useFormIsValid(newFields);

                    if (validForm) {
                        setLoading(true);
                        submit(data).then((response) => {
                            if (formRef && formRef.current) {
                                setLoading(false);

                                if (!response) {
                                    setChanged(true);
                                }
                            }
                        });
                    }

                    setFields(newFields);
                }
            }
        },
        cancel: () => {
            if (Object.keys(data).length) {
                if (typeof submit === 'function') {
                    cancel();
                }
            }
        },
        ...actions
    };

    let buttons = null;

    if (form.buttons) {
        buttons = JSON.parse(JSON.stringify(form.buttons)).map(button => {
            if (!changed) {
                button.params.disabled = true;
            }

            if (loading) {
                button.params.disabled = true;
            }

            return button;
        });
    }

    return (
        <View style={styles.container}>
            {form.title && (
                <View style={styles.title}>
                    <Text type="h2">{form.title}</Text>
                </View>
            )}
            {form.description && (
                <View style={styles.description}>
                    <Text>{form.description}</Text>
                </View>
            )}
            {form.image && (
                <View style={styles.image}>
                    <DefaultImage source={form.image}/>
                </View>
            )}
            <View style={styles.body}>
                {Object.keys(fields).map((key) => {
                    const field = fields[key];
                    return (
                        <View key={field.name}
                              style={styles.field}>
                            {field.label &&  (
                                <View>
                                    <Text style={styles.label}>{field.label}</Text>
                                </View>
                            )}

                            {field.type && (
                                <Field
                                    error={(field.error && field.error.valid === false)}
                                    name={field.name}
                                    component={field.component}
                                    params={field.params}
                                    onChange={(value) => {
                                        return formActions[field.action](field.name, value)
                                    }}
                                />
                            )}

                            {(field.error && field.error.message) && (
                                <Text style={styles.error}>{field.error.message}</Text>
                            )}
                        </View>
                    )
                })}
            </View>

            {buttons && (
                <View style={styles.footer}>
                    {buttons.map(button => {
                        const { params, name, action } = button;

                        return (
                            <View
                                key={name}
                                style={styles.button}
                            >
                                <Button
                                    onPress={() => {
                                        formActions[action]();
                                    }}
                                    {...params}
                                />
                            </View>
                        )
                    })}
                </View>
            )}
        </View>
    )
};

export default Form;
