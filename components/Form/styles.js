import Inputs from "../../constants/styles/Inputs";

export default {
    container: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        paddingHorizontal: 0,
    },
    title: {
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    description: {
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    image: {
        alignSelf: 'center',
        width: 'auto',
        height: 'auto',
        marginBottom: 100,
        paddingVertical: 10,
        paddingHorizontal: 0,
    },
    body: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
    },
    label: {
        width: '100%',
        fontSize: Inputs.field.fontSize,
        color: Inputs.field.labelColor,
    },
    error: {
        color: Inputs.field.errorColor,
        fontSize: 12,
        marginTop: 5,
    },
    field: {
        width: '100%',
        paddingVertical: 5,
    },
    footer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        marginTop: 10,
    },
    button: {
    },
}
