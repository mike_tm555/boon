import React from "react";
import PhoneInput from "../Fields/PhoneInput";
import CodeInput from "../Fields/CodeInput";
import CheckBoxInput from "../Fields/CheckBoxInput";
import ResendInput from "../Fields/ResendInput";
import {View} from "../Themed";
import NameInput from "../Fields/NameInput";
import EmailInput from "../Fields/EmailInput";
import HelpInput from "../Fields/HelpInput";
import NumberInput from "../Fields/NumberInput";
import SwitchInput from "../Fields/SwitchInput";
import LanguageInput from "../Fields/LanguageInput";
import TextAreaInput from "../Fields/TextAreaInput";

const FIELD_COMPONENTS = {
    phone: PhoneInput,
    code: CodeInput,
    checkbox: CheckBoxInput,
    resend: ResendInput,
    name: NameInput,
    email: EmailInput,
    help: HelpInput,
    number: NumberInput,
    switch: SwitchInput,
    language: LanguageInput,
    text: TextAreaInput
};

export default function (props) {
    const { component, error, params, onChange } = props;
    const FieldComponent = FIELD_COMPONENTS[component];

    return (
        <View>
            <FieldComponent
                error={error}
                {...params}
                onChange={onChange}
            />
        </View>
    )
}
