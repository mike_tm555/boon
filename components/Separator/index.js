import React from "react";
import {View} from "../Themed";
import styles from "./styles";

const Separator = (props) => {
    const {
        type = 'horizontal',
        size = 'large'
    } = props;

    const style = styles[`${type}_${size}`];

    return (
        <View style={style}/>
    )
};

export default Separator;
