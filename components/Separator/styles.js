import useTheme from "../../hooks/useTheme";

const theme = useTheme();
export default {
    horizontal_large: {
        width: '100%',
        height: 3,
        opacity: 0.7,
        backgroundColor: theme.separator,
        marginVertical: 20,
    },
    horizontal_small: {
        width: '100%',
        height: 1,
        opacity: 0.3,
        backgroundColor: theme.separator,
        marginVertical: 3,
    },
    vertical_large: {
        width: 3,
        height: '100%',
        opacity: 0.7,
        backgroundColor: theme.separator,
        marginHorizontal: 20
    },
    vertical_small: {
        width: 1,
        height: '100%',
        opacity: 0.3,
        backgroundColor: theme.separator,
        marginHorizontal: 5
    },
}
