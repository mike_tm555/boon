import React, {useEffect} from 'react';
import * as ScreenOrientation from "expo-screen-orientation";

const Orientation = (props) => {
    const {
        children,
    } = props;
    const screenToLandscape = async () => {
        await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.ALL);
    }

    const screenToDefault = async () => {
        ScreenOrientation.removeOrientationChangeListeners();
        await ScreenOrientation.unlockAsync();
        await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
    }

    useEffect(() => {
        screenToLandscape();

        return () => {
            screenToDefault();
        }
    }, []);

    return (
        <>
            {children}
        </>
    );
}

export default Orientation;
