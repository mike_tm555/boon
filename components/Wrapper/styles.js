import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";
import Layout from "../../constants/styles/Layout";

const theme = useTheme();

export default {
    container: {
        width: '100%',
        height: '100%',
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: Layout.wrapper.space,
        backgroundColor: theme.content,
    },
    header: {
        width: '100%',
        marginTop: -50,
        zIndex: 1,
    },
    headerWrapper: {
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        paddingTop: 50,
        marginBottom: 10,
        backgroundColor: theme.border,
        ...Shadows.default
    },
    footer: {
        width: '100%',
        background: 'transparent',
        padding: Layout.content.space,
        zIndex: 1,
    },
    footerWrapper: {
        background: 'transparent',
    },
    content: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        marginTop: -50,
        backgroundColor: theme.content,
        ...Shadows.default,
    },
    wrapper: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        colors: theme.wrapper,
    },
    scroll: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    scrollContent: {
        flexGrow: 1,
        width: '100%',
        height: 'auto',
        paddingTop: 50,
        paddingHorizontal: Layout.wrapper.space,
        flexDirection: 'column',
    }
};
