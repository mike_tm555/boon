import React, {useEffect, useState} from "react";
import {LinearGradient} from "expo-linear-gradient";
import {ScrollView} from "react-native";
import {View} from "../Themed";
import styles from "./styles";

const Wrapper = (props) => {
    const {
        header,
        footer,
        children,
        onScrollBottom,
        style = {}
    } = props;

    useEffect(() => {
        setScrollToBottom(false);
    }, [children]);

    const [scrolledToBottom, setScrollToBottom] = useState(false);

    const onScroll = (e) => {
        if (e.nativeEvent.contentOffset.y >= e.nativeEvent.layoutMeasurement.height) {
            if (scrolledToBottom === false) {
                setScrollToBottom(true);
                if (typeof onScrollBottom === 'function') {
                    onScrollBottom()
                }
            }
        } else {
            setScrollToBottom(false);
        }
    };

    return (
        <View style={[styles.container, style]}>
            {header && (
                <View style={styles.header}>
                    <View style={styles.headerWrapper}>
                        {header}
                    </View>
                </View>
            )}
            <View style={styles.content}>
                <LinearGradient
                    colors={styles.wrapper.colors}
                    style={styles.wrapper}
                >
                    <ScrollView
                        scrollEventThrottle={1}
                        onScroll={onScroll}
                        style={styles.scroll}
                        contentContainerStyle={styles.scrollContent}
                    >
                        {children}
                    </ScrollView>
                </LinearGradient>
            </View>
            {footer && (
                <View style={styles.footer}>
                    <View style={styles.footerWrapper}>
                        {footer}
                    </View>
                </View>
            )}
        </View>
    )
};

export default Wrapper;
