import React from "react";
import {
    Modal as DefaultModal, TouchableOpacity,
} from "react-native";
import Content from "../Content";
import {View} from "../Themed";
import styles from "./styles";

export default function Dialog(props) {
    const {
        header,
        footer,
        children,
        close,
        style={},
        useNativeDriver = true,
        ...otherProps
    } = props;

    const hideDialog = () => {
        if (typeof close === 'function') {
            close();
        }
    };

    return (
        <DefaultModal
            transparent={true}
            statusBarTranslucent={true}
            useNativeDriver={useNativeDriver}
            {...otherProps}
        >
            <View style={styles.container}>
                <View style={{
                    ...styles.content,
                    ...style
                }}>
                    {children}
                </View>
                <TouchableOpacity
                    style={styles.overLay}
                    onPress={hideDialog}
                />
            </View>
        </DefaultModal>
    )
}
