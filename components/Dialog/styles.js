import useTheme from "../../hooks/useTheme";
import Shadows from "../../constants/styles/Shadows";

const theme = useTheme();

export default {
    container: {
        width: '100%',
        height: '100%',
        flex: 1,
        padding: 30,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: theme.overlay,
    },
    overLay: {
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        position: 'absolute',
        borderWidth: 1,
        elevation: 1,
    },
    content: {
        width: '100%',
        height: 'auto',
        maxWidth: 400,
        flexDirection: 'column',
        backgroundColor: theme.content,
        borderRadius: 10,
        overflow: 'hidden',
        ...Shadows.default
    }
}
