import React, {useEffect, useRef} from "react";
import { Animated } from 'react-native';

const SlideAnimate = (props) => {
    const  {
        children,
        direction = 'right',
        duration = 500,
    } = props;

    let startValue = -500;

    if (direction === 'left') {
        startValue = 500;
    }

    const animatedValue = useRef(new Animated.Value(startValue)).current;

    useEffect(() => {
        Animated.timing(
            animatedValue,
            {
                toValue: 1,
                duration: duration,
                useNativeDriver: true
            }
        ).start();
    }, [animatedValue]);

    return (
        <Animated.View
            style={{
                transform: [
                    {
                        translateX: animatedValue,
                    }
                ],
            }}
        >
            {children}
        </Animated.View>
    )
};

export default SlideAnimate;
