import React, {useEffect, useRef} from "react";
import { Animated } from 'react-native';

const FadeInAnimate = (props) => {
    const  {
        children,
        duration = 1000,
    } = props;

    const animatedValue = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.timing(
            animatedValue,
            {
                toValue: 1,
                duration: duration,
                useNativeDriver: true
            }
        ).start();
    }, [animatedValue]);

    return (
        <Animated.View
            style={{
                opacity: animatedValue,
            }}
        >
            {children}
        </Animated.View>
    )
};

export default FadeInAnimate;
