import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    container: {
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'column',
        backgroundColor: theme.content,
    },
    header: {
        width: '100%',
        alignItems: 'center',
        paddingTop: 40,
    },
    content: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    footer: {
        width: '100%',
    }
}
