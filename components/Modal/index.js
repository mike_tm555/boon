import React, {useEffect} from "react";
import * as ScreenOrientation from 'expo-screen-orientation';
import {
    Modal as DefaultModal,
} from "react-native";
import {View} from "../Themed";
import styles from "./styles";

export default function Modal(props) {
    const {
        header,
        footer,
        children,
        close,
        useNativeDriver = true,
        visible,
        supportedOrientations = [],
        ...otherProps
    } = props;

    return (
        <DefaultModal
            visible={visible}
            statusBarTranslucent={true}
            {...otherProps}
        >
            <View style={styles.container}>
                {header && (
                    <View style={styles.header}>
                        {header}
                    </View>
                )}
                <View style={styles.content}>
                    {children}
                </View>
                {footer && (
                    <View style={styles.footer}>
                        {footer}
                    </View>
                )}
            </View>
        </DefaultModal>
    )
}
