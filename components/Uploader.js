import React, { Component } from "react";
import {ActivityIndicator, Alert, StyleSheet} from 'react-native'
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {View, Text} from "./Themed";
import {IconButton} from "./Button";
import useTheme from "../hooks/useTheme";
import I18n from '../utils/i18n';

const theme = useTheme();

export default class Uploader extends Component {
    state = {
        image: null,
        uploading: false,
    };

    _takePhoto = async () => {
        const {
            status: cameraPerm
        } = await Permissions.askAsync(Permissions.CAMERA);

        const {
            status: cameraRollPerm
        } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        // only if user allows permission to camera AND camera roll
        if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
            let pickerResult = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4, 3],
            });

            this._handleImagePicked(pickerResult);
        }
    };

    _pickImage = async () => {
        const {
            status: cameraRollPerm
        } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        // only if user allows permission to camera roll
        if (cameraRollPerm === 'granted') {
            let pickerResult = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3],
            });

            this._handleImagePicked(pickerResult);
        }
    };

    _handleImagePicked = async pickerResult => {
        try {
            this.setState({
                uploading: true
            });

            if (!pickerResult.cancelled) {
                const uploadResult = await this.props.upload({
                    uri: pickerResult.uri,
                    name: 'avatar.jpg',
                    type: 'image/jpeg'
                });
                this.setState({
                    image: uploadResult
                });
            }
        } catch (e) {
            Alert.alert(I18n.t('uploadFailed'), I18n.t('tryAgain'));
        } finally {
            this.setState({
                uploading: false
            });
        }
    };

    render() {
        let {
            image,
            uploading,
        } = this.state;

        let {
            defaultImage
        } = this.props;

        const { gallery, photo } = this.props;
        const source = image ? { uri: image } : defaultImage;
        return (
            <View style={styles.container}>
                {(!uploading && gallery) && (
                    <IconButton
                        style={styles.image}
                        source={source}
                        onPress={() => {
                            this._pickImage();
                        }}
                    />
                )}
                {(!uploading && photo) && (
                    <IconButton
                        style={styles.image}
                        source={source}
                        onPress={() => {
                            this._takePhoto()
                        }}
                    />
                )}
                {uploading && (
                    <View style={[StyleSheet.absoluteFill, styles.maybeRenderUploading]}>
                        <ActivityIndicator color={theme.indicator} size="large" />
                    </View>
                )}

                {image ? (
                    <Text type="caption">{I18n.t('changePhoto')}</Text>
                ) : (
                    <Text type="caption">{I18n.t('addPhoto')}</Text>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    image: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 20,
    },
    maybeRenderUploading: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 20,
        position: 'relative',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
