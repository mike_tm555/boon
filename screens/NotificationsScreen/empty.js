import React from "react";
import {Text, View} from "../../components/Themed";
import {StyleSheet} from "react-native";
import {Button} from "../../components/Button";
import Assets from "../../constants/definitions/Assets";
import I18n from '../../utils/i18n';
import {SvgIcon} from "../../components/SvgIcon";

export default function ({ navigation }) {

    return (
        <View style={styles.container}>
            <SvgIcon
                icon={Assets.notifications.emptyNotifications}
                style={styles.icon}/>

            <Text type="h1">{I18n.t('notifications.empty.title')}</Text>
            <Text type="h5">{I18n.t('notifications.empty.description')}</Text>

            <Button
                type="action"
                title={I18n.t('back')}
                onPress={() =>{
                    navigation.goBack();
                }}>
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon:{
        marginBottom: 50,
        resizeMode: "cover",
        height: 120,
        width: '100%',
    },
    button:{
        marginVertical: 10
    }
});
