import React from 'react';
import Screen from '../../components/Screen';
import List from "./list";
import Single from "./single";
import {connect} from "react-redux";

const CONTENT_COMPONENTS = {
    list: List,
    single: Single
};

const NotificationsScreen = ({ navigation, route, user }) => {
    const ContentComponent = CONTENT_COMPONENTS[route.name];

    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            <ContentComponent
                user={user}
                navigation={navigation}
                route={route}
            />
        </Screen>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

export default connect(mapStateToProps)(NotificationsScreen);

