import React, {useEffect, useState} from 'react';
import { ActivityIndicator, StyleSheet } from "react-native";
import Empty from "./empty";
import Content from "../../components/Content";
import Item from './item';
import useRequest from "../../hooks/useRequest";
import Routing from "../../navigation/Routing";
import {View} from "../../components/Themed";
import {Button} from "../../components/Button";
import Separator from "../../components/Separator";
import I18n from '../../utils/i18n';
import {connect} from "react-redux";
import useTheme from "../../hooks/useTheme";

const NOTIFICATION_FILTERS = [
    {
        title: I18n.t('unread'),
        key: 'unread',
        value: {
            key: 'isRead',
            value: false
        },
        active: true,
    },
    {
        title: I18n.t('read'),
        key: 'read',
        value: {
            key: 'isRead',
            value: true
        },
        active: false
    }
];

const theme = useTheme();

const List = ({ navigation, user, notifications }) => {
    const request = useRequest();
    const [filters, setFilters] = useState(NOTIFICATION_FILTERS);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const params = {
            userId: user.id,
            pageNumber: 1,
            pageSize: 50,
        };

        filters.forEach(filter => {
            if (filter.active) {
                params[filter.value.key] = filter.value.value;
            }
        });

        setLoading(true);
        request.notifications(params).then(() => {
            setLoading(false);
        });
    }, [filters]);

    const changeFilter = (key) => {
        const newFilters = filters.map(filter => {
            filter.active = key === filter.key;
            return filter;
        });

        setFilters(newFilters);
    };

    const openNotification = (item) => {
        navigation.navigate(Routing.notifications.name, {
            screen: Routing.notifications.screens.single.name,
            params: {
                item: item
            }
        })
    };

    let content = null;

    if (loading) {
        content = (
            <ActivityIndicator
                color={styles.indicator.color}
                style={styles.indicator}
                size="large"
            />
        )
    } else {
        if (notifications.length) {
            content = (
                notifications.map((item, key) => {
                    return (
                        <Item
                            key={key}
                            onPress={() => {
                                openNotification(item)
                            }}
                            {...item}
                        />
                    )
                })
            )
        } else {
            content = (
                <Empty navigation={navigation}/>
            )
        }
    }

    const filterComponents = [];
    filters.forEach((filter, index) => {
        if (index === filters.length - 1) {
            filterComponents.push(
                <Separator
                    key={filter.key + "separator"}
                    type="vertical"
                />
            )
        }

        filterComponents.push(
            <Button
                key={filter.key}
                style={styles.filterButton}
                type="simple"
                disabled={filter.active}
                title={filter.title}
                onPress={() => {
                    changeFilter(filter.key);
                }}
            />
        );
    });

    return (
        <Content
            header={(
                <View style={styles.filterWrapper}>
                    {filterComponents}
                </View>
            )}
        >
            <View style={styles.container}>
                {content}
            </View>
        </Content>
    )
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 20,
    },
    filterWrapper: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
    },
    filterButton: {
        width: 150,
        minWidth: 0,
        fontWeight: 'bold',
        marginVertical: 0,
        paddingVertical: 5,
    },
    indicator: {
        marginTop: 50,
        color: theme.indicator
    }
});

const mapStateToProps = (state) => {
    return {
        notifications: state.notifications,
    };
};

export default connect(mapStateToProps)(List);
