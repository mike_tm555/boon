import React from "react";
import {View} from "../../components/Themed";
import {StyleSheet} from "react-native";
import {Text} from "../../components/Themed";
import ListItem from "../../components/ListItem";

export default function (props) {
    const {
        id,
        date,
        title,
        message,
    } = props;

    return (
        <ListItem
            press={() => {
                props.onPress(id);
            }}
        >
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text type="h4">{title}</Text>
                    <Text type="caption">{date}</Text>
                </View>
                <View style={styles.message}>
                    <Text>{message}</Text>
                </View>
            </View>
        </ListItem>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
    },
    header: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    message: {
        width: '100%'
    },
});
