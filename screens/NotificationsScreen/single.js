import React from 'react';
import { StyleSheet } from 'react-native';
import Content from "../../components/Content";
import {Text} from "../../components/Themed";
import Separator from "../../components/Separator";

const Single = ({ route }) => {
    const { item } = route.params;

    return (
        <Content style={styles.container}>
            <Text type="h1" style={styles.title}>{item.title}</Text>
            <Separator type="horizontal"/>
            <Text style={styles.date}>{item.date}</Text>
            <Text style={styles.message}>{item.message}</Text>
        </Content>
    )
};

const styles = StyleSheet.create({
    container: {
      height: '100%',
    },
    date: {
        marginVertical: 20,
    },
    title: {
        marginBottom: 20,
        alignSelf: 'center'
    },
    message: {
        fontSize: 13,
        lineHeight: 25
    }
});

export default Single;
