import React, {useEffect, useRef, useState} from "react";
import {StyleSheet} from "react-native";
import {Tooltip, View} from "../../components/Themed";
import {Text} from "../../components/Themed";
import I18n from '../../utils/i18n';
import Shadows from "../../constants/styles/Shadows";
import Separator from "../../components/Separator";
import useTheme from "../../hooks/useTheme";
import LockUnlockInput from "../../components/Fields/UnlockInput";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";
import {Button} from "../../components/Button";
import Routing from "../../navigation/Routing";
import useRequest from "../../hooks/useRequest";
import ListItem from "../../components/ListItem";
import Dialog from "../../components/Dialog";
import {INTERCOM_NAME_FORM} from "../../constants/definitions/Forms";
import Form from "../../components/Form";
import Confirm from "../../components/Confirm";

const theme = useTheme();

export default function (props) {
    const {
        id,
        address,
        navigation,
        type,
        name,
    } = props;

    const [form] = useState(JSON.parse(JSON.stringify(INTERCOM_NAME_FORM)));
    const [showEdit, setShowEdit] = useState(false);
    const [showDelete, setShowDelete] = useState(false);

    const [status, setStatus] = useState(props.status);
    const tooltipRef = useRef(null);
    const request = useRequest();

    const submitForm = async (data) => {
        const intercomData = {
            flatNumber: address.flatNumber,
            name: data.name,
        };

        const response = await request.editIntercom(id, intercomData);

        if (response) {
            closeEdit();
        }

        return response;
    }

    const openEdit = () => {
        tooltipRef.current?.toggleTooltip();
        setShowEdit(true);
    }

    const closeEdit = () => {
        setShowEdit(false);
    }

    const toggleLock = (status) => {
        request.openIntercom(id).then((response) => {
            if (response) {
                setStatus(!status);
            }
        })
    };

    const openCamera = () => {
        tooltipRef.current?.toggleTooltip();
        navigation.navigate(Routing.cameras.name, {
            screen: Routing.cameras.screens.list.name
        });
    };

    const openActivity = () => {
        tooltipRef.current?.toggleTooltip();
        navigation.navigate(Routing.history.name, {
            screen: Routing.history.screens.list.name
        })
    };

    const deleteLock = () => {
        setShowDelete(true);
    };

    const closeDelete = () => {
        setShowDelete(false);
    }

    const confirmDelete = () => {
        request.deleteIntercom(id).then((response) => {
            tooltipRef.current?.toggleTooltip();
        });
    }

    return (
        <ListItem>
            <Dialog
                close={closeEdit}
                onRequestClose={closeEdit}
                visible={showEdit}>
                <View style={styles.editForm}>
                    <Text type="h1">{I18n.t('editIntercomName')}</Text>
                    <Form
                        data={{
                            name: name,
                        }}
                        submit={submitForm}
                        form={form}/>
                </View>
            </Dialog>
            <Confirm
                visible={showDelete}
                close={closeDelete}
                confirm={confirmDelete}
                cancelTitle={I18n.t('no')}
                confirmTitle={I18n.t('yes')}
            >
                <Text type="h1">{I18n.t('locks.delete.title')}</Text>
                <Text type="h5">{I18n.t('locks.delete.description')}</Text>
            </Confirm>
            <View style={styles.container}>
                <SvgIcon
                    style={styles.icon}
                    icon={Assets.locks.icons[type]}
                />
                <Text
                    type="h4"
                    style={styles.info}
                >
                    {name}
                </Text>
                <Separator type="vertical"/>
                <View style={styles.toggle}>
                    <LockUnlockInput
                        onChange={toggleLock}
                        value={status}
                    />
                </View>
                <Tooltip
                    tooltipRef={tooltipRef}
                    popover={(
                        <View style={styles.actionsMenu}>
                            <Button
                                style={styles.actionButton}
                                type="simple"
                                title={I18n.t('edit')}
                                icon={{
                                    name: 'pencil',
                                    type: 'font-awesome'
                                }}
                                onPress={openEdit}
                            />
                            <Separator type="horizontal" size="small"/>
                            <Button
                                style={styles.actionButton}
                                type="simple"
                                title={I18n.t('liveCam')}
                                icon={{
                                    name: 'video-camera',
                                    type: 'font-awesome'
                                }}
                                onPress={openCamera}
                            />
                            <Separator type="horizontal" size="small"/>
                            <Button
                                style={styles.actionButton}
                                type="simple"
                                title={I18n.t('activity')}
                                icon={{
                                    name: 'history',
                                    type: 'font-awesome'
                                }}
                                onPress={openActivity}
                            />
                            <Separator type="horizontal" size="small"/>
                            <Button
                                style={styles.actionButton}
                                type="simple"
                                title={I18n.t('delete')}
                                icon={{
                                    name: 'times-circle-o',
                                    type: 'font-awesome'
                                }}
                                onPress={deleteLock}
                            />
                        </View>
                    )}
                >
                    <View style={styles.actions}>
                        <SvgIcon
                            value={false}
                            style={styles.dots}
                            icon={Assets.icons.dots}
                        />
                    </View>
                </Tooltip>
            </View>
        </ListItem>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    editForm: {
        width: '100%',
        padding: 25,
    },
    icon: {
        width: '20%',
        height: 60,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    info: {
        width: '30%',
        flex: 1,
        paddingHorizontal: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'left',
    },
    toggle: {
        flex: 1,
        width: '30%',
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    actions: {
        width: 20,
        height: 50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
    },
    dots: {
        width: 5,
        height: 20,
    },
    actionsMenu: {
        width: '100%',
        paddingHorizontal: 10,
        ...Shadows.default,
    },
    actionButton: {
        width: '100%',
        paddingVertical: 8,
        marginVertical: 0,
        color: theme.lightText,
        justifyContent: 'flex-start'
    }
});
