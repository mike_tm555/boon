import React, {useEffect, useState} from 'react';
import { ActivityIndicator, StyleSheet } from "react-native";
import { Text, View } from "../../components/Themed";
import Empty from "./empty";
import Content from "../../components/Content";
import Item from './item';
import useRequest from "../../hooks/useRequest";
import {connect} from "react-redux";
import useSearch from "../../hooks/useSearch";
import {Button} from "../../components/Button";
import Routing from "../../navigation/Routing";
import I18n from '../../utils/i18n';
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

const List = ({ navigation, user, locks }) => {
    const request = useRequest();
    const search = useSearch(['name']);
    const [searchText] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        request.getLocks({
            userId: user.id
        }).then(() => {
            setLoading(false);
        });
    }, []);

    const addLock = () => {
        navigation.navigate(Routing.locks.name, {
            screen: Routing.locks.screens.intercom.name
        });
    };

    if (loading) {
        return (
            <Content>
                <View style={styles.container}>
                    <ActivityIndicator
                        size="large"
                        color={styles.indicator.color}
                        style={styles.indicator}
                    />
                </View>
            </Content>
        )
    } else {
        if (locks.length) {
            const items = search(locks, searchText);
            const filtered = {};

            items.forEach(item => {
                if (!filtered[item.address.id]) {
                    filtered[item.address.id] = {
                        address: item.address,
                        locks: []
                    }
                }

                filtered[item.address.id].locks.push(item);
            });

            return (
                <Content
                    footer={(
                        <Button
                            type="action"
                            icon={{
                                name: 'add-circle'
                            }}
                            title={I18n.t('addLock')}
                            onPress={addLock}
                        />
                    )}
                >
                    <View style={styles.container}>
                        {Object.keys(filtered).map((key) => {
                            const address = filtered[key].address;
                            const items = filtered[key].locks;
                            return (
                                <View
                                    key={key}
                                    style={styles.section}
                                >
                                    <Text
                                        type="caption"
                                        style={styles.sectionTitle}
                                    >
                                        {I18n.t('fullAddress', address)}
                                    </Text>
                                    {items.map((item) => {
                                        return (
                                            <Item
                                                key={item.id}
                                                navigation={navigation}
                                                {...item}
                                            />
                                        )
                                    })}
                                </View>
                            )
                        })}
                    </View>
                </Content>
            )
        } else {
            return (
                <Content>
                    <View style={styles.container}>
                        <Empty navigation={navigation}/>
                    </View>
                </Content>
            )
        }
    }
};

const  styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 20,
    },
    section: {
        width: '100%'
    },
    sectionTitle: {
        marginBottom: 15,
    },
    indicator: {
        marginTop: 50,
        color: theme.indicator
    }
});


const mapStateToProps = (state) => {
    return {
        locks: state.locks,
    };
};

export default connect(mapStateToProps)(List);
