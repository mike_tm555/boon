import React from "react";
import {View} from "../../components/Themed";
import {StyleSheet} from "react-native";
import {Text} from "../../components/Themed";
import {Button} from "../../components/Button";
import Assets from "../../constants/definitions/Assets";
import I18n from '../../utils/i18n';
import {SvgIcon} from "../../components/SvgIcon";
import Routing from "../../navigation/Routing";

export default function ({ navigation }) {
    const addLock = () => {
        navigation.navigate(Routing.locks.name, {
            screen: Routing.locks.screens.intercom.name
        });
    };

    return (
        <View style={styles.container}>
            <SvgIcon
                icon={Assets.locks.emptyLocks}
                style={styles.icon}
            />
            <Text type="h1">{I18n.t('locks.empty.title')}</Text>
            <Text type="h5">{I18n.t('locks.empty.description')}</Text>
            <Button
                type="action"
                title={I18n.t('addLock')}
                onPress={addLock}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:'100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon:{
        marginBottom: 50,
        resizeMode: "cover",
        height: 120,
        width: '100%',
    },
    button:{
        marginVertical: 10
    }
});
