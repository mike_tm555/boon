import React, {useState} from 'react';
import { StyleSheet } from 'react-native';
import Form from "../components/Form";
import {WELCOME_FORM} from "../constants/definitions/Forms";
import useRequest from "../hooks/useRequest";
import {Text, View} from "../components/Themed";
import Uploader from "../components/Uploader";
import Assets from "../constants/definitions/Assets";
import Content from "../components/Content";
import Routing from "../navigation/Routing";
import Screen from "../components/Screen";
import {connect} from "react-redux";
import I18n from "../utils/i18n";

const WelcomeScreen = ({navigation, route, user}) => {
    const [form] = useState(JSON.parse(JSON.stringify(WELCOME_FORM)));
    const [avatarUrl, setAvatarUrl] = useState(user && user.avatarUrl);
    const request = useRequest();

    const submitForm = async (data) => {
        const response = await request.register({
            avatarUrl: avatarUrl,
            name: data.name,
            email: data.email,
            id: user.id,
            phoneNumber: user.phoneNumber,
        });

        if (response) {
            return navigation.navigate(Routing.linkDevice.name, {
                screen: Routing.linkDevice.screens.intercom.name
            });
        }

        return data;
    };

    const uploadAvatar= async (uri) => {
        const response = await request.avatar({
            image: uri
        });

        setAvatarUrl(response.data.imageUrl);

        return response.data.imageUrl;
    };

    return (
        <Screen
            title={route.params.title}
            navigation={navigation}
        >
            <Content>
                <View style={styles.container}>
                <Text type="h1">{I18n.t('welcome.title')}</Text>
                <Text type="h5">{I18n.t('welcome.description')}</Text>
                <Uploader
                    gallery={true}
                    defaultImage={Assets.icons.avatar}
                    upload={uploadAvatar}
                />
                <Form
                    actions={{
                        upload: uploadAvatar
                    }}
                    submit={submitForm}
                    form={form}/>
                </View>
            </Content>
        </Screen>
    );
};

const styles = StyleSheet.create({
   container: {
       width: '100%',
       flexDirection: 'column',
       paddingVertical: 30,
   }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

export default connect(mapStateToProps)(WelcomeScreen);

