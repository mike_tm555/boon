import React from "react";
import {StyleSheet} from "react-native";
import {ImageBackground, View} from "../../components/Themed";
import {Text} from "../../components/Themed";
import I18n from '../../utils/i18n';
import Shadows from "../../constants/styles/Shadows";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

const PackageItem = (props) => {
    const {
        id,
        name,
        address,
        pricePerMonth,
        imageUrl,
    } = props;

    return (
        <View
            style={styles.container}
        >
            <View style={styles.info}>
                <Text type="h1" style={styles.title}>{name}</Text>
                <View style={styles.price}>
                    <Text type="h1" style={styles.amount}>{pricePerMonth}</Text>
                    <Text style={styles.label}>{I18n.t('perMonth')}</Text>
                </View>
            </View>
            <ImageBackground
                source={{
                    uri: imageUrl
                }}
                style={styles.image}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        position: 'relative',
        borderRadius: 15,
        overflow: 'hidden',
        ...Shadows.default,
    },
    info: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        padding: 10,
        flexDirection: 'column',
        elevation: 1,
        zIndex: 1,
    },
    image: {
        width: '100%',
        height: '100%',
        position: 'relative',
    },
    title: {
        color: theme.lightText,
        paddingVertical: 10,
    },
    price: {
        paddingVertical: 10,
        paddingHorizontal: 30,
        alignSelf: 'center',
        borderWidth: 3,
        borderRadius: 50,
        borderColor: theme.border
    },
    amount: {
        color: theme.lightText,
        marginBottom: 0,
    },
    label: {
        color: theme.lightText,
        marginBottom: 0,
    },
});

export default PackageItem;