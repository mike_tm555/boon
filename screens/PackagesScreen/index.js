import React, {useEffect, useState} from 'react';
import Screen from '../../components/Screen';
import Addresses from "./addresses";
import Packages from "./packages";
import useRequest from "../../hooks/useRequest";
import {connect} from "react-redux";
import Content from "../../components/Content";
import {ActivityIndicator, StyleSheet} from "react-native";
import Empty from "./empty";
import Routing from "../../navigation/Routing";
import useTheme from "../../hooks/useTheme";
import I18n from '../../utils/i18n';

const theme = useTheme();

function getAddresses(items) {
    const addresses = {};

    items.forEach((item) => {
        if (item.address) {
            if (!addresses[item.address.id]) {
                addresses[item.address.id] = {
                    address: item.address,
                    activePackage: I18n.t('noActivePackage'),
                };
            }

            if (item.active) {
                addresses[item.address.id].activePackage = item.name;
            }
        }
    });

    return addresses;
}

const PackagesScreen = ({ navigation, route, user, token, packages }) => {
    const [loading, setLoading] = useState(false);
    const request = useRequest();

    useEffect(() => {
        setLoading(true);
        request.getPackages().then(() => {
            setLoading(false)
        })
    }, []);

    let content = null;

    if (loading) {
        content = (
            <Content>
                <ActivityIndicator
                    size="large"
                    color={styles.indicator.color}
                    style={styles.indicator}
                />
            </Content>
        );
    } else if (packages.length) {
        if (route.name === Routing.packages.screens.addresses.name) {
            const addresses = getAddresses(packages);
            const addressesKeys = Object.keys(addresses);
            if (addressesKeys.length === 1) {
                content = (
                    <Packages
                        user={user}
                        token={token}
                        navigation={navigation}
                        packages={packages}
                        route={{
                            params: {
                                address: addresses[addressesKeys[0]].address
                            }
                        }}
                    />
                )
            } else {
                content = (
                    <Addresses
                        user={user}
                        token={token}
                        navigation={navigation}
                        route={route}
                        addresses={addresses}
                    />
                )
            }
        } else if (route.name === Routing.packages.screens.packages.name) {
            content = (
                <Packages
                    user={user}
                    token={token}
                    navigation={navigation}
                    packages={packages}
                    route={route}
                />
            );
        }
    } else {
        content = (
            <Content>
                <Empty navigation={navigation}/>
            </Content>
        )
    }

    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            {content}
        </Screen>
    );
};

const styles = StyleSheet.create({
    indicator: {
        marginTop: 50,
        color: theme.indicator
    },
});


const mapStateToProps = (state) => {
    return {
        packages: state.packages,
    };
};

export default connect(mapStateToProps)(PackagesScreen);
