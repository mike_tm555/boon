import React, {useState} from "react";
import {StyleSheet} from "react-native";
import { vh, vw } from 'react-native-expo-viewport-units';
import {Text, View} from "../../components/Themed";
import Slider from "../../components/Slider";
import {Button} from "../../components/Button";
import useTheme from "../../hooks/useTheme";
import PackageItem from './package-item';
import I18n from "../../utils/i18n";
import Content from "../../components/Content";
import Confirm from "../../components/Confirm";
import useRequest from "../../hooks/useRequest";

const theme = useTheme();

const Packages = ({ route, packages}) => {
    const request = useRequest();

    const [showUpdate, setShowUpdate] = useState(false);

    const items = packages.filter((item) => {
        return (
            item.address.id === route.params.address.id
        )
    });

    const [activeSlide, setActiveSlide] = useState(0);

    const changeSlide = (index) => {
        setActiveSlide(index);
    };

    const openUpdate = () => {
        setShowUpdate(true);
    }

    const closeUpdate = () => {
        setShowUpdate(false);
    }

    const updatePackage = async () => {
        const updateData = {
            packageId: items[activeSlide].packageId,
            flatId: route.params.address.id,
        };

        const response = await request.updatePackage(updateData);

        if (response) {
            closeUpdate();
        }
    }

    return (
        <Content space={null}>
            <View style={styles.container}>
                <Confirm
                    visible={showUpdate}
                    cancel={closeUpdate}
                    confirm={updatePackage}
                    cancelTitle={I18n.t('no')}
                    confirmTitle={I18n.t('yes')}
                >
                    <Text type="h1">{I18n.t('packages.update.title')}</Text>
                    <Text type="h5">
                        {I18n.t('packages.update.description', {
                            package: items[activeSlide].name
                        })}
                    </Text>
                </Confirm>

                <Text type="h1">{I18n.t('packages.title')}</Text>
                <Text type="h5">{I18n.t('packages.description')}</Text>
                <View style={styles.sliderContainer}>
                    <Slider
                        data={items}
                        itemWidth={vw(60)}
                        pagination={(items.length > 1)}
                        layoutCardOffset={30}
                        inactiveSlideOp
                        autoplay={false}
                        loop={true}
                        changeSlide={changeSlide}
                        layout="stack"
                        renderItem={({item, index}) => {
                            return (
                                <PackageItem
                                    key={index}
                                    {...item}
                                />
                            )
                        }}
                    />
                </View>
                <View
                    style={styles.actions}
                >
                    {(items[activeSlide].isActive) ? (
                        <Button
                            style={{
                                ...styles.actionButton,
                                ...styles.actionActive
                            }}
                            type="simple"
                            title={"Active"}
                            disabled={true}
                        />
                    ) : (
                        <Button
                            style={styles.actionButton}
                            type="action"
                            title={"Select"}
                            onPress={openUpdate}
                        />
                    )}
                </View>
            </View>
        </Content>
    )
};


const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 30,
    },
    sliderContainer: {
        width: '100%',
        height: vh(60),
        marginTop: vh(3),
        position: 'relative',
    },
    actions: {
        width: '100%',
        height: vh(10),
        marginTop: -10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    actionButton: {
        minWidth: 150,
        height: 50,
        borderRadius: 50,
        paddingVertical: 0,
    },
    actionActive: {
        borderWidth: 2,
        borderColor: theme.success
    }
});

export default Packages;

