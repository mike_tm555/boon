import React from "react";
import {StyleSheet} from "react-native";
import AddressItem from "./address-item";
import {Text, View} from "../../components/Themed";
import I18n from "../../utils/i18n";
import Content from "../../components/Content";

const Addresses = ({ navigation, addresses }) => {
    const addressesKeys = Object.keys(addresses);

    console.log(addresses);
    return (
        <Content>
            <View style={styles.container}>
            <View style={styles.header}>
                <Text type="caption">{I18n.t('addresses')}</Text>
            </View>
            {addressesKeys.map(key => {
                const item = addresses[key];
                return (
                    <AddressItem
                        key={key}
                        navigation={navigation}
                        {...item}
                    />
                )
            })}
            </View>
        </Content>
    )
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 20,
    },
    header: {
        marginBottom: 20,
    }
});

export default Addresses;
