import React from "react";
import {View} from "../../components/Themed";
import {StyleSheet} from "react-native";
import {Text} from "../../components/Themed";
import Assets from "../../constants/definitions/Assets";
import I18n from '../../utils/i18n';
import {SvgIcon} from "../../components/SvgIcon";
import Routing from "../../navigation/Routing";
import {Button} from "../../components/Button";

export default function ({ navigation }) {
    const addDevice = () => {
        navigation.navigate(Routing.linkDevice.name, {
            screen: Routing.linkDevice.screens.intercom.name
        });
    };

    return (
        <View style={styles.container}>
            <SvgIcon
                icon={Assets.cameras.emptyCameras}
                style={styles.icon}
            />
            <Text type="h1">{I18n.t('people.empty.title')}</Text>
            <Text type="h5">{I18n.t('people.empty.description')}</Text>
            <Button
                type="action"
                title={I18n.t('addDevice')}
                onPress={addDevice}
            >
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:'100%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        paddingVertical: 20,
    },
    icon:{
        marginBottom: 50,
        resizeMode: "cover",
        height: 120,
        width: '100%',
    },
    button:{
        marginVertical: 10
    }
});
