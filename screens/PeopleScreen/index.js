import React, {useEffect, useState} from 'react';
import Screen from '../../components/Screen';
import Addresses from "./addresses";
import Users from "./users";
import useRequest from "../../hooks/useRequest";
import {connect} from "react-redux";
import Content from "../../components/Content";
import {ActivityIndicator, StyleSheet} from "react-native";
import Empty from "./empty";
import Routing from "../../navigation/Routing";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

function getAddresses(items) {
    const addresses = {};

    items.forEach((item) => {
        if (!addresses[item.address.id]) {
            addresses[item.address.id] = {
                address: item.address,
                activeUsers: 0,
            };
        }

        if (item.isActive) {
            addresses[item.address.id].activeUsers += 1;
        }
    });

    return addresses;
}

const PeopleScreen = ({ navigation, route, user, token, people }) => {
    const [loading, setLoading] = useState(false);
    const request = useRequest();

    useEffect(() => {
        setLoading(true);
        request.getPeople().then(() => {
            setLoading(false)
        })
    }, []);

    let content = null;

    if (loading) {
        content = (
            <Content>
                <ActivityIndicator
                    size="large"
                    color={styles.indicator.color}
                    style={styles.indicator}
                />
            </Content>
        );
    } else if (people.length) {
        if (route.name === Routing.people.screens.addresses.name) {
            const addresses = getAddresses(people);
            const addressesKeys = Object.keys(addresses);
            if (addressesKeys.length === 1) {
                content = (
                    <Users
                        user={user}
                        token={token}
                        navigation={navigation}
                        people={people}
                        route={{
                            params: {
                                address: addresses[addressesKeys[0]].address
                            }
                        }}
                    />
                )
            } else {
                content = (
                    <Addresses
                        user={user}
                        token={token}
                        navigation={navigation}
                        route={route}
                        addresses={addresses}
                    />
                )
            }
        } else if (route.name === Routing.people.screens.users.name) {
            content = (
                <Users
                    user={user}
                    token={token}
                    navigation={navigation}
                    people={people}
                    route={route}
                />
            );
        }
    } else {
        content = (
            <Content>
                <Empty navigation={navigation}/>
            </Content>
        )
    }

    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            {content}
        </Screen>
    );
};

const styles = StyleSheet.create({
    indicator: {
        marginTop: 50,
        color: theme.indicator
    },
});


const mapStateToProps = (state) => {
    return {
        people: state.people,
    };
};

export default connect(mapStateToProps)(PeopleScreen);
