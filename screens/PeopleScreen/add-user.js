import React, {useRef, useState} from "react";
import { StyleSheet } from "react-native";
import {View} from "../../components/Themed";
import {Button} from "../../components/Button";
import I18n from "../../utils/i18n";
import Generator from "../../components/generator";
import useRequest from "../../hooks/useRequest";

export default function ({ flatId }) {
    const [code, setCode] = useState(null);
    const generatorRef = useRef();
    const request = useRequest();

    const addPeople = () => {
        generatorRef.current?.open();
    };

    const generateCode = async () => {
        const data = {
            flatId: flatId
        };

        const response = await request.generateCode(data);

        if (response) {
            setCode(response.data);
        }

        return response;
    };

    return (
        <View style={styles.container}>
            <Button
                type="action"
                icon={{
                    name: 'add-circle'
                }}
                title={I18n.t('addPeople')}
                onPress={addPeople}
            />
            <Generator
                generatorRef={generatorRef}
                title={I18n.t('people.add.title')}
                description={I18n.t('people.add.description')}
                generate={generateCode}
                code={code}
            />
        </View>
    );
}

const styles = StyleSheet.create({
   container: {
       width: '100%',
   }
});
