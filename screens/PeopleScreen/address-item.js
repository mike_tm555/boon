import React from "react";
import {StyleSheet} from "react-native";
import Routing from "../../navigation/Routing";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";
import {Text, View} from "../../components/Themed";
import I18n from "../../utils/i18n";
import ListItem from "../../components/ListItem";

const AddressItem = (props) => {
    const {
        id,
        address,
        activeUsers,
        navigation
    } = props;

    const openPeople = (address) => {
        navigation.navigate(Routing.people.name, {
            screen: Routing.people.screens.users.name,
            params: {
                address: address
            }
        })
    };

    return (
        <ListItem
            press={() => {
                openPeople(address);
            }}
        >
            <View style={styles.container}>
                <SvgIcon
                    style={styles.icon}
                    icon={Assets.people.icons.address}
                />
                <View style={styles.info}>
                    <Text type="title">
                        {I18n.t('fullAddress', address)}
                    </Text>
                    <Text type="caption">
                        {I18n.t('activeUsers', {users: activeUsers})}
                    </Text>
                </View>
            </View>
        </ListItem>
    )
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        width: '30%',
        height: 60,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    info: {
        width: '70%',
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: 10,
    }
});

export default AddressItem;
