import React from "react";
import {StyleSheet} from "react-native";
import Content from "../../components/Content";
import {connect} from "react-redux";
import I18n from "../../utils/i18n";
import {View, Text} from "../../components/Themed";
import UserItem from "./user-item";
import AddUser from './add-user';
import OrderChip from './order-chip';

const Users = ({ navigation, route, people, user }) => {
    const users = people.filter((item) => {
        return (
            item.address.id === route.params.address.id
        )
    }).sort((prev, next) => {
        if (next.isAdmin) {
            return 1;
        } else {
            return -1;
        }
    });

    const checkMeForAdmin = (id, users) => {
        const me = users.find(item => item.userId === id);

        if (me) {
            return me.isAdmin;
        }

        return false;
    };


    const isIAdmin = checkMeForAdmin(user.id, users);

    return (
        <Content
            footer={(
                <View style={styles.footer}>
                    {isIAdmin && (
                        <AddUser flatId={route.params.address.id}/>
                    )}
                    <OrderChip flatId={route.params.address.id}/>
                </View>
            )}
        >
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text type="caption">
                        {I18n.t('fullAddress', route.params.address)}
                    </Text>
                </View>
                {users.map((userItem, index) => {
                    return (
                        <UserItem
                            key={index}
                            navigation={navigation}
                            flatId={route.params.address.id}
                            isIAdmin={isIAdmin}
                            {...userItem}
                        />
                    )
                })}
            </View>
        </Content>
    )
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 20,
    },
    header: {
        marginBottom: 20,
    },
    footer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    }
});


const mapStateToProps = (state) => {
    return {
        people: state.people,
        user: state.user,
    };
};

export default connect(mapStateToProps)(Users);
