import React, {useRef} from "react";
import {StyleSheet} from "react-native";
import {ImageBackground, Tooltip, View} from "../../components/Themed";
import {Text} from "../../components/Themed";
import I18n from '../../utils/i18n';
import Shadows from "../../constants/styles/Shadows";
import Separator from "../../components/Separator";
import useTheme from "../../hooks/useTheme";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";
import {Button} from "../../components/Button";
import useRequest from "../../hooks/useRequest";
import ListItem from "../../components/ListItem";
import Routing from "../../navigation/Routing";
import {connect} from "react-redux";

const theme = useTheme();

const  UserItem = (props) => {
    const {
        isIAdmin,
        userId,
        flatId,
        isActive,
        isAdmin,
        name,
        avatarUrl,
        navigation,
    } = props;

    const tooltipRef = useRef(null);
    const request = useRequest();

    const disableUser = () => {
        request.disableUser({
            flatId: flatId,
            userId: userId,
        }).then((response) => {
            tooltipRef.current?.toggleTooltip();
        });
    };

    const enableUser = () => {
        request.enableUser({
            flatId: flatId,
            userId: userId,
        }).then((response) => {
            tooltipRef.current?.toggleTooltip();
        });
    };

    const deleteUser = () => {
        request.deleteUser({
            flatId: flatId,
            userId: userId,
        }).then((response) => {
            tooltipRef.current?.toggleTooltip();
        });
    };

    const openActivity = () => {
        tooltipRef.current?.toggleTooltip();
        navigation.navigate(Routing.history.name, {
            params: {
                userId: userId,
                flatId: flatId
            }
        })
    };

    let typeStyle = null;

    if (isAdmin) {
        typeStyle = styles.admin
    }

    const showActions = (!isAdmin && isIAdmin);

    return (
        <ListItem style={typeStyle}>
            <View style={styles.container}>
                {avatarUrl ? (
                    <ImageBackground
                        style={styles.icon}
                        source={{
                            uri: avatarUrl
                        }}
                    />
                ) : (
                    <ImageBackground
                        style={styles.icon}
                        source={Assets.icons.avatar}
                    />
                )}
                <View style={styles.info}>
                    <Text type="title">{name}</Text>
                    {isActive ? (
                        <Text type="caption" style={styles.active}>{I18n.t('active')}</Text>
                    ) : (
                        <Text type="caption" style={styles.inactive}>{I18n.t('inactive')}</Text>
                    )}
                </View>

                {showActions && (
                    <Tooltip
                        tooltipRef={tooltipRef}
                        popover={(
                            <View style={styles.actionsMenu}>
                                {isActive ? (
                                    <Button
                                        style={styles.actionButton}
                                        type="simple"
                                        title={I18n.t('disable')}
                                        icon={{
                                            name: 'lightbulb-off',
                                            type: 'material-community'
                                        }}
                                        onPress={disableUser}
                                    />
                                ) : (
                                    <Button
                                        style={styles.actionButton}
                                        type="simple"
                                        title={I18n.t('enable')}
                                        icon={{
                                            name: 'lightbulb-on',
                                            type: 'material-community'
                                        }}
                                        onPress={enableUser}
                                    />
                                )}

                                <Separator type="horizontal" size="small"/>
                                <Button
                                    style={styles.actionButton}
                                    type="simple"
                                    title={I18n.t('activity')}
                                    icon={{
                                        name: 'history',
                                        type: 'font-awesome'
                                    }}
                                    onPress={openActivity}
                                />

                                <Separator type="horizontal" size="small"/>
                                <Button
                                    style={styles.actionButton}
                                    type="simple"
                                    title={I18n.t('delete')}
                                    icon={{
                                        name: 'times-circle-o',
                                        type: 'font-awesome'
                                    }}
                                    onPress={deleteUser}
                                />
                            </View>
                        )}
                    >
                        <View style={styles.actions}>
                            <SvgIcon
                                value={false}
                                style={styles.dots}
                                icon={Assets.icons.dots}
                            />
                        </View>
                    </Tooltip>
                )}
            </View>
        </ListItem>
    )
};

const styles = StyleSheet.create({
    admin: {
        borderWidth: 2,
        borderColor: theme.primary,
    },
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        width: 60,
        height: 60,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    info: {
        width: '40%',
        flex: 1,
        marginLeft: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'left',
    },
    active: {
        color: theme.success
    },
    inactive: {
        opacity: 0.8
    },
    actions: {
        width: 50,
        height: 50,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dots: {
        width: 5,
        height: 20,
    },
    actionsMenu: {
        width: '100%',
        paddingHorizontal: 10,
        ...Shadows.default,
    },
    actionButton: {
        width: '100%',
        paddingVertical: 8,
        marginVertical: 0,
        color: theme.lightText,
        justifyContent: 'flex-start'
    }
});

export default UserItem;
