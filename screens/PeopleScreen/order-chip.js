import React, {useState} from "react";
import { StyleSheet } from "react-native";
import {Text, View} from "../../components/Themed";
import {Button} from "../../components/Button";
import I18n from "../../utils/i18n";
import useRequest from "../../hooks/useRequest";
import useTheme from "../../hooks/useTheme";
import Confirm from "../../components/Confirm";
import Success from "../../components/Success";

const theme = useTheme();

export default function ({ flatId }) {
    const [showOrderDialog, setShowOrderDialog] = useState(false);
    const [showSuccessDialog, setShowSuccessDialog] = useState(false);
    const request = useRequest();

    const openOrderChip = () => {
        setShowOrderDialog(true);
    };

    const closeOrderChip = () => {
        setShowOrderDialog(false);
    };

    const closeOrderSuccess = () => {
        setShowSuccessDialog(false);
    };

    const orderChip = async () => {
        setShowOrderDialog(false);
        const response = await request.orderChip(flatId);

        if (response !== false) {
            setShowSuccessDialog(true);
        }
    };

    return (
        <View style={styles.container}>
            <Button
                style={styles.order}
                type="light"
                icon={{
                    name: "microchip",
                    type: "font-awesome"
                }}
                title={I18n.t('orderChip.title')}
                onPress={openOrderChip}
            />
            <Confirm
                visible={showOrderDialog}
                close={closeOrderChip}
                confirm={orderChip}
                cancelTitle={I18n.t('no')}
                confirmTitle={I18n.t('yes')}
            >
                <Text type="h1">{I18n.t('orderChip.title')}</Text>
                <Text type="h5">{I18n.t('orderChip.description')}</Text>
            </Confirm>
            <Success
                visible={showSuccessDialog}
                close={closeOrderSuccess}
                onRequestClose={closeOrderSuccess}
                buttonTitle={I18n.t('ok')}
            >
                <Text type="h1">{I18n.t('orderChip.title')}</Text>
                <Text type="h5">{I18n.t('orderChip.success')}</Text>
            </Success>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    order: {
        borderWidth: 2,
        borderColor: theme.borderButton.border,
        color: theme.borderButton.color,
        backgroundColor: theme.borderButton.bg,
    }
});
