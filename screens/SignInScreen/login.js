import React, {useState} from 'react';
import { StyleSheet } from "react-native";
import I18n from '../../utils/i18n';
import Form from "../../components/Form";
import {Text, View} from "../../components/Themed";
import useRequest from "../../hooks/useRequest";
import Content from "../../components/Content";
import FBLoginButton from "../../components/FBLoginButton";
import Routing from "../../navigation/Routing";
import {SIGN_IN_FORM} from "../../constants/definitions/Forms";

const Login = ({ navigation }) => {
    const [form] = useState(SIGN_IN_FORM);
    const request = useRequest();

    const submitForm = async (data) => {
        const phoneNumber = data.phone.dialCode + data.phone.phoneNumber;
        const response = await request.otp({
            phoneNumber: phoneNumber
        });

        if (response && response.data.code) {
            return navigation.navigate(Routing.signIn.name, {
                screen: Routing.signIn.screens.activate.name,
                params: {
                    phoneNumber: phoneNumber,
                    code: response.data.code
                }
            });
        }

        return response;
    };

    const FBLogin = async (data, token) => {
        const response = await request.loginWithFacebook({
            avatarUrl: data.picture.url,
            email: data.email,
            facebookId: data.id,
            name: data.name,
            phoneNumber: data.phoneNumber
        }, token);

        if (response.data.user && response.data.token) {
            navigation.navigate(Routing.linkDevice.name, {
                screen: Routing.linkDevice.screens.intercom.name
            });
        }
    };

    return (
        <Content>
            <View style={styles.container}>
                <Text type="h1">{I18n.t('login.title')}</Text>
                <Text type="h5">{I18n.t('login.description')}</Text>
                <Form
                    submit={submitForm}
                    form={form}
                />
                <Text style={styles.separator}>{I18n.t('or')}</Text>
                <FBLoginButton onPress={FBLogin}/>
            </View>
        </Content>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 30,
    },
    separator: {
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 20,
    }
});

export default Login;
