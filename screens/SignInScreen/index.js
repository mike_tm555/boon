import React from 'react';
import Screen from '../../components/Screen';
import Activate from "./activate";
import Login from "./login";
import {connect} from "react-redux";

const CONTENT_COMPONENTS = {
    activate: Activate,
    login: Login,
};

const SignInScreen = ({ navigation, route, user, token }) => {
    const ContentComponent = CONTENT_COMPONENTS[route.name];

    return (
        <Screen
            title={route.params.title}
            navigation={navigation}
        >
            <ContentComponent
                user={user}
                token={token}
                navigation={navigation}
                route={route}
            />
        </Screen>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

export default connect(mapStateToProps)(SignInScreen);
