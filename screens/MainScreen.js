import React, {useRef} from 'react';
import { StyleSheet } from 'react-native';
import {View} from "../components/Themed";
import {Button} from "../components/Button";
import I18n from '../utils/i18n';
import Assets from "../constants/definitions/Assets";
import {connect} from "react-redux";
import Routing from "../navigation/Routing";
import {SvgIcon} from "../components/SvgIcon";
import Languages from "../components/Languages";
import useTheme from "../hooks/useTheme";
import ScaleAnimate from "../components/Animations/Scale";
import Gradient from "../components/Gradient";

const theme = useTheme();

const MainScreen = ({ navigation }) => {
    const languageSheetRef = useRef();

    return (
        <Gradient
            colors={theme.main}
            style={styles.container}
        >
            <SvgIcon
                style={styles.logo}
                icon={Assets.patterns.logoLight}
            />
            <ScaleAnimate>
                <View style={styles.buttons}>
                    <Button
                        type='light'
                        title={I18n.t("getStarted")}
                        onPress={() =>
                            navigation.navigate(Routing.authorization.name, {
                                screen: Routing.authorization.screens.authorize.name
                            })
                        }
                    />
                </View>
            </ScaleAnimate>
            <ScaleAnimate>
                <Button
                    style={styles.languageButton}
                    type="simple"
                    title={I18n.t("selectLanguage")}
                    icon={{
                        name: 'language'
                    }}
                    onPress={() => {
                        languageSheetRef.current?.open();
                    }}
                />
            </ScaleAnimate>

            <Languages
                languages={I18n.languages}
                langRef={languageSheetRef}
            />
        </Gradient>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
    },
    logo: {
        width: '100%',
        height: 80,
    },
    buttons: {
        marginTop: 150,
        marginBottom: 20,
    },
    languageButton: {
        color: theme.lightText
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

export default connect(mapStateToProps)(MainScreen);
