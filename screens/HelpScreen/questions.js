import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {ActivityIndicator, StyleSheet} from "react-native";
import useRequest from "../../hooks/useRequest";
import {Text, View} from "../../components/Themed";
import Wrapper from "../../components/Wrapper";
import Layout from "../../constants/styles/Layout";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

const Questions = ({ appInfo }) => {
    const [loading, setLoading] = useState(false);
    const request = useRequest();

    useEffect(() => {
        if  (!appInfo.questions) {
            setLoading(true);
            request.getAppInfo().then(() => {
                setLoading(false);
            });
        }
    }, []);

    return (
        <Wrapper>
            {loading ? (
                <View style={styles.container}>
                    <ActivityIndicator
                        size="large"
                        color={styles.indicator.color}
                        style={styles.indicator}
                    />
                </View>
            ) : (
                <View style={styles.container}>
                    {appInfo.questions.map((question, index) => {
                        return (
                            <View
                                key={index}
                                style={styles.item}
                            >
                                <Text type="title">{question.question}</Text>
                                <Text type="p">{question.answer}</Text>
                            </View>
                        )
                    })}
                </View>
            )}
        </Wrapper>
    )
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 20,
    },
    item: {
        marginBottom: Layout.listItem.marginBottom,
    },
    indicator: {
        marginTop: 50,
        color: theme.indicator
    }
});

const mapStateToProps = (state) => {
    return {
        appInfo: state.appInfo,
    };
};

export default connect(mapStateToProps)(Questions);

