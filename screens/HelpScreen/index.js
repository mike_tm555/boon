import React from 'react';
import Screen from '../../components/Screen';
import Questions from "./questions";
import Support from "./support";

const CONTENT_COMPONENTS = {
    support: Support,
    questions: Questions,
};

const HelpScreen = ({ navigation, route, user, token }) => {
    const ContentComponent = CONTENT_COMPONENTS[route.name];
    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            <ContentComponent
                user={user}
                token={token}
                navigation={navigation}
                route={route}
            />
        </Screen>
    );
};

export default HelpScreen;
