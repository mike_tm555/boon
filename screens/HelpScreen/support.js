import React, {useEffect, useState} from "react";
import {ActivityIndicator} from "react-native";
import {Text, View} from "../../components/Themed";
import Separator from "../../components/Separator";
import Form from '../../components/Form'
import {connect} from "react-redux";
import useTheme from "../../hooks/useTheme";
import {Button} from "../../components/Button";
import Routing from "../../navigation/Routing";
import useRequest from "../../hooks/useRequest";
import I18n from '../../utils/i18n';
import Wrapper from "../../components/Wrapper";
import {SUPPORT_FORM} from "../../constants/definitions/Forms";
import Success from "../../components/Success";

const theme = useTheme();

const Support = ({navigation, appInfo}) => {
    const [loading, setLoading] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [form, setForm] = useState(JSON.parse(JSON.stringify(SUPPORT_FORM)));
    const request = useRequest();

    useEffect(() => {
        if (!appInfo) {
            setLoading(true);
            request.getAppInfo().then(() => {
                setLoading(false);
            });
        }
    }, [appInfo]);

    const onSubmit = async (data) => {
        const response = await request.contactUs(data);

        if (response) {
            setForm(JSON.parse(JSON.stringify(SUPPORT_FORM)))
        }

        if (response) {
            setShowSuccess(true);
        }

        return response;
    };

    const closeShowSuccess = () => {
        setShowSuccess(false);
    }

    return (
        <Wrapper>
            {loading ? (
                <View style={styles.container}>
                    <ActivityIndicator
                        size="large"
                        color={styles.indicator.color}
                        style={styles.indicator}
                    />
                </View>
            ) : (
                <View style={styles.container}>
                    <Text type="h1">{I18n.t('support.title')}</Text>
                    <Separator type="horizontal" size="large"/>
                    <Form
                        form={form}
                        submit={onSubmit}
                    />
                    <View style={styles.contacts}>
                        <Button
                            type="simple"
                            style={styles.item}
                            title={I18n.t('faq')}
                            icon={{
                                name: 'question-circle-o',
                                type: 'font-awesome'
                            }}
                            onPress={() => {
                                navigation.navigate(Routing.help.name, {
                                    screen: Routing.help.screens.questions.name
                                });
                            }}
                        />
                        <Button
                            type="simple"
                            disabled={true}
                            style={styles.item}
                            title={appInfo.address}
                            icon={{
                                name: 'map-marker',
                                type: 'font-awesome'
                            }}
                        />
                        <Button
                            type="simple"
                            disabled={true}
                            style={styles.item}
                            title={appInfo.phoneNumber}
                            icon={{
                                name: 'phone',
                                type: 'font-awesome'
                            }}
                        />
                        <Button
                            type="simple"
                            disabled={true}
                            style={styles.item}
                            title={appInfo.email}
                            icon={{
                                name: 'envelope',
                                type: 'font-awesome'
                            }}
                        />
                    </View>

                    <Success
                        visible={showSuccess}
                        close={closeShowSuccess}
                        onRequestClose={closeShowSuccess}
                        buttonTitle={I18n.t('ok')}
                    >
                        <Text type="h1">{I18n.t('contactUs.title')}</Text>
                        <Text type="h5">{I18n.t('contactUs.success')}</Text>
                    </Success>
                </View>
            )}
        </Wrapper>
    )
};

const styles = {
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 20,
        paddingHorizontal: 10,
    },
    indicator: {
        marginTop: 50,
        color: theme.indicator
    },
    contacts: {
        width: '100%',
        flexDirection: 'column',
        marginTop: 20,
    },
    item: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 13,
        fontWeight: 'bold',
        size: 22,
        paddingVertical: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: theme.separator,
    },
};

const mapStateToProps = (state) => {
    return {
        appInfo: state.appInfo,
    };
};

export default connect(mapStateToProps)(Support);

