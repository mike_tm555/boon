import React, {useEffect, useState} from 'react';
import { vh } from 'react-native-expo-viewport-units';
import Assets from "../../constants/definitions/Assets";
import {ImageBackground, View} from '../../components/Themed';
import Screen from '../../components/Screen';
import Menu from "../../constants/definitions/Menu";
import Card from "../../components/Card";
import {connect} from "react-redux";
import Slider from "../../components/Slider";
import Shadows from "../../constants/styles/Shadows";
import Profile from "./profile";
import useTheme from "../../hooks/useTheme";
import SideBar from "../../components/SideBar";
import Routing from "../../navigation/Routing";
import {Button, SvgButton} from "../../components/Button";
import useRequest from "../../hooks/useRequest";
import {ActivityIndicator, Linking, TouchableOpacity} from "react-native";

const theme = useTheme();

const HomeScreen = ({navigation, route, user, appInfo}) => {
    const [menu] = useState(Menu);
    const [sideBarOpened, setSideBarOpened] = useState(false);
    const [loading, setLoading] = useState(false);
    const request = useRequest();
    const tweenDuration = 250;

    useEffect(() => {
        if (!appInfo || !appInfo.slides) {
            setLoading(true);
            request.getAppInfo().then(() => {
                setLoading(false);
            })
        }
    }, []);

    const openPage = (page) => {
        if (page.screen) {
            navigation.navigate(page.route, {
                screen: page.screen
            });
        } else {
            navigation.navigate(page.route);
        }
    };

    let  bottomComponent = null;

    if (loading) {
        bottomComponent = (
            <View style={styles.sliderContainer}>
                <ActivityIndicator
                    color={styles.indicator.color}
                    style={styles.indicator}
                    size="large"
                />
            </View>
        )
    } else if (appInfo && appInfo.slides && appInfo.slides.length) {
        const showPagination = appInfo.slides.length > 1;
        const sliderStyles = [styles.sliderContainer];

        if (!showPagination) {
            sliderStyles.push(styles.imageContainer)
        }

        bottomComponent = (
            <View style={styles.sliderContainer}>
                <Slider
                    pagination={showPagination}
                    data={appInfo.slides}
                    space={30}
                    loop={true}
                    renderItem={({item, index}) => {
                        return (
                            <TouchableOpacity
                                style={styles.sliderItem}
                                onPress={() => {
                                    if (item.link) {
                                        Linking.openURL(item.link);
                                    }
                                }}
                            >
                                <ImageBackground
                                    key={index}
                                    style={styles.sliderImage}
                                    imageStyle={styles.sliderImage}
                                    source={{
                                        uri: item.imageUrl
                                    }}
                                />
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>
        )
    }

    return (
        <SideBar
            open={sideBarOpened}
            tweenDuration={tweenDuration}
            content={(user && (
                <Profile
                    user={user}
                    navigation={navigation}
                    route={route}
                    onNavigate={(goToPage = null) => {
                        setSideBarOpened(false);

                        if (typeof goToPage === 'function') {
                            setTimeout(() => {
                                goToPage();
                            }, tweenDuration * 2);
                        }
                    }}
                />
            ))}
            onClose={() => {
                setSideBarOpened(false);
            }}
        >
            <Screen
                image={Assets.patterns.logoLight}
                rightComponent={(
                    <SvgButton
                        style={styles.messages}
                        icon={Assets.icons.messages}
                        onPress={() => {
                            navigation.navigate(Routing.notifications.name, {
                                screen: Routing.notifications.screens.list.name
                            })
                        }}
                    />
                )}
                leftComponent={(
                    <Button
                        style={styles.menu}
                        type="simple"
                        icon={{
                            name: 'menu'
                        }}
                        onPress={() => {
                            setSideBarOpened(true);
                        }}
                    />
                )}
                bottomComponent={bottomComponent}
                navigation={navigation}
            >
                <View style={styles.menuWrapper}>
                    <View style={styles.menuContainer}>
                        {Object.keys(menu).map(key => {
                            const menuItem = menu[key];
                            if (menuItem.visible) {
                                return (
                                    <View
                                        key={key}
                                        style={styles.menuItemWrapper}
                                    >
                                        <Card
                                            type="light"
                                            onPress={openPage}
                                            icon={menuItem.icon}
                                            title={menuItem.title}
                                            navigation={menuItem.navigation}>
                                        </Card>
                                    </View>
                                )
                            }
                        })}
                    </View>
                </View>
            </Screen>
        </SideBar>
    );
};

const styles = {
    sliderContainer: {
        width: '100%',
        height: vh(35),
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'center',
        zIndex: 1,
    },
    imageContainer: {
        height: vh(30),
    },
    indicator: {
        color: theme.indicator
    },
    sliderItem: {
        width: '100%',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: theme.content,
        backgroundColor: theme.content,
        zIndex: 1,
        ...Shadows.default,
    },
    sliderImage: {
        width: '100%',
        height: '100%',
        position: 'relative',
        resizeMode: "cover",
    },
    menuWrapper: {
        width: '100%',
        height: '100%',
        position: 'relative',
    },
    menuContainer: {
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        position: 'relative',
    },
    menuItemWrapper: {
        maxWidth: 160,
        maxHeight: 160,
        minWidth: 100,
        minHeight: 100,
        width: '50%',
        height: '50%',
        flexDirection: 'row',
        position: 'relative',
        padding: vh(1.5),
    },
    messages: {
        width: 40,
        height: 40,
        marginTop: 60,
        marginRight: 10,
        position: 'relative',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: theme.headerIcon,
        ...Shadows.dark,
    },
    menu: {
        width: 60,
        height: 60,
        marginTop: 60,
        marginLeft: 10,
        position: 'relative',
        color: theme.lightText,
        size: 30,
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
        appInfo: state.appInfo
    };
};

export default connect(mapStateToProps)(HomeScreen);
