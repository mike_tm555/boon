import React, {useState} from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import {View, BlurView, Text} from "../../components/Themed";
import ProfileMenu from "../../constants/definitions/Profile";
import Content from "../../components/Content";
import useRequest from "../../hooks/useRequest";
import Routing from "../../navigation/Routing";
import {SvgIcon} from "../../components/SvgIcon";
import Separator from "../../components/Separator";
import useTheme from "../../hooks/useTheme";
import I18n from '../../utils/i18n';
import {Button} from "../../components/Button";

const theme = useTheme();

export default function Profile({ navigation, user, onNavigate }) {
    const [menu] = useState(ProfileMenu);
    const request = useRequest();
    const  menuKeys = Object.keys(menu);

    const actions = {
        signOut: async () => {
            await request.signOut();
            navigation.navigate(Routing.main.name);
        }
    };

    const profileAction = (page) => {
        if (typeof onNavigate === 'function') {
            onNavigate(() => {
                if (page.action) {
                    actions[page.action]();
                } else if (page.route) {
                    navigation.navigate(page.route, {
                        screen: page.screen
                    });
                }
            });
        }
    };

    const menuItems = [];

    menuKeys.forEach((key, i) => {
        const section = menu[key];
        const itemKeys = Object.keys(section);

        itemKeys.map((itemKey, i) => {
            const item = section[itemKey];

            if (item.visible === true) {
                menuItems.push(
                    <TouchableOpacity
                        onPress={() => {
                            profileAction(item.navigation)
                        }}
                        style={styles.item}
                        key={key + itemKey}>
                        <SvgIcon
                            style={styles.icon}
                            icon={item.icon}
                            fill={styles.icon.fill}
                        />
                        <Text style={styles.title}>{item.title}</Text>
                    </TouchableOpacity>
                )
            }
        });

        if (i < menuKeys.length -1) {
            menuItems.push(
                <Separator key={key} type="horizontal"/>
            )
        }
    });

    return (
        <BlurView
            style={styles.wrapper}
        >
            <Content>
                <View style={styles.container}>
                    <Button
                        type="simple"
                        icon={{
                            name: 'times',
                            type: 'font-awesome'
                        }}
                        style={styles.close}
                        onPress={() => {
                            onNavigate();
                        }}
                    />
                    <Text
                        type="h1"
                        style={styles.header}
                    >
                        {I18n.t('hi', {name: user.name})}
                    </Text>
                    <View style={styles.menu}>
                        {menuItems}
                    </View>
                </View>
            </Content>
        </BlurView>
    )
}

const styles = {
    wrapper: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        paddingVertical: 0,
        backgroundColor: theme.sidebar,
    },
    container: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    close: {
        width: 28,
        height: 28,
        color: theme.lightText,
        size: 14,
        paddingVertical: 0,
        borderWidth: 2,
        borderColor: theme.lightText,
        alignSelf: 'flex-start',
        borderRadius: 5,
        marginLeft: 5,
        marginBottom: 20,
    },
    header: {
        padding: 10,
        textAlign: 'left',
        color: theme.lightText,
    },
    menu: {
        padding: 10,
    },
    item: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        marginBottom: 10,
    },
    title: {
        fontSize: 15,
        marginLeft: 20,
        color: theme.lightText,
    },
    icon: {
        width: 28,
        height: 28,
        fill: theme.lightText
    },
};
