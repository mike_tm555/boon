import React from 'react';
import Screen from '../components/Screen';

const AutomationScreen = ({ navigation, route }) => {
    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
        </Screen>
    );
};

export default AutomationScreen;
