import React, {useState} from 'react';
import Form from "../../components/Form";
import {ACCOUNT_FORM} from "../../constants/definitions/Forms";
import useRequest from "../../hooks/useRequest";
import Uploader from "../../components/Uploader";
import Assets from "../../constants/definitions/Assets";
import Content from "../../components/Content";
import {View} from "../../components/Themed";
import {StyleSheet} from "react-native";

const Account = ({ user }) => {
    const [form] = useState(ACCOUNT_FORM);
    const [changed, setChanged] = useState(false);
    const [avatarUrl, setAvatarUrl] = useState(user.avatarUrl);
    const request = useRequest();

    const submitForm = async (data) => {
        const userData = {
            name: data.name,
            email: data.email,
        };

        if (avatarUrl) {
            userData.avatarUrl = avatarUrl;
        }

        await request.updateUser(userData, user.id);

        setChanged(false);

        return data;
    };

    const uploadAvatar= async (uri) => {
        const response = await request.avatar({
            image: uri
        });

        setAvatarUrl(response.data.imageUrl);
        setChanged(true);

        return response.data.imageUrl;
    };

    return (
        <Content>
            <View style={styles.container}>
                <Uploader
                    gallery={true}
                    defaultImage={user.avatarUrl ? {uri: user.avatarUrl} : Assets.icons.avatar}
                    upload={uploadAvatar}
                />
                <Form
                    data={{
                        name: user.name,
                        email: user.email,
                    }}
                    changed={changed}
                    actions={{
                        upload: uploadAvatar
                    }}
                    submit={submitForm}
                    form={form}/>
            </View>
        </Content>
    );
};


const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 30,
    },
});

export default Account;
