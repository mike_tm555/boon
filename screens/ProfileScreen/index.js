import React from 'react';
import Screen from '../../components/Screen';
import Account from "./account";
import {connect} from "react-redux";

const PROFILE_COMPONENTS = {
    account: Account
};

const ProfileScreen = ({ navigation, route, user }) => {
    const ProfileComponent = PROFILE_COMPONENTS[route.name];
    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            <ProfileComponent
                user={user}
                route={route}
                navigation={navigation}
            />
        </Screen>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

export default connect(mapStateToProps)(ProfileScreen);
