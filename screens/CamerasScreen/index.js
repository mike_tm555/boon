import React from 'react';
import Screen from '../../components/Screen';
import List from "../CamerasScreen/list";

const CONTENT_COMPONENTS = {
    list: List,
};

const CamerasScreen = ({ navigation, route }) => {
    const ContentComponent = CONTENT_COMPONENTS[route.name];

    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            <ContentComponent
                navigation={navigation}
            />
        </Screen>
    );
};

export default CamerasScreen;
