import React, {useEffect, useState} from 'react';
import { ActivityIndicator, StyleSheet } from "react-native";
import {connect} from "react-redux";
import Empty from "./empty";
import Content from "../../components/Content";
import GridItem from './grid-item';
import SliderItem from './slider-item';
import useRequest from "../../hooks/useRequest";
import {View} from "../../components/Themed";
import Slider from "../../components/Slider";
import useTheme from "../../hooks/useTheme";
import {Button} from "../../components/Button";
import Separator from "../../components/Separator";
import I18n from '../../utils/i18n';

const theme = useTheme();

const List = ({ navigation, cameras }) => {
    const request = useRequest();
    const [loading, setLoading] = useState(false);
    const [gridView, setGridView] = useState(false);

    useEffect(() => {
        setLoading(true);
        request.getCameras().then(() => {
            setLoading(false);
        });
    }, []);

    if (loading) {
        return (
            <Content>
                <View style={styles.container}>
                    <ActivityIndicator
                        color={styles.indicator.color}
                        style={styles.indicator}
                        size="large"
                    />
                </View>
            </Content>
        )
    } else {
        if (cameras.length) {
            return (
                <Content
                    space={null}
                    header={(
                        <View
                            style={styles.headerContainer}
                        >
                            <Button
                                style={styles.headerButton}
                                type="simple"
                                title={I18n.t('sliderView')}
                                disabled={!gridView}
                                icon={{
                                    name: 'film',
                                    type: 'font-awesome'
                                }}
                                onPress={() => {
                                    setGridView(!gridView);
                                }}
                            />
                            <Separator type="vertical"/>
                            <Button
                                style={styles.headerButton}
                                type="simple"
                                title={I18n.t('gridView')}
                                disabled={gridView}
                                icon={{
                                    name: 'th-large',
                                    type: 'font-awesome'
                                }}
                                onPress={() => {
                                    setGridView(!gridView);
                                }}
                            />
                        </View>
                    )}
                >
                    <View style={styles.container}>
                        {gridView ? (
                            <View style={styles.gridContainer}>
                                {cameras.map((item, index) => {
                                    return (
                                        <View
                                            key={index}
                                            style={styles.gridItem}>
                                            <GridItem {...item}/>
                                        </View>
                                    )
                                })}
                            </View>
                        ) : (
                            <View style={styles.sliderContainer}>
                                <Slider
                                    data={cameras}
                                    space={0}
                                    pagination={(cameras.length > 1)}
                                    renderItem={({item, index}) => (
                                        <View style={styles.sliderItem}>
                                            <SliderItem
                                                key={index}
                                                {...item}
                                            />
                                        </View>
                                    )}
                                />
                            </View>
                        )}
                    </View>
                </Content>
            )
        } else {
            return (
                <Content>
                    <View style={styles.container}>
                        <Empty navigation={navigation}/>
                    </View>
                </Content>
            )
        }
    }
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingVertical: 20,
        flexDirection: 'column'
    },
    sliderContainer: {
        width: '100%',
        height: 350,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    sliderItem: {
        width: '100%',
        height: '100%',
        paddingHorizontal: 30,
    },
    gridContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 30,
    },
    gridItem: {
        width: '100%',
        height: 250,
        marginBottom: 5,
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 10,
    },
    headerButton: {
        width: 150,
        minWidth: 0,
        fontWeight: 'bold',
        marginVertical: 0,
        paddingVertical: 5,
    },
    indicator: {
        marginTop: 50,
        color: theme.indicator
    }
});

const mapStateToProps = (state) => {
    return {
        cameras: state.cameras,
    };
};

export default connect(mapStateToProps)(List);
