import React, {useEffect} from "react";
import {Icon, View} from "../../components/Themed";
import VideoPlayer from '../../components/Video';
import {Text} from "../../components/Themed";
import useTheme from "../../hooks/useTheme";
import Modal from "../../components/Modal";
import {Button} from "../../components/Button";
import Orientation from "../../components/Orientation";
import I18n from '../../utils/i18n';

const theme = useTheme();

const FullItem = (props) => {
    const {
        address,
        name,
        rtspUrl,
        close
    } = props;


    return (
        <Orientation>
            <Modal
                visibile={true}
                close={close}
                onRequestClose={close}
                supportedOrientations={['portrait', 'landscape']}
            >
                <View
                    style={styles.container}
                >
                    <View style={styles.videoHeader}>
                        <View style={styles.videoTitle}>
                            <Text type="caption" style={styles.title}>{I18n.t('fullAddress', address)}</Text>
                            <Text type="caption" style={styles.title}>{name}</Text>
                        </View>
                        <View style={styles.tools}>
                            <Icon
                                name="circle"
                                type="font-awesome"
                                style={styles.liveIcon}
                            />
                            <Text
                                type="p"
                                style={styles.liveTitle}
                            >
                                {I18n.t("live")}
                            </Text>
                            <Button
                                type="simple"
                                style={styles.fullscreenButton}
                                icon={{
                                    name: 'fullscreen-exit',
                                    type: 'material',
                                }}
                                onPress={close}
                            />
                        </View>
                    </View>
                    <View style={styles.video}>
                        <VideoPlayer
                            url={rtspUrl}
                            live={true}
                        />
                    </View>
                </View>
            </Modal>
        </Orientation>
    )
};

const styles = {
    container: {
        width: '100%',
        height: '100%',
        position: 'relative',
        flexDirection: 'column',
        overflow: 'hidden',
        flex: 1,
    },
    video: {
        width: '100%',
        height: '100%',
    },
    videoHeader: {
        width: '100%',
        position: 'absolute',
        top: 0,
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: theme.overlay,
        elevation: 1,
        zIndex: 1,
    },
    videoTitle: {
        width: '100%',
        flex: 1,
    },
    title: {
        width: '100%',
        color: theme.lightText,
    },
    tools: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    liveIcon: {
        color: theme.error,
        fontSize: 16,
    },
    liveTitle: {
        color:  theme.lightText,
        marginLeft: 10,
    },
    fullscreenButton: {
        size: 40,
        color: theme.lightText,
        marginLeft: 10,
        paddingVertical: 0,
    }
};

export default FullItem;
