import React, {useState} from "react";
import {Icon, View} from "../../components/Themed";
import VideoPlayer from '../../components/Video';
import {Text} from "../../components/Themed";
import useTheme from "../../hooks/useTheme";
import I18n from '../../utils/i18n';
import {Button} from "../../components/Button";
import FullItem from "./full-item";

const theme = useTheme();

const SliderItem = (props) => {
    const {
        address,
        name,
        rtspUrl
    } = props;

    const [fullscreen, setFullScreen] = useState(false);

    const enterFullscreen = () => {
        setFullScreen(true);
    };

    const exitFullScreen = () => {
        setFullScreen(false);
    };

    if (fullscreen) {
        return (
            <FullItem
                address={address}
                name={name}
                rtspUrl={rtspUrl}
                close={exitFullScreen}
            />
        )
    }

    return (
        <View
            style={styles.container}
        >
            <View style={styles.header}>
                <Text type="caption" style={styles.title}>{I18n.t('fullAddress', address)}</Text>
                <Text type="caption" style={styles.title}>{name}</Text>
            </View>
            <View style={styles.videoContainer}>
                <View style={styles.videoHeader}>
                    <Icon
                        name="circle"
                        type="font-awesome"
                        style={styles.liveIcon}
                    />
                    <Text type="p" style={styles.liveTitle}>{I18n.t("live")}</Text>
                    <Button
                        type="simple"
                        style={styles.fullscreenButton}
                        icon={{
                            name: 'fullscreen',
                            type: 'material',
                        }}
                        onPress={enterFullscreen}
                    />
                </View>
                <View  style={styles.video}>
                    <VideoPlayer
                        url={rtspUrl}
                        live={true}
                    />
                </View>
            </View>
        </View>
    )
};

const styles = {
    container: {
        width: '100%',
        height: '100%',
        position: 'relative',
        flexDirection: 'column',
        overflow: 'hidden',
    },
    header: {
        width: '100%',
        flexDirection: 'column',
        marginBottom: 20,
    },
    title: {
        color: theme.caption
    },
    videoContainer: {
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: theme.overlay,
        flexDirection: 'column',
    },
    video: {
        width: '100%',
        height: '100%',
    },
    videoHeader: {
        width: '100%',
        height: 40,
        position: 'absolute',
        top: 0,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: theme.overlay,
        elevation: 1,
        zIndex: 1,
    },
    liveIcon: {
        color: theme.error,
        fontSize: 16,
    },
    liveTitle: {
        color:  theme.lightText,
        marginLeft: 10,
    },
    fullscreenButton: {
        size: 30,
        color: theme.lightText,
        marginLeft: 10,
        paddingVertical: 0,
    }
};

export default SliderItem;
