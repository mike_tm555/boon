import React, {useState} from 'react';
import { StyleSheet } from 'react-native';
import { connect } from "react-redux";
import Screen from '../components/Screen';
import Form from "../components/Form";
import {Text, View} from "../components/Themed";
import {SETTINGS_FORM} from "../constants/definitions/Forms";
import I18n from '../utils/i18n';
import useRequest from "../hooks/useRequest";
import Wrapper from "../components/Wrapper";
import {SvgIcon} from "../components/SvgIcon";
import Assets from "../constants/definitions/Assets";

const configs = require('../app.json');

const SettingsScreen = ({ navigation, route, user }) => {
    const [form] = useState(SETTINGS_FORM);
    const [settings, setSettings] = useState(user.userSettings);
    const request = useRequest();

    const submitForm = async (data) => {
        const newSettings = {
            ...settings,
            ...data,
        };

        const response = await request.updateUser({
            settings: newSettings
        }, user.id);

        if (response) {
            setSettings(newSettings);
        }

        return response;
    };

    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            <Wrapper>
                <View style={styles.container}>
                    <Form
                        form={form}
                        data={settings}
                        autoSubmit={submitForm}
                    />
                    <View style={styles.footer}>
                        <SvgIcon
                            style={styles.logo}
                            icon={Assets.patterns.logoDark}
                        />
                        <Text type="caption">
                            {I18n.t('appVersion', {
                                version: configs.expo.version
                            })}
                        </Text>
                    </View>
                </View>
            </Wrapper>
        </Screen>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingVertical: 20,
    },
    footer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: "center",
    },
    logo: {
        width: 60,
        height: 50,
        marginBottom: 20
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

export default connect(mapStateToProps)(SettingsScreen);
