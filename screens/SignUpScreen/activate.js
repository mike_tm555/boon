import React, {useState} from 'react';
import { StyleSheet } from "react-native";
import Form from "../../components/Form";
import {ACTIVATE_FORM} from "../../constants/definitions/Forms";
import useRequest from "../../hooks/useRequest";
import Content from "../../components/Content";
import Routing from "../../navigation/Routing";
import {Text, View} from "../../components/Themed";
import I18n from "../../utils/i18n";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";

const Activate = ({route, navigation}) => {
    const [form] = useState(ACTIVATE_FORM);
    const request = useRequest();

    const submitForm = async (data) => {
        const response = await request.verify({
            phoneNumber: route.params.phoneNumber,
            code: data.code,
        });

        if (response && response.data.token) {
            return navigation.navigate(Routing.welcome.name);
        }

        return response;
    };

    const resendCode = async () => {
        const response = await request.otp({
            phoneNumber: route.params.phoneNumber
        });

        if (response && response.data.code) {
            navigation.navigate(Routing.signUp.name, {
                screen: Routing.signUp.screens.activate.name,
                params: {
                    phoneNumber: route.params.phoneNumber,
                    code: response.data.code
                }
            });
        }
    };

    return (
        <Content>
            <View style={styles.container}>
                <Text type="h1">{I18n.t('activate.title')}</Text>
                <Text type="h5">{I18n.t('activate.description')}</Text>
                <SvgIcon
                    icon={Assets.authorize.sms}
                    style={styles.image}
                />
                <Form
                    changed={true}
                    form={form}
                    data={{
                        code: route.params.code
                    }}
                    actions={{
                        resend: resendCode
                    }}
                    submit={submitForm}
                />
            </View>
        </Content>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 30,
    },
    image: {
        width: '100%',
        alignSelf: 'center',
        height: 120,
        marginVertical: 20,
    }
});

export default Activate
