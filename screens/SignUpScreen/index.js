import React from 'react';
import Screen from '../../components/Screen';
import Activate from "./activate";
import Register from "./register";
import {connect} from "react-redux";

const SIGN_UP_COMPONENTS = {
    activate: Activate,
    register: Register,
};

const SignUpScreen = ({ navigation, route, user, token }) => {
    const SignUpComponent = SIGN_UP_COMPONENTS[route.name];

    return (
        <Screen
            title={route.params.title}
            navigation={navigation}
        >
            <SignUpComponent
                user={user}
                token={token}
                navigation={navigation}
                route={route}
            />
        </Screen>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

export default connect(mapStateToProps)(SignUpScreen);
