import React, {useState} from 'react';
import Content from "../../components/Content";
import Routing from "../../navigation/Routing";
import Form from "../../components/Form";
import {LINK_DEVICE_FORM} from "../../constants/definitions/Forms";
import {Text, View} from "../../components/Themed";
import I18n from "../../utils/i18n";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";
import {StyleSheet} from "react-native";
import useRequest from "../../hooks/useRequest";
import Modal from "../../components/Modal";
import {Button} from "../../components/Button";
import useTheme from "../../hooks/useTheme";

const theme = useTheme();

const Intercom = ({ navigation }) => {
    const request = useRequest();
    const [showHelp, setShowHelp] = useState(false);
    const [form] = useState(LINK_DEVICE_FORM);

    const submitForm = async (data) => {
        const response = await request.getIntercomAddress({
            code: data.code
        });

        if (response.data) {
            return navigation.navigate(Routing.linkDevice.name, {
                screen: Routing.linkDevice.screens.address.name,
                params: {
                    device: response.data,
                    code: data.code
                }
            });
        }

        return response;
    };

    const openHelp = () => {
        setShowHelp(true);
    };

    const closeHelp = () => {
        setShowHelp(false);
    };

    return (
        <Content>
            <View style={styles.container}>
                <Text type="h1">{I18n.t('intercom.title')}</Text>
                <Text type="h5">{I18n.t('intercom.description')}</Text>
                <SvgIcon icon={Assets.devices.intercom} style={styles.image}/>
                <Form
                    actions={{
                        help: openHelp
                    }}
                    submit={submitForm}
                    form={form}/>
                <Modal
                    header={(
                        <Text
                            type="h4"
                            style={styles.modalTitle}
                        >
                            {I18n.t('intercom.description')}
                        </Text>
                    )}
                    footer={(
                        <Button
                            type="action"
                            style={styles.closeButton}
                            title={I18n.t('close')}
                            onPress={closeHelp}
                        />
                    )}
                    transparent={true}
                    animationType="fade"
                    visible={showHelp}
                    close={closeHelp}
                    onRequestClose={closeHelp}
                >
                    <Content>
                    </Content>
                </Modal>
            </View>
        </Content>
    );
};


const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 30,
    },
    image: {
        width: '100%',
        alignSelf: 'center',
        height: 100,
        marginVertical: 20,
    },
    closeButton: {
        marginVertical: 20,
    },
    modalTitle: {
        width: '100%',
        padding: 20,
        backgroundColor: theme.primary,
        color: theme.lightText,
    }
});

export default Intercom;
