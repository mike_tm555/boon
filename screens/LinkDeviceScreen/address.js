import React, {useState} from 'react';
import Content from "../../components/Content";
import Routing from "../../navigation/Routing";
import Form from "../../components/Form";
import {ADDRESS_CONFIRM_FORM} from "../../constants/definitions/Forms";
import {Text, View} from "../../components/Themed";
import I18n from "../../utils/i18n";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";
import {StyleSheet} from "react-native";
import useRequest from "../../hooks/useRequest";

const Address = ({ navigation, route }) => {
    const request = useRequest();
    const [form] = useState(ADDRESS_CONFIRM_FORM);

    const submitForm = async (data) => {
        const response = await request.linkIntercom({
            code: route.params.code
        });

        if (response.data) {
            return navigation.navigate(Routing.home);
        }

        return response;
    };

    const cancelForm = () => {
        navigation.goBack(null);
    };

    return (
        <Content>
            <View style={styles.container}>
                <Text type="h1">{I18n.t('address.title')}</Text>
                <SvgIcon icon={Assets.devices.address} style={styles.image}/>
                <Form
                    changed={true}
                    submit={submitForm}
                    cancel={cancelForm}
                    form={form}
                    data={route.params.device}
                />
            </View>
        </Content>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 30,
    },
    image: {
        width: '100%',
        alignSelf: 'center',
        height: 100,
        marginVertical: 20,
    }
});

export default Address;
