import React from 'react';
import Screen from '../../components/Screen';
import Intercom from "./intercom";
import Address from "./address";
import {connect} from "react-redux";

const LINK_DEVICE_COMPONENTS = {
    intercom: Intercom,
    address: Address,
};

const LinkDeviceScreen = ({ navigation, route, user, token }) => {
    const LinkDeviceComponent = LINK_DEVICE_COMPONENTS[route.name];

    return (
        <Screen
            title={route.params.title}
            back={route.params.back ?? false}
            navigation={navigation}
        >
            <LinkDeviceComponent
                user={user}
                token={token}
                navigation={navigation}
                route={route}
            />
        </Screen>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

export default connect(mapStateToProps)(LinkDeviceScreen);
