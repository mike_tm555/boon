import React, {useEffect, useState} from 'react';
import { ActivityIndicator, StyleSheet } from "react-native";
import Empty from "./empty";
import Content from "../../components/Content";
import Intercom from './intercom';
import useRequest from "../../hooks/useRequest";
import Search from "../../components/Search";
import useSearch from "../../hooks/useSearch";
import {connect} from "react-redux";
import useTheme from "../../hooks/useTheme";
import {Text, View} from "../../components/Themed";
import {getDate} from "../../helpers/date";

const theme = useTheme();

const ITEM_COMPONENTS = {
    intercom: Intercom,
};

const List = ({ navigation, history  }) => {
    const request = useRequest();
    const search = useSearch(['title', 'message']);
    const [searchText, setSearchText] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        request.history().then(() => {
            setLoading(false);
        })
    }, []);

    const onSearch = (text) => {
        setSearchText(text);
    };

    if (loading) {
        return (
            <Content>
                <View style={styles.container}>
                    <ActivityIndicator
                        size="large"
                        color={styles.indicator.color}
                        style={styles.indicator}
                    />
                </View>
            </Content>
        )
    } else {
        if (history.length) {
            const items = search(history, searchText);
            const dates = {};

            items.forEach(item => {
                const date = getDate(item.callTime);

                if (!dates[date]) {
                    dates[date] = {
                        date: date,
                        list: [],
                    }
                }

                dates[date].list.push(item);
            });

            return (
                <Content
                    header={(
                        <Search
                            value={searchText}
                            search={onSearch}
                        />
                    )}
                >
                    <View style={styles.container}>
                        {Object.keys(dates).map((key) => {
                            const item = dates[key];
                            return (
                                <View
                                    style={styles.section}
                                    key={item.date}>
                                    <Text
                                        type="caption"
                                        style={styles.separator}
                                    >
                                        {item.date}
                                    </Text>
                                    {item.list.map((listItem) => {
                                        const ItemComponent = ITEM_COMPONENTS[listItem.type];
                                        return (
                                            <ItemComponent
                                                key={listItem.id}
                                                id={listItem.id}
                                                {...listItem}
                                            />
                                        )
                                    })}
                                </View>
                            )
                        })}
                    </View>
                </Content>
            )
        } else {
            return (
                <Content>
                    <View style={styles.container}>
                        <Empty navigation={navigation}/>
                    </View>
                </Content>
            )
        }
    }
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        paddingVertical: 10,
    },
    indicator: {
        marginTop: 50,
        color: theme.indicator
    },
    section: {
        width: '100%',
        marginBottom: 10,
    },
    separator: {
        width: '100%',
        textAlign: 'center',
        marginBottom: 10,
    }
});

const mapStateToProps = (state) => {
    return {
        history: state.history,
    };
};

export default connect(mapStateToProps)(List);
