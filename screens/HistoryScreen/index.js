import React from 'react';
import Screen from '../../components/Screen';
import List from "./list";
import {connect} from "react-redux";

const CONTENT_COMPONENTS = {
    list: List
};

const HistoryScreen = ({navigation, route, user, token}) => {
    const ContentComponent = CONTENT_COMPONENTS[route.name];

    return (
        <Screen
            back={true}
            title={route.params.title}
            navigation={navigation}
        >
            <ContentComponent
                user={user}
                route={route}
                navigation={navigation}
            />
        </Screen>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

export default connect(mapStateToProps)(HistoryScreen);
