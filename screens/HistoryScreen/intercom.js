import React, {useState} from "react";
import {ImageBackground, View} from "../../components/Themed";
import {StyleSheet} from "react-native";
import {Text} from "../../components/Themed";
import ListItem from "../../components/ListItem";
import useTheme from "../../hooks/useTheme";
import {SvgIcon} from "../../components/SvgIcon";
import Assets from "../../constants/definitions/Assets";
import I18n from '../../utils/i18n';
import Dialog from "../../components/Dialog";
import {getDate, getTime} from "../../helpers/date";

const  theme = useTheme();

export default function (props) {
    const {
        id,
        callTime,
        doorOpenTime,
        address,
        intercomName,
        screenShotUrl,
        openedByName,
        date,
        time
    } = props;

    const [showInfo, setShowInfo] = useState(false);

    const openInfo = () => {
        setShowInfo(true);
    };

    const hideInfo = () => {
        setShowInfo(false);
    };

    let indicatorColor = 'transparent';

    if (doorOpenTime) {
        indicatorColor = theme.success;
    } else {
        indicatorColor = theme.error;
    }

    const source = screenShotUrl ? {uri: screenShotUrl} : Assets.history.screenshot;

    return (
        <ListItem
            press={openInfo}
        >
            <View style={styles.container}>
                <View
                    style={{
                        ...styles.indicator,
                        backgroundColor: indicatorColor,
                    }}
                />
                <View style={styles.icon}>
                    <SvgIcon
                        icon={Assets.icons.intercom}
                        style={styles.intercom}
                    />
                </View>
                <View style={styles.info}>
                    <Text type="h4" style={styles.name}>{intercomName}</Text>
                    <Text type="caption" style={styles.address}>{I18n.t('fullAddress', address)}</Text>
                </View>
                <View style={styles.date}>
                    <Text type="caption">{getTime(callTime)}</Text>
                </View>
            </View>
            <Dialog
                close={hideInfo}
                onRequestClose={hideInfo}
                visible={showInfo}>
                <View style={styles.dialogHeader}>
                    <View style={styles.icon}>
                        <SvgIcon
                            icon={Assets.icons.intercom}
                            style={styles.intercom}
                        />
                    </View>
                    <View style={styles.info}>
                        <Text type="h4" style={styles.name}>{intercomName}</Text>
                        <Text type="caption" style={styles.address}>{I18n.t('fullAddress', address)}</Text>
                    </View>
                    <View style={styles.date}>
                        <Text type="caption">{getTime(callTime)}</Text>
                    </View>
                </View>
                <View style={styles.dialogImage}>
                    <ImageBackground
                        style={styles.image}
                        source={source}
                    />
                </View>
                {openedByName && (
                    <View style={styles.dialogFooter}>
                        <View style={styles.footerSection}>
                            <Text type="caption">{getDate(doorOpenTime)}</Text>
                            <Text type="caption">{getTime(doorOpenTime)}</Text>
                        </View>
                        <View style={styles.footerSection}>
                            <Text type="p" style={styles.footerLabel}>{I18n.t('openedBy')}</Text>
                            <Text type="p" style={styles.footerLabel}>{openedByName}</Text>
                        </View>
                    </View>
                )}
            </Dialog>
        </ListItem>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    indicator: {
        width: 7,
        height: 30,
        left: -15,
    },
    icon: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    intercom: {
        width: 30,
        height: 50,
    },
    info: {
        width: '100%',
        flex: 1,
        paddingHorizontal: 10,
    },
    name: {
        textAlign: 'left',
        marginBottom: 5,
    },
    date: {
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dialogHeader: {
        width: '100%',
        height: 100,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    dialogImage:  {
        width: '100%',
    },
    image: {
        width: '100%',
        height: 200
    },
    dialogFooter: {
        width: '100%',
        padding: 20,
        flexDirection: 'column',
        alignItems: 'center',
    },
    footerSection: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 4,
    },
    footerLabel: {
        fontWeight: 'bold',
    }
});
