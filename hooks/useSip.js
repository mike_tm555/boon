import {Endpoint} from 'react-native-pjsip';
import Constants from "expo-constants";

let endpoint = null;

function getEndPoint() {
    if (endpoint === null) {
        endpoint = new Endpoint();
    }

    return endpoint;
}

function startSip() {
    let endpoint = getEndPoint();
    endpoint.start().then((response) => {
        endpoint.on("registration_changed", (account) => {});
        endpoint.on("connectivity_changed", (online) => {});
        endpoint.on("call_received", (call) => {});
        endpoint.on("call_changed", (call) => {});
        endpoint.on("call_terminated", (call) => {});
    });
}

function authorize(data) {
    const endpoint = getEndPoint();
    let configuration = {
        "name": data.name,
        "username": data.username,
        "domain": data.server,
        "password": data.password,
        "proxy": null,
        "transport": null, // Default TCP
        "regServer": null, // Default wildcard
        "regTimeout": null, // Default 3600
        "regHeaders": {
            "X-Custom-Header": "Value"
        },
        "regContactParams": `;unique-device-token-id=${Constants.deviceId}`,
    };

    return endpoint.createAccount(configuration);
}

function useSip() {
    return {
        startSip,
        authorize
    }
}

export default useSip;
