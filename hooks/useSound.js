import { Audio } from 'expo-av';

const useSound = () => {
    Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        staysActiveInBackground: true,
    });

    return {
        play: async (sound, options) => {
            let soundObject = null;
            try {
                const soundObject = await Audio.Sound.createAsync(sound, {
                    shouldPlay: true,
                    isLooping: true,
                    ...options
                });

                if (options.duration) {
                    setTimeout(async () => {
                        if (soundObject) {
                            await soundObject.unloadAsync();
                        }
                    }, options.duration)
                }
            } catch (error) {
                soundObject = error;
                // An error occurred!
            }

            return soundObject;
        },
        stop: async (sound) => {
            await sound.unloadAsync();
        }
    }
};

export default useSound;
