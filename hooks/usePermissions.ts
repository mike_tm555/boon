const Permissions = {
    authorized: (props: any) => {
        return (!!props.token && props.user && props.user.id);
    },
    notAuthorized: (props: any) => {
        return !props.token;
    },
    registered: (props: any) => {
        return (props.user && props.user.id && props.user.name);
    },
    notRegistered: (props: any) => {
        return !props.user || !props.user.name;
    }
} as any;

const usePermission = (user: any, token: string) => {
    return (permissions: string[]) => {
        if (!permissions) {
            return true;
        }
        return permissions.every((permmission: string) => {
            return Permissions[permmission]({
                user,
                token
            });
        });
    }
};

export default usePermission;
