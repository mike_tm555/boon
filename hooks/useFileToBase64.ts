import * as FileSystem from 'expo-file-system';
import base64 from 'base64-js';

function stringToUint8Array(str: string) {
    const length = str.length;
    const array = new Uint8Array(new ArrayBuffer(length));
    for (let i = 0; i < length; i++) array[i] = str.charCodeAt(i);
    return array;
}

export async function useFileToBase64(uri: string) {
    try {
        const content = await FileSystem.readAsStringAsync(uri, { encoding: 'base64' });
        return base64.fromByteArray(stringToUint8Array(content))
    } catch (e) {
        console.warn('fileToBase64()', e.message);
        return ''
    }
}
