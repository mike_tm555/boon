import Inputs from "../constants/styles/Inputs";

export default function (error: boolean) {
    if (error) {
        return {
            borderColor: Inputs.field.errorColor,
            color: Inputs.field.errorColor
        }
    }

    return {};
}
