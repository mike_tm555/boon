export default function (props: string[]) {
    return (list: any[], term: string) => {
        if (!list || !term) {
            return list;
        }

        return list.filter((item: any) => {
            let matched = false;

            for (let key in props) {
                if (item[key] && item[key].indexOf(term) > -1) {
                    matched = true;
                    break;
                }
            }

            return matched;
        })
    }
}
