import React from "react";
import History from "../constants/mocks/History";
import {TOAST_TYPES, Toaster} from '../components/Toasts/toaster';
import I18n from '../utils/i18n';

import {
    addCamera,
    addHistory,
    addLock,
    addNotification,
    addPackage,
    addPeople,
    createToken,
    createUser,
    deleteLock, deletePeople,
    removeToken,
    removeUser,
    setAppInfo,
} from '../store/actions';
import Store from '../store';
import useStorage from './useStorage';
import Packages from "../constants/mocks/Packages";
import Devices from "../constants/definitions/Devices";
import People from "../constants/mocks/People";
import AppInfo from "../constants/mocks/AppInfo";
import usePusher from "./usePusher";

function createURL(params) {
    let url = params.host;

    if (params.path) {
        url += params.path;
    }

    if (params.id) {
        url += `/${params.id}`;
    }

    if (params.query) {
        url += `?`;
        url += Object.keys(params.query).map(key => {
            return `${key}=${params.query[key]}`;
        }).join('&');
    }

    return url;
}

class ApiRequest {
    host = 'http://207.154.207.237:8080';

    setHost(value) {
        this.host = value;
    }

    async public(path, query, headers = {}) {
        let fetchURL = createURL({
            host: this.host,
            path: path,
            query: query,
        });

        const response = await fetch(fetchURL, {
            method: 'GET',
            headers: headers
        });

        const body = await response.json();

        if (body.message) {
            if (body.error) {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.error,
                });
            } else {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.success,
                });
            }
        } else if (body.error) {
            Toaster.show({
                message: I18n.t(body.error),
                type: TOAST_TYPES.error,
            });
        }

        if (body.data) {
            return body;
        }

        return false;
    }

    async get(path, query, headers = null) {
        let fetchURL = createURL({
            host: this.host,
            path: path,
            query: query,
        });

        const { token } = Store.getState();

        if (!headers) {
            headers = {};
        }

        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }

        const response = await fetch(fetchURL, {
            method: 'GET',
            headers: headers
        });

        const body = await response.json();

        if (body.message) {
            if (body.error) {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.error,
                });
            } else {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.success,
                });
            }
        } else if (body.error) {
            Toaster.show({
                message: I18n.t(body.error),
                type: TOAST_TYPES.error,
            });
        }

        if (body.data) {
            return body;
        }

        return false;
    }

    async post(path, data, headers = null) {
        let fetchURL = createURL({
            host: this.host,
            path: path,
        });

        if (!headers) {
            headers = {
                'accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }

        const { token } = Store.getState();

        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }

        const response = await fetch(fetchURL, {
            method: 'POST',
            body: data,
            headers: headers
        });

        const body = await response.json();

        if (body.message) {
            if (body.error) {
                Toaster.show({
                    title: I18n.t(body.message),
                    type: TOAST_TYPES.error,
                });
            } else {
                Toaster.show({
                    title: I18n.t(body.message),
                    type: TOAST_TYPES.success,
                });
            }
        } else if (body.error) {
            Toaster.show({
                title: I18n.t(body.error),
                type: TOAST_TYPES.error,
            });
        }

        if (body.data) {
            return body;
        }

        return false;
    }

    async delete(path, headers = {}) {
        let fetchURL = createURL({
            host: this.host,
            path: path,
        });

        const { token } = Store.getState();

        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }

        const response = await fetch(fetchURL, {
            method: 'DELETE',
            headers: headers
        });

        const body = await response.json();

        if (body.message) {
            if (body.error) {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.error,
                });
            } else {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.success,
                });
            }
        } else if (body.error) {
            Toaster.show({
                message: I18n.t(body.error),
                type: TOAST_TYPES.error,
            });
        }

        if (body.data) {
            return body;
        }

        return false;
    }

    async put(path, data, headers = null) {
        let fetchURL = createURL({
            host: this.host,
            path: path,
        });

        if (!headers) {
            headers = {
                'accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }

        const { token } = Store.getState();

        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }

        const response = await fetch(fetchURL, {
            method: 'PUT',
            body: data,
            headers: headers,
        });

        const body = await response.json();

        if (body.message) {
            if (body.error) {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.error,
                });
            } else {
                Toaster.show({
                    message: I18n.t(body.message),
                    type: TOAST_TYPES.success,
                });
            }
        } else if (body.error) {
            Toaster.show({
                message: I18n.t(body.error),
                type: TOAST_TYPES.error,
            });
        }

        if (body.data) {
            return body;
        }

        return false;
    }
}

function requestHooks() {
    const pusher = usePusher();
    const request = new ApiRequest();

    return function () {
        return {
            otp: async (data) => {
                return await request.post('/api/otp/send', JSON.stringify(data));
            },
            verify: async (data) => {
                const response = await request.post('/api/otp/verify', JSON.stringify(data));

                if (response && response.data.user) {
                    await useStorage.save({
                        key: 'user',
                        data: response.data.user
                    });
                    Store.dispatch(createUser(response.data.user));
                }

                if (response && response.data.token) {
                    await useStorage.save({
                        key: 'token',
                        data: response.data.token
                    });
                    Store.dispatch(createToken(response.data.token));
                }

                return response;
            },

            register: async (data) => {
                const pushToken = await pusher.getToken();
                const newData = {
                    pushToken: pushToken,
                    ...data
                };
                const response = await request.post('/api/register', JSON.stringify(newData));

                if (response && response.data) {
                    useStorage.save( {
                        key: 'user',
                        data: response.data,
                    });

                    Store.dispatch(createUser(response.data));
                }

                return response;
            },
            signOut: async () => {
                await useStorage.remove({
                    key: 'token'
                });
                await useStorage.remove({
                    key: 'user'
                });
                Store.dispatch(removeToken(null));
                Store.dispatch(removeUser(null));
                return true;
            },
            updateUser: async (data,  id) => {
                const pushToken = await pusher.getToken();
                const newData = {
                    pushToken: pushToken,
                    ...data
                };

                const response = await request.put(`/api/users/${id}`, JSON.stringify(newData));

                if (response && response.data) {
                    useStorage.save( {
                        key: 'user',
                        data: response.data,
                    });

                    Store.dispatch(createUser(response.data));
                }

                return response;
            },
            loginWithFacebook: async (data, token) => {
                const pushToken = await pusher.getToken();
                const newData = {
                    pushToken: pushToken,
                    ...data
                };
                const response = await request.post(`/api/register/facebook?accessToken=${token}`, JSON.stringify(newData));

                if (response && response.data) {
                    useStorage.save({
                        key: 'token',
                        data: response.data.token
                    });
                    useStorage.save({
                        key: 'user',
                        data: response.data.user
                    });

                    Store.dispatch(createToken(response.data.token));
                    Store.dispatch(createUser(response.data.user));
                }

                return response;
            },
            avatar: (data) => {
                const formData = new FormData();

                Object.keys(data).forEach(key => {
                    formData.append(key, data[key]);
                });

                return request.post('/api/users/avatar', formData, {
                    'Content-Type': 'multipart/form-data',
                });
            },
            getIntercomAddress: async (params) => {
                return await request.get('/api/intercoms', params);
            },
            linkIntercom: async (data) => {
                const response = await request.put(`/api/intercoms/link`, JSON.stringify(data));

                if (response && response.data) {
                    Store.dispatch(addLock([{
                        type: Devices.intercom,
                        ...response.data,
                    }]));
                }

                return response;
            },
            getLocks: async (params) => {
                const response = await request.get(`/api/locks/all`);

                if (response && response.data) {
                    Store.dispatch(addLock(response.data));
                }

                return response;
            },
            deleteIntercom: async (id) => {
                const response = await request.delete(`/api/intercoms/${id}`);

                if (response && response.data) {
                    Store.dispatch(deleteLock(id));
                }

                return response;
            },
            editIntercom: async (id, data) => {
                const response = await request.put(`/api/locks/${id}`, JSON.stringify(data));

                if (response && response.data) {
                    Store.dispatch(addLock([response.data]));
                }

                return response;
            },
            openIntercom: async (id) => {
                return await request.put(`/api/intercoms/${id}/openDoor`);
            },
            getCameras: async (params) => {
                const response = await request.get(`/api/cameras/all`, params);

                if (response && response.data) {
                    Store.dispatch(addCamera(response.data));
                }

                return response;
            },
            getPackages: async () => {
                const response = await request.get(`/api/packages`);

                if (response && response.data) {
                    Store.dispatch(addPackage(response.data));
                } else {
                    Store.dispatch(addPackage(Packages.data));
                }

                return response;
            },
            updatePackage: async (data) => {
                const response = await request.put(`/api/packages`, JSON.stringify(data));

                if (response && response.data) {
                    Store.dispatch(addPackage([response.data]));
                }

                return response;
            },
            getPeople: async () => {
                const response = await request.get(`/api/people`);

                if (response && response.data) {
                    Store.dispatch(addPeople(response.data));
                }

                return response;
            },
            deleteUser: async (data) => {
                const response = await request.put('/api/people/delete', JSON.stringify(data));

                if (response && response.data) {
                    Store.dispatch(deletePeople(data.userId));
                }

                return response;
            },
            disableUser: async (data) => {
                const response = await request.put('/api/people/disable', JSON.stringify(data));

                if (response && response.data) {
                    Store.dispatch(addPeople([response.data]))
                }

                return response;
            },
            enableUser: async (data) => {
                const response = await request.put('/api/people/enable', JSON.stringify(data));

                if (response && response.data) {
                    Store.dispatch(addPeople([response.data]))
                }

                return response;
            },
            generateCode: async (data) => {
                return await request.post(`/api/people/generateCode/${data.flatId}`);
            },
            orderChip: async (flatId) => {
                return await request.post(`/api/people/orderChip/${flatId}`);
            },
            history: async (params = {
                offset: 0,
                limit: 50,
            }) => {
                const response = await request.get('/api/history', params);

                if (response && response.data) {
                    Store.dispatch(addHistory(response.data));
                }

                return response;
            },
            notifications: async (data) => {
                let response = await request.get('/api/notifications', data);

                if (response && response.data) {
                    Store.dispatch(addNotification(response.data));
                }

                return response;
            },
            getAppInfo: async () => {
                let response = await request.public('/api/app/info');

                if (response && response.data) {
                    Store.dispatch(setAppInfo(response.data));
                }

                return response;
            },
            contactUs: async (data) => {
                return await request.post('/api/contact/us', JSON.stringify(data));
            }
        }
    }();
}


let apiInstance = null;

export default function useRequest() {
    if (apiInstance === null) {
        apiInstance = requestHooks();
    }

    return apiInstance;
}
