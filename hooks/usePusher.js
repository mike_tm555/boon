import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import {Platform} from 'react-native';
import useTheme from "./useTheme";
import useSound from "./useSound";
import Assets from "../constants/definitions/Assets";

const sound = useSound();
const theme = useTheme();

const pusherData = {
    token: null,
    channels: null,
    categories: null,
    subscribed: false
};

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: false,
    }),
});

async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
        const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            alert('Failed to get push token for push notification!');
            return;
        }

        try {
            token = (await Notifications.getExpoPushTokenAsync()).data;
        }  catch (e) {
            token = 'ExponentToken[test-token]'
        }

        if (Platform.OS === 'android') {
            if (pusherData.channels === null) {
                pusherData.channels = await setChannel();
            }
        }

        if (pusherData.categories === null) {
            pusherData.categories = await createCategories();
        }

        if (pusherData.subscribed === false) {
            subscribe();
        }
    } else {
        alert('Must use physical device for Push Notifications');
    }

    return token;
}

async function setChannel() {
    const callsChannel = await Notifications.setNotificationChannelAsync('calls', {
        name: 'calls',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        sound: 'default',
        lightColor: theme.secondary,
    });

    return {
        calls: callsChannel
    };
}

function subscribe() {
    Notifications.addNotificationReceivedListener(notification => {
        switch (notification.request.content.data.type) {
            case "call": {
                sound.play(Assets.sounds.call, {
                    duration: 30000,
                    vibrate: true
                }).then((response) => {

                });
            }
        }
    });
    Notifications.addNotificationResponseReceivedListener(response => {
        console.log('opened', response);
    });

    pusherData.subscribed = true;
}

async function createCategories() {
    const callNotifications = await Notifications.setNotificationCategoryAsync('open-call', [
        {
            identifier: 'submit',
            buttonTitle: 'Answer',
            options: {
                opensAppToForeground: true,
            }
        },
        {
            identifier: 'decline',
            buttonTitle: 'Decline',
            options: {
                isDestructive: true,
                opensAppToForeground: false,
            }
        }
    ]);

    return {
        call: callNotifications,
    }
}

async function getToken() {
    if (!pusherData.token) {
        pusherData.token = await registerForPushNotificationsAsync();
    }

    return pusherData.token;
}

async function sendPushNotification(token) {
    const message = {
        to: token,
        title: 'Intercom Call',
        _category: 'open-call',
        badge: 0,
        priority: 'high',
        data: {
            type: "call",
            data: 'goes here'
        },
        trigger: {
            channelId: 'calls'
        }
    };

    const response = await fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Accept-encoding': 'gzip, deflate',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(message),
    });

    return await response.json();
}

const usePusher = () => {
    return {
        getToken,
        sendPushNotification
    }
};

export default usePusher;
