import I18n from '../utils/i18n';
import ru from "../utils/lang/ru";

const Hooks = {
    alpha: (value: any, rule: any) => {
        const regName = /^[a-zA-Z ]+$/;
        const valid = regName.test(value);
        const result = {
            valid: valid
        } as any;

        if (!valid) {
            result.message = I18n.t('validations.invalidAlpha')
        }

        return result;
    },
    phone: (value: any, rule: any) => {
        const valid = (value.dialCode && value.phoneNumber);
        const result = {
            valid: valid
        } as any;

        if (!valid) {
            result.message = I18n.t('validations.invalidPhone')
        }

        return result;
    },
    email: (value: any, rule: any) => {
        const regName = /\S+@\S+\.\S+/;
        const valid = regName.test(value);
        const result = {
            valid: valid
        } as any;

        if (!valid) {
            result.message = I18n.t('validations.invalidEmail')
        }

        return result;
    },
    numeric: (value: any, rule: any) => {
        const valid = !isNaN(value);
        const result = {
            valid: valid,
        } as any;

        if(!valid) {
            result.message = I18n.t('validations.invalidNumeric')
        }

        return result;
    },
    minLength: (value: any, rule: any) => {

    },
    maxLength: (value: any, rule: any) => {

    },
    required: (value: any, rule: any) => {
        const valid = !!value;
        const result = {
            valid: valid
        }  as any;

        if (!valid) {
            result.message = I18n.t('validations.required')
        }

        return result;
    },
    checked: (value: any, rule: any) => {
        const valid = !!value;
        return {
            valid: valid
        }  as any;
    },
} as any;

export default function useFormValidator (fields: any) {
    const newFields = {} as any;
    Object.keys(fields).forEach((key: string) => {
        const field = fields[key];
        if (field.validation) {
            let fieldIsValid = true;
            for (let rule of field.validation) {
                const result = Hooks[rule.type](field.params.value, rule.params);
                if (!result.valid) {
                    fieldIsValid = false;
                    field.error = result;
                    break;
                }
            }

            if (fieldIsValid) {
                delete field.error;
            }
        }

        newFields[key] = field;
    });

    return newFields;
}

export function useFormIsValid(fields: any) {
    let formIsValid = true;

    for (let key of Object.keys(fields)) {
        const field = fields[key];
        if (field.error && !field.error.valid) {
            formIsValid = false;
            break;
        }
    }

    return formIsValid;
}
