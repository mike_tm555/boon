import Colors from "../constants/styles/Colors";
import useColorScheme from "./useColorScheme";

export default function useTheme() {
    return Colors.light as any;
}
