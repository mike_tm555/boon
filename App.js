import React from 'react';
import { Provider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import Store from "./store";
import Toasts from "./components/Toasts";


/**
 * @return {null}
 */

export default function App() {
    const isLoadingComplete = useCachedResources();
    const colorScheme = useColorScheme();

    if (isLoadingComplete) {
        return (
            <SafeAreaProvider>
                <Provider store={Store}>
                    <Navigation colorScheme={colorScheme} />
                    <Toasts/>
                </Provider>
            </SafeAreaProvider>
        );
    }

    return null;
}
