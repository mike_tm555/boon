import I18n from "../utils/i18n";
import {
    AUTHORIZED,
    NOT_AUTHORIZED,
    NOT_REGISTERED,
    REGISTERED
} from "../constants/definitions/Permissions";

export default {
    main: {
        name: 'main',
        permissions: [
            NOT_AUTHORIZED,
        ],
        params: {
        }
    },
    authorization: {
        name: 'authorization',
        permissions: [
            NOT_AUTHORIZED,
        ],
        screens: {
            authorize: {
                name: 'authorize',
                permissions: [
                    NOT_AUTHORIZED,
                ],
                params: {
                    title: I18n.t('authorize.header'),
                }
            },
            activate: {
                name: 'activate',
                permissions: [
                    NOT_AUTHORIZED,
                ],
                params: {
                    title: I18n.t('activate.header'),
                }
            }
        }
    },
    welcome: {
        name: 'welcome',
        permissions: [
            AUTHORIZED,
            NOT_REGISTERED
        ],
        params: {
            title: I18n.t('welcome.header'),
        }
    },
    home: {
        name: 'home',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {}
    },
    linkDevice: {
        name: 'linkDevice',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {},
        screens: {
            intercom: {
                name: 'intercom',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    back: false,
                    title: I18n.t('intercom.header'),
                }
            },
            address: {
                name: 'address',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    back: true,
                    title: I18n.t('address.header'),
                }
            }
        }
    },
    profile: {
        name: 'profile',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {},
        screens: {
            account: {
                name: 'account',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('account.header'),
                }
            }
        }
    },
    history: {
        name: 'history',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {},
        screens: {
            list: {
                name: 'list',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('history.header'),
                }
            },
            single: {
                name: 'single',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('history.header'),
                }
            }
        }
    },
    notifications: {
        name: 'notifications',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {},
        screens: {
            list: {
                name: 'list',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('notifications.header'),
                }
            },
            single: {
                name: 'single',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('notifications.header'),
                }
            }
        }
    },
    locks: {
        name: 'locks',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {},
        screens: {
            list: {
                name: 'list',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('locks.header'),
                }
            },
            single: {
                name: 'single',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('locks.header'),
                }
            },
            intercom: {
                name: 'intercom',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    back: false,
                    title: I18n.t('intercom.header'),
                }
            },
            address: {
                name: 'address',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    back: true,
                    title: I18n.t('address.header'),
                }
            }
        }
    },
    settings: {
        name: 'settings',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {
            title: I18n.t('settings.header'),
        }
    },
    packages: {
        name: 'packages',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {
            title: I18n.t('packages.header'),
        },
        screens: {
            addresses: {
                name: 'addresses',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('packages.header'),
                }
            },
            packages: {
                name: 'packages',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('packages.header'),
                }
            }
        }
    },
    payments: {
        name: 'payments',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {
            title: I18n.t('payments.header'),
        },
    },
    people: {
        name: 'people',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {
            title: I18n.t('people.header'),
        },
        screens: {
            addresses: {
                name: 'addresses',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('people.header'),
                }
            },
            users: {
                name: 'users',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('people.header'),
                }
            }
        }
    },
    help: {
        name: 'help',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {
            title: I18n.t('help.header'),
        },
        screens: {
            support: {
                name: 'support',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('support.header'),
                }
            },
            questions: {
                name: 'questions',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('questions.header'),
                }
            }
        }
    },
    cameras: {
        name: 'cameras',
        permissions: [
            AUTHORIZED,
            REGISTERED,
        ],
        params: {
            title: I18n.t('cameras.header'),
        },
        screens: {
            list: {
                name: 'list',
                permissions: [
                    AUTHORIZED,
                    REGISTERED,
                ],
                params: {
                    title: I18n.t('cameras.header'),
                }
            }
        }
    },
};
