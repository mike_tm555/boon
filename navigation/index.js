import * as React from 'react';
import { connect } from 'react-redux';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import {
    createStackNavigator ,
    CardStyleInterpolators
} from '@react-navigation/stack';
import Linking from "./Linking";
import Routing from "./Routing";
//screens imports
import MainScreen from "../screens/MainScreen";
import SignUpScreen from "../screens/SignUpScreen";
import SignInScreen from "../screens/SignInScreen";
import LinkDeviceScreen from "../screens/LinkDeviceScreen";
import HomeScreen from "../screens/HomeScreen";
import ProfileScreen from "../screens/ProfileScreen";
import AutomationScreen from "../screens/AutomationScreen";
import CamerasScreen from "../screens/CamerasScreen";
import DevicesScreen from "../screens/DevicesScreen";
import HistoryScreen from "../screens/HistoryScreen";
import LocksScreen from "../screens/LocksScreen";
import PeopleScreen from "../screens/PeopleScreen";
import HelpScreen from "../screens/HelpScreen";
import SecurityScreen from "../screens/SecurityScreen";
import SettingsScreen from "../screens/SettingsScreen";
import NotificationsScreen from "../screens/NotificationsScreen";
import WelcomeScreen from "../screens/WelcomeScreen";
import PaymentsScreen from "../screens/PaymentsScreen";
import PackagesScreen from "../screens/PackagesScreen";
import AuthorizationScreen from "../screens/Authorization";
//--------
import usePermission from "../hooks/usePermissions";

const Stack = createStackNavigator();

const SCREEN_COMPONENTS = {
    main: MainScreen,
    welcome: WelcomeScreen,
    home: HomeScreen,
    signIn: SignInScreen,
    signUp: SignUpScreen,
    authorization: AuthorizationScreen,
    linkDevice: LinkDeviceScreen,
    locks: LocksScreen,
    history: HistoryScreen,
    notifications: NotificationsScreen,
    profile: ProfileScreen,
    automation: AutomationScreen,
    cameras: CamerasScreen,
    devices: DevicesScreen,
    people: PeopleScreen,
    security: SecurityScreen,
    payments: PaymentsScreen,
    settings: SettingsScreen,
    packages: PackagesScreen,
    help: HelpScreen,
};

const Navigation = ({ colorScheme, user, token }) => {
    const permission = usePermission(user, token);
    const routes = Object.keys(Routing);
    return (
        <NavigationContainer
            linking={Linking}
            theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                }}>
                {routes.map((routeKey, index) => {
                    const route = Routing[routeKey];

                    let ScreenComponent = null;

                    if (route.screens) {
                        const screens = Object.keys(route.screens);
                        const NestedStack = createStackNavigator();
                        ScreenComponent = () => (
                            <NestedStack.Navigator
                                screenOptions={{
                                    headerShown: false,
                                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                                }}>
                                {screens.map((screenKey, index) => {
                                    const screen = route.screens[screenKey];
                                    if (permission(screen.permissions)) {
                                        const NestedScreenComponent = SCREEN_COMPONENTS[route.name];
                                        return (
                                            <NestedStack.Screen
                                                key={screenKey + index}
                                                name={screen.name}
                                                component={NestedScreenComponent}
                                                initialParams={screen.params}
                                            />
                                        )
                                    }

                                    return null;
                                })}
                            </NestedStack.Navigator>
                        );
                    } else {
                        ScreenComponent = SCREEN_COMPONENTS[route.name];
                    }

                    if (permission(route.permissions)) {
                        return (
                            <Stack.Screen
                                key={routeKey + index}
                                name={route.name}
                                component={ScreenComponent}
                                initialParams={route.params}
                            />
                        )
                    }

                    return null;
                })}
            </Stack.Navigator>
        </NavigationContainer>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

export default connect(mapStateToProps)(Navigation);
