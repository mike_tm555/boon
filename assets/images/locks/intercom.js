export default
`<svg width="31" height="50" viewBox="0 0 31 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.5">
<rect x="4.42847" y="6.36365" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="5.31421" y="7.27283" width="4.42857" height="4.54545" rx="2.21429" fill="url(#paint0_linear)"/>
<rect x="6.19995" y="8.18188" width="2.65714" height="2.72727" rx="1.32857" fill="#717889"/>
<rect x="4.42847" y="21.8182" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="12.3999" y="21.8182" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="20.3716" y="21.8182" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="4.42847" y="30" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="12.3999" y="30" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="20.3716" y="30" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="4.42847" y="38.1819" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="12.3999" y="38.1819" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<rect x="20.3716" y="38.1819" width="6.2" height="6.36364" rx="3.1" fill="#717889"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 0C2.23858 0 0 2.23858 0 5V45C0 47.7614 2.23858 50 5 50H26C28.7614 50 31 47.7614 31 45V5C31 2.23858 28.7614 0 26 0H5ZM5.54297 3.63635C4.4384 3.63635 3.54297 4.53178 3.54297 5.63635V44.3636C3.54297 45.4682 4.43839 46.3636 5.54296 46.3636H25.4573C26.5618 46.3636 27.4573 45.4682 27.4573 44.3636V5.63635C27.4573 4.53178 26.5618 3.63635 25.4573 3.63635H5.54297Z" fill="#717889"/>
</g>
<defs>
<linearGradient id="paint0_linear" x1="9.74278" y1="9.54555" x2="4.86342" y2="8.68121" gradientUnits="userSpaceOnUse">
<stop stop-color="#9E9E9E"/>
<stop offset="1" stop-color="#9E9E9E" stop-opacity="0"/>
</linearGradient>
</defs>
</svg>`

