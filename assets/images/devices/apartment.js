export default
    `<svg width="375" height="112" viewBox="0 0 375 112" fill="none" xmlns="http://www.w3.org/2000/svg">
<ellipse opacity="0.2" cx="188.13" cy="102.84" rx="40.1295" ry="5.15951" fill="#717889"/>
<path d="M169.785 32.7409C169.785 29.9795 172.023 27.7409 174.785 27.7409H201.475C204.236 27.7409 206.475 29.9795 206.475 32.7409V91.0947C206.475 92.1993 205.579 93.0947 204.475 93.0947H171.785C170.68 93.0947 169.785 92.1993 169.785 91.0947V32.7409Z" fill="#717889"/>
<g filter="url(#filter0_i)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M199.595 32.2125H176.664C175.397 32.2125 174.371 33.1365 174.371 34.2763V92.0628H201.888V34.2763C201.888 33.1365 200.861 32.2125 199.595 32.2125ZM176.664 31.1806C174.764 31.1806 173.224 32.5666 173.224 34.2763V93.0947H203.035V34.2763C203.035 32.5666 201.495 31.1806 199.595 31.1806H176.664Z" fill="url(#paint0_linear)"/>
</g>
<g filter="url(#filter1_i)">
<path d="M174.371 35.3655C174.371 33.6875 175.792 32.3271 177.546 32.3271H198.713C200.466 32.3271 201.888 33.6875 201.888 35.3655V93.0947H174.371V35.3655Z" fill="url(#paint1_linear)"/>
</g>
<circle cx="188.129" cy="43.7927" r="1.14656" fill="#E9E6E6"/>
<rect x="197.302" y="57.5514" width="2.29312" height="9.17247" rx="1" fill="#E9E6E6"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M243 13.4089C243 20.6911 235.043 26.5944 225.228 26.5944C221.396 26.5944 217.847 25.6944 214.944 24.1637L206.475 26.6398L210.018 20.2321C208.392 18.2415 207.456 15.9063 207.456 13.4089C207.456 6.12684 215.413 0.223526 225.228 0.223526C227.577 0.223526 229.819 0.561535 231.871 1.17556C231.851 1.18699 231.831 1.1992 231.812 1.21219C231.666 1.31961 231.544 1.44657 231.446 1.59305C231.358 1.73954 231.29 1.8909 231.241 2.04715C231.202 2.2034 231.178 2.33036 231.168 2.42801L230.846 4.33231H228.355C228.434 3.88309 228.492 3.53153 228.531 3.27762C228.58 3.01395 228.614 2.81864 228.634 2.69168C228.653 2.55497 228.663 2.46708 228.663 2.42801C228.673 2.37918 228.678 2.34012 228.678 2.31083C228.678 2.03739 228.634 1.81766 228.546 1.65165C228.458 1.48563 228.341 1.35868 228.194 1.27079C228.058 1.1829 227.896 1.1243 227.711 1.09501C227.525 1.06571 227.34 1.05106 227.154 1.05106C226.92 1.05106 226.725 1.10477 226.568 1.21219C226.422 1.31961 226.3 1.44657 226.202 1.59305C226.114 1.73954 226.046 1.8909 225.997 2.04715C225.958 2.2034 225.934 2.33036 225.924 2.42801L225.602 4.33231H223.697C223.492 4.33231 223.292 4.34208 223.097 4.36161C222.901 4.38114 222.73 4.43485 222.584 4.52274C222.437 4.61063 222.315 4.74735 222.218 4.9329C222.13 5.11844 222.086 5.37723 222.086 5.70926C222.086 6.012 222.13 6.25126 222.218 6.42704C222.315 6.60282 222.437 6.73954 222.584 6.83719C222.73 6.92508 222.901 6.98368 223.097 7.01297C223.292 7.04227 223.492 7.05692 223.697 7.05692H225.133L224.547 10.3382H222.672C222.467 10.3382 222.267 10.3479 222.071 10.3675C221.876 10.387 221.705 10.4407 221.559 10.5286C221.412 10.6165 221.29 10.7532 221.192 10.9388C221.104 11.1243 221.061 11.3831 221.061 11.7151C221.061 12.0276 221.104 12.2718 221.192 12.4475C221.29 12.6233 221.412 12.76 221.559 12.8577C221.705 12.9456 221.876 13.0042 222.071 13.0335C222.267 13.053 222.467 13.0628 222.672 13.0628H224.107C224.02 13.5804 223.951 13.9905 223.902 14.2932C223.854 14.5862 223.819 14.8108 223.8 14.9671C223.78 15.1233 223.766 15.2307 223.756 15.2893V15.4065C223.756 15.6897 223.8 15.9095 223.888 16.0657C223.976 16.222 224.093 16.344 224.239 16.4319C224.386 16.51 224.547 16.5589 224.723 16.5784C224.908 16.5979 225.094 16.6077 225.279 16.6077C225.494 16.6077 225.675 16.554 225.821 16.4466C225.978 16.3391 226.104 16.1975 226.202 16.0218C226.31 15.8362 226.393 15.6214 226.451 15.3772C226.52 15.1331 226.578 14.8694 226.627 14.5862L226.861 13.0628H229.352C229.264 13.5804 229.195 13.9905 229.146 14.2932C229.098 14.5862 229.063 14.8108 229.044 14.9671C229.024 15.1233 229.01 15.2307 229 15.2893V15.4065C229 15.6897 229.044 15.9095 229.132 16.0657C229.22 16.222 229.337 16.344 229.483 16.4319C229.63 16.51 229.791 16.5589 229.967 16.5784C230.152 16.5979 230.338 16.6077 230.523 16.6077C230.738 16.6077 230.919 16.554 231.065 16.4466C231.222 16.3391 231.349 16.1975 231.446 16.0218C231.554 15.8362 231.637 15.6214 231.695 15.3772C231.764 15.1331 231.822 14.8694 231.871 14.5862L232.105 13.0628H233.805C233.99 13.0628 234.181 13.053 234.376 13.0335C234.571 13.0042 234.742 12.9456 234.889 12.8577C235.045 12.76 235.172 12.6233 235.27 12.4475C235.367 12.2718 235.416 12.0276 235.416 11.7151C235.416 11.3831 235.367 11.1243 235.27 10.9388C235.172 10.7532 235.045 10.6165 234.889 10.5286C234.742 10.4407 234.571 10.387 234.376 10.3675C234.181 10.3479 233.99 10.3382 233.805 10.3382H232.574L233.16 7.05692H234.859C235.045 7.05692 235.235 7.04227 235.431 7.01297C235.626 6.98368 235.797 6.92508 235.943 6.83719C236.1 6.73954 236.227 6.60282 236.324 6.42704C236.422 6.25126 236.471 6.012 236.471 5.70926C236.471 5.37723 236.422 5.11844 236.324 4.9329C236.227 4.74735 236.1 4.61063 235.943 4.52274C235.797 4.43485 235.626 4.38114 235.431 4.36161C235.235 4.34208 235.045 4.33231 234.859 4.33231H233.6C233.678 3.88309 233.736 3.53153 233.775 3.27762C233.824 3.01395 233.858 2.81864 233.878 2.69168C233.897 2.55497 233.907 2.46708 233.907 2.42801C233.917 2.37918 233.922 2.34012 233.922 2.31083C233.922 2.15158 233.907 2.01055 233.877 1.88774C239.319 4.14153 243 8.45637 243 13.4089ZM229.791 10.3382H227.33L227.916 7.05692H230.377L229.791 10.3382Z" fill="url(#paint2_linear)"/>
<g style="mix-blend-mode:multiply">
<ellipse opacity="0.2" cx="98.1295" cy="102.84" rx="40.1295" ry="5.15951" fill="#E9E6E6"/>
<path d="M79.7847 32.7409C79.7847 29.9795 82.0232 27.7409 84.7847 27.7409H111.475C114.236 27.7409 116.475 29.9795 116.475 32.7409V91.0947C116.475 92.1993 115.579 93.0947 114.475 93.0947H81.7847C80.6801 93.0947 79.7847 92.1993 79.7847 91.0947V32.7409Z" fill="#E9E6E6"/>
<g filter="url(#filter2_i)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M109.595 32.2125H86.6638C85.3973 32.2125 84.3707 33.1365 84.3707 34.2763V92.0628H111.888V34.2763C111.888 33.1365 110.861 32.2125 109.595 32.2125ZM86.6638 31.1806C84.7641 31.1806 83.2241 32.5666 83.2241 34.2763V93.0947H113.035V34.2763C113.035 32.5666 111.495 31.1806 109.595 31.1806H86.6638Z" fill="#E9E6E6"/>
</g>
<g filter="url(#filter3_i)">
<path d="M84.3706 35.3655C84.3706 33.6875 85.7921 32.3271 87.5457 32.3271H108.713C110.466 32.3271 111.888 33.6875 111.888 35.3655V93.0947H84.3706V35.3655Z" fill="#E9E6E6"/>
</g>
<circle cx="98.1295" cy="43.7927" r="1.14656" fill="#E9E6E6"/>
<rect x="107.302" y="57.5514" width="2.29312" height="9.17247" rx="1" fill="#E9E6E6"/>
</g>
<g style="mix-blend-mode:multiply" filter="url(#filter4_f)">
<ellipse opacity="0.2" cx="8.12954" cy="102.84" rx="40.1295" ry="5.15951" fill="#E9E6E6"/>
<path d="M-10.2153 32.7409C-10.2153 29.9795 -7.97676 27.7409 -5.21533 27.7409H21.4745C24.236 27.7409 26.4745 29.9795 26.4745 32.7409V91.0947C26.4745 92.1993 25.5791 93.0947 24.4745 93.0947H-8.21533C-9.3199 93.0947 -10.2153 92.1993 -10.2153 91.0947V32.7409Z" fill="#E9E6E6"/>
<g filter="url(#filter5_i)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M19.595 32.2125H-3.3362C-4.60266 32.2125 -5.62932 33.1365 -5.62932 34.2763V92.0628H21.8881V34.2763C21.8881 33.1365 20.8614 32.2125 19.595 32.2125ZM-3.3362 31.1806C-5.23588 31.1806 -6.77588 32.5666 -6.77588 34.2763V93.0947H23.0346V34.2763C23.0346 32.5666 21.4946 31.1806 19.595 31.1806H-3.3362Z" fill="#E9E6E6"/>
</g>
<g filter="url(#filter6_i)">
<path d="M-5.62939 35.3655C-5.62939 33.6875 -4.20786 32.3271 -2.45431 32.3271H18.7129C20.4665 32.3271 21.888 33.6875 21.888 35.3655V93.0947H-5.62939V35.3655Z" fill="#E9E6E6"/>
</g>
<circle cx="8.12947" cy="43.7927" r="1.14656" fill="#E9E6E6"/>
<rect x="17.3022" y="57.5514" width="2.29312" height="9.17247" rx="1" fill="#E9E6E6"/>
</g>
<g style="mix-blend-mode:multiply" filter="url(#filter7_f)">
<ellipse opacity="0.2" cx="364.13" cy="102.84" rx="40.1295" ry="5.15951" fill="#E9E6E6"/>
<path d="M345.785 32.7409C345.785 29.9795 348.023 27.7409 350.785 27.7409H377.475C380.236 27.7409 382.475 29.9795 382.475 32.7409V91.0947C382.475 92.1993 381.579 93.0947 380.475 93.0947H347.785C346.68 93.0947 345.785 92.1993 345.785 91.0947V32.7409Z" fill="#E9E6E6"/>
<g filter="url(#filter8_i)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M375.595 32.2125H352.664C351.397 32.2125 350.371 33.1365 350.371 34.2763V92.0628H377.888V34.2763C377.888 33.1365 376.861 32.2125 375.595 32.2125ZM352.664 31.1806C350.764 31.1806 349.224 32.5666 349.224 34.2763V93.0947H379.035V34.2763C379.035 32.5666 377.495 31.1806 375.595 31.1806H352.664Z" fill="#E9E6E6"/>
</g>
<g filter="url(#filter9_i)">
<path d="M350.371 35.3655C350.371 33.6875 351.792 32.3271 353.546 32.3271H374.713C376.466 32.3271 377.888 33.6875 377.888 35.3655V93.0947H350.371V35.3655Z" fill="#E9E6E6"/>
</g>
<circle cx="364.129" cy="43.7927" r="1.14656" fill="#E9E6E6"/>
<rect x="373.302" y="57.5514" width="2.29312" height="9.17247" rx="1" fill="#E9E6E6"/>
</g>
<g style="mix-blend-mode:multiply">
<ellipse opacity="0.2" cx="274.13" cy="102.84" rx="40.1295" ry="5.15951" fill="#E9E6E6"/>
<path d="M255.785 32.7409C255.785 29.9795 258.023 27.7409 260.785 27.7409H287.475C290.236 27.7409 292.475 29.9795 292.475 32.7409V91.0947C292.475 92.1993 291.579 93.0947 290.475 93.0947H257.785C256.68 93.0947 255.785 92.1993 255.785 91.0947V32.7409Z" fill="#E9E6E6"/>
<g filter="url(#filter10_i)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M285.595 32.2125H262.664C261.397 32.2125 260.371 33.1365 260.371 34.2763V92.0628H287.888V34.2763C287.888 33.1365 286.861 32.2125 285.595 32.2125ZM262.664 31.1806C260.764 31.1806 259.224 32.5666 259.224 34.2763V93.0947H289.035V34.2763C289.035 32.5666 287.495 31.1806 285.595 31.1806H262.664Z" fill="#E9E6E6"/>
</g>
<g filter="url(#filter11_i)">
<path d="M260.371 35.3655C260.371 33.6875 261.792 32.3271 263.546 32.3271H284.713C286.466 32.3271 287.888 33.6875 287.888 35.3655V93.0947H260.371V35.3655Z" fill="#E9E6E6"/>
</g>
<circle cx="274.129" cy="43.7927" r="1.14656" fill="#E9E6E6"/>
<rect x="283.302" y="57.5514" width="2.29312" height="9.17247" rx="1" fill="#E9E6E6"/>
</g>
<defs>
<filter id="filter0_i" x="173.224" y="31.1806" width="29.8105" height="61.9142" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter1_i" x="174.371" y="32.3271" width="27.5174" height="60.7676" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter2_i" x="83.2241" y="31.1806" width="29.8105" height="61.9142" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter3_i" x="84.3706" y="32.3271" width="27.5174" height="60.7676" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter4_f" x="-36" y="23.7409" width="88.2591" height="88.2591" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feGaussianBlur stdDeviation="2" result="effect1_foregroundBlur"/>
</filter>
<filter id="filter5_i" x="-6.77588" y="31.1806" width="29.8105" height="61.9142" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter6_i" x="-5.62939" y="32.3271" width="27.5174" height="60.7676" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter7_f" x="320" y="23.7409" width="88.2591" height="88.2591" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feGaussianBlur stdDeviation="2" result="effect1_foregroundBlur"/>
</filter>
<filter id="filter8_i" x="349.224" y="31.1806" width="29.8105" height="61.9142" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter9_i" x="350.371" y="32.3271" width="27.5174" height="60.7676" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter10_i" x="259.224" y="31.1806" width="29.8105" height="61.9142" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<filter id="filter11_i" x="260.371" y="32.3271" width="27.5174" height="60.7676" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"/>
<feBlend mode="normal" in2="shape" result="effect1_innerShadow"/>
</filter>
<linearGradient id="paint0_linear" x1="203.035" y1="31.1806" x2="175.218" y2="93.9778" gradientUnits="userSpaceOnUse">
<stop stop-color="#F2F1FF"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint1_linear" x1="174.371" y1="93.0947" x2="218.547" y2="74.0905" gradientUnits="userSpaceOnUse">
<stop stop-color="colors.primary"/>
<stop offset="1" stop-color="colors.secondary"/>
</linearGradient>
<linearGradient id="paint2_linear" x1="206.475" y1="26.6398" x2="231.972" y2="-6.85133" gradientUnits="userSpaceOnUse">
<stop stop-color="colors.primary"/>
<stop offset="1" stop-color="colors.secondary"/>
</linearGradient>
</defs>
</svg>`

