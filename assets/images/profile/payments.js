export default
        `<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M20 4H4C2.89 4 2.01 4.89 2.01 6L2 18C2 19.11 2.89 20 4 20H20C21.11 20 22 19.11 22 18V6C22 4.89 21.11 4 20 4ZM20.5 18.5H3.5V12H20.5V18.5ZM20.5 9H3.5V5.5H20.5V9Z" fill="colors.fill"/>
</svg>`

