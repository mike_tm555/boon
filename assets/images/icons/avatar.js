export default `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<ellipse opacity="0.8" cx="25" cy="25" rx="25" ry="25" fill="url(#paint0_linear)"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M-1.17187e-05 22.7676C1.12924 10.0063 11.8469 0 24.9017 0C32.7308 0 39.7193 3.59881 44.3033 9.2324C43.174 21.9937 32.4564 32 19.4016 32C11.5725 32 4.58396 28.4012 -1.17187e-05 22.7676Z" fill="url(#paint1_linear)"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M5.59839 40.7676C6.72764 28.0063 17.4453 18 30.5001 18C38.3292 18 45.3177 21.5988 49.9017 27.2324C48.7724 39.9937 38.0548 50 25 50C17.1709 50 10.1824 46.4012 5.59839 40.7676Z" fill="url(#paint2_linear)"/>
<g filter="url(#filter0_b)">
<circle cx="25.3333" cy="25.0001" r="18.3333" fill="#929292" fill-opacity="0.17"/>
</g>
<g clip-path="url(#clip0)">
<path d="M25 16.1439C21.8312 16.1439 19.3125 18.6627 19.3125 21.8314C19.3125 23.7814 20.2875 25.5689 21.8313 26.5439C18.9063 27.6814 16.875 30.6064 16.875 33.9377H18.5C18.5 30.3627 21.425 27.4377 25 27.4377C25.4875 27.4377 25.8938 27.5189 26.3 27.6002C26.3374 27.549 26.3869 27.5051 26.4268 27.4547C25.5042 28.6013 25.0009 30.0285 25 31.5002C25 35.0752 27.925 38.0002 31.5 38.0002C35.075 38.0002 38 35.0752 38 31.5002C38 27.9252 35.075 25.0002 31.5 25.0002C30.7976 25.0015 30.1002 25.1179 29.4354 25.3447C30.1069 24.512 30.5283 23.506 30.6507 22.4434C30.7732 21.3808 30.5916 20.3053 30.1271 19.3417C29.6626 18.3782 28.9343 17.5663 28.0267 17.0002C27.1192 16.4342 26.0696 16.1372 25 16.1439ZM25 17.6877C27.275 17.6877 29.0625 19.4752 29.0625 21.7502C29.0625 24.0252 27.275 25.8127 25 25.8127C22.725 25.8127 20.9375 24.0252 20.9375 21.7502C20.9375 19.4752 22.725 17.6877 25 17.6877ZM27.6609 26.2725L27.5911 26.3237C27.6142 26.3065 27.6375 26.2894 27.6609 26.2725ZM31.5 26.6252C34.1812 26.6252 36.375 28.8189 36.375 31.5002C36.375 34.1814 34.1812 36.3752 31.5 36.3752C28.8188 36.3752 26.625 34.1814 26.625 31.5002C26.625 28.8189 28.8188 26.6252 31.5 26.6252ZM30.6875 28.2502V31.9877L29.3062 33.3689L30.4438 34.5064L32.3125 32.6377V28.2502H30.6875Z" fill="white"/>
</g>
<defs>
<filter id="filter0_b" x="5" y="4.66675" width="40.6667" height="40.6667" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feGaussianBlur in="BackgroundImage" stdDeviation="1"/>
<feComposite in2="SourceAlpha" operator="in" result="effect1_backgroundBlur"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_backgroundBlur" result="shape"/>
</filter>
<linearGradient id="paint0_linear" x1="44.5" y1="4.99999" x2="7.00007" y2="50" gradientUnits="userSpaceOnUse">
<stop stop-color="#8D0642"/>
<stop offset="1" stop-color="#0085FF"/>
</linearGradient>
<linearGradient id="paint1_linear" x1="48.3333" y1="12.5" x2="15.9839" y2="42.5351" gradientUnits="userSpaceOnUse">
<stop stop-color="#8D0642"/>
<stop offset="1" stop-color="#0085FF"/>
</linearGradient>
<linearGradient id="paint2_linear" x1="12.9167" y1="48.3333" x2="34.9827" y2="9.89722" gradientUnits="userSpaceOnUse">
<stop stop-color="#8D0642"/>
<stop offset="1" stop-color="#0085FF"/>
</linearGradient>
<clipPath id="clip0">
<rect width="26" height="26" fill="white" transform="translate(12 12)"/>
</clipPath>
</defs>
</svg>`;
