export default
    `<svg width="75" height="81" viewBox="0 0 75 81" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M17 15C17 12.2386 19.2386 10 22 10H47C49.7614 10 52 12.2386 52 15V60C52 62.7614 49.7614 65 47 65H22C19.2386 65 17 62.7614 17 60V15ZM21 16.5C21 15.3954 21.8954 14.5 23 14.5H46C47.1046 14.5 48 15.3954 48 16.5V55.5C48 56.6046 47.1046 57.5 46 57.5H23C21.8954 57.5 21 56.6046 21 55.5V16.5ZM35 63C36.1046 63 37 62.1046 37 61C37 59.8954 36.1046 59 35 59C33.8954 59 33 59.8954 33 61C33 62.1046 33.8954 63 35 63Z" fill="#717889"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M59.3567 23C67.917 23 74.8567 17.8513 74.8567 11.5C74.8567 5.14871 67.917 0 59.3567 0C50.7964 0 43.8567 5.14871 43.8567 11.5C43.8567 13.678 44.6726 15.7146 46.0903 17.4506L43 23.0396L50.387 20.8799C52.9187 22.215 56.0142 23 59.3567 23Z" fill="url(#paint0_linear)"/>
<circle cx="51.5" cy="11.5" r="2.5" fill="white"/>
<circle cx="59.5" cy="11.5" r="2.5" fill="white"/>
<circle cx="67.5" cy="11.5" r="2.5" fill="white"/>
<ellipse opacity="0.2" cx="35" cy="76.5" rx="35" ry="4.5" fill="#717889"/>
<defs>
<linearGradient id="paint0_linear" x1="43" y1="23.0396" x2="65.2377" y2="-6.17086" gradientUnits="userSpaceOnUse">
<stop stop-color="colors.primary"/>
<stop offset="1" stop-color="colors.secondary"/>
</linearGradient>
</defs>
</svg>`

