export default
    `<svg width="70" height="70" viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="35" cy="35" r="34.5" fill="url(#paint0_linear)" stroke="url(#paint1_linear)"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M35 24.3125C35.3149 24.3125 35.617 24.4376 35.8397 24.6603C36.0624 24.883 36.1875 25.1851 36.1875 25.5V35C36.1875 35.3149 36.0624 35.617 35.8397 35.8397C35.617 36.0624 35.3149 36.1875 35 36.1875H25.5C25.1851 36.1875 24.883 36.0624 24.6603 35.8397C24.4376 35.617 24.3125 35.3149 24.3125 35C24.3125 34.6851 24.4376 34.383 24.6603 34.1603C24.883 33.9376 25.1851 33.8125 25.5 33.8125H33.8125V25.5C33.8125 25.1851 33.9376 24.883 34.1603 24.6603C34.383 24.4376 34.6851 24.3125 35 24.3125Z" fill="url(#paint2_linear)"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M33.8125 35C33.8125 34.6851 33.9376 34.383 34.1603 34.1603C34.383 33.9376 34.6851 33.8125 35 33.8125H44.5C44.8149 33.8125 45.117 33.9376 45.3397 34.1603C45.5624 34.383 45.6875 34.6851 45.6875 35C45.6875 35.3149 45.5624 35.617 45.3397 35.8397C45.117 36.0624 44.8149 36.1875 44.5 36.1875H36.1875V44.5C36.1875 44.8149 36.0624 45.117 35.8397 45.3397C35.617 45.5624 35.3149 45.6875 35 45.6875C34.6851 45.6875 34.383 45.5624 34.1603 45.3397C33.9376 45.117 33.8125 44.8149 33.8125 44.5V35Z" fill="url(#paint3_linear)"/>
<defs>
<linearGradient id="paint0_linear" x1="70" y1="0" x2="52.6743" y2="81.2344" gradientUnits="userSpaceOnUse">
<stop stop-color="#F2F1FF"/>
<stop offset="1" stop-color="white"/>
</linearGradient>
<linearGradient id="paint1_linear" x1="-2.84892e-06" y1="70" x2="70" y2="3.5" gradientUnits="userSpaceOnUse">
<stop stop-color="colors.primary"/>
<stop offset="1" stop-color="colors.secondary"/>
</linearGradient>
<linearGradient id="paint2_linear" x1="24.3125" y1="36.1875" x2="36.1875" y2="24.9063" gradientUnits="userSpaceOnUse">
<stop stop-color="colors.primary"/>
<stop offset="1" stop-color="colors.secondary"/>
</linearGradient>
<linearGradient id="paint3_linear" x1="33.8125" y1="45.6875" x2="45.6875" y2="34.4063" gradientUnits="userSpaceOnUse">
<stop stop-color="colors.primary"/>
<stop offset="1" stop-color="colors.secondary"/>
</linearGradient>
</defs>
</svg>`

