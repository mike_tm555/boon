export default
    `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16 18C16 17.4477 16.4477 17 17 17H33C33.5523 17 34 17.4477 34 18V27C34 28.6569 32.6569 30 31 30H19C17.3431 30 16 28.6569 16 27V18Z" stroke="colors.fill" stroke-width="2"/>
<rect x="20" y="9" width="3" height="8" rx="1" stroke="colors.fill" stroke-width="2"/>
<rect x="27" y="9" width="3" height="8" rx="1" stroke="colors.fill" stroke-width="2"/>
<circle cx="25" cy="25" r="1.5" stroke="colors.fill"/>
<rect x="23" y="19" width="4" height="1" rx="0.5" fill="colors.fill"/>
<rect x="23" y="21" width="4" height="1" rx="0.5" fill="colors.fill"/>
<path d="M25 29V34C25 35.1046 25.8954 36 27 36H29" stroke="colors.fill" stroke-width="2"/>
<path d="M31 43V38C31 36.8954 30.1046 36 29 36H27" stroke="colors.fill" stroke-width="2"/>
</svg>`

