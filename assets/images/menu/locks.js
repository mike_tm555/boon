export default
    `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="14" y="23" width="22" height="16" rx="1" stroke="colors.fill" stroke-width="2"/>
<path d="M18.8387 16C18.8387 12.6863 21.525 10 24.8387 10C28.1524 10 30.8387 12.6863 30.8387 16V23H18.8387V16Z" stroke="colors.fill" stroke-width="2"/>
<path d="M26.5 31C26.5 31.8284 25.8284 32.5 25 32.5C24.1716 32.5 23.5 31.8284 23.5 31C23.5 30.1716 24.1716 29.5 25 29.5C25.8284 29.5 26.5 30.1716 26.5 31Z" stroke="colors.fill"/>
</svg>`

