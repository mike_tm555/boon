export default
    `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9 15C9 13.8954 9.89543 13 11 13H39C40.1046 13 41 13.8954 41 15V30H9V15Z" stroke="colors.fill" stroke-width="2"/>
<path d="M9 33H41V35C41 36.1046 40.1046 37 39 37H11C9.89543 37 9 36.1046 9 35V33Z" stroke="colors.fill" stroke-width="2"/>
<rect x="13" y="21" width="6" height="4" rx="1" stroke="colors.fill" stroke-width="2"/>
</svg>`

