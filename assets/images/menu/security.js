export default
    `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.8333 27.3821C16.8333 23.5161 19.9673 20.3821 23.8333 20.3821H26.1666C30.0326 20.3821 33.1666 23.5161 33.1666 27.3821V35.2683C33.1666 35.8205 32.7189 36.2683 32.1666 36.2683H17.8333C17.281 36.2683 16.8333 35.8205 16.8333 35.2683V27.3821Z" stroke="colors.fill" stroke-width="2"/>
<rect x="13.5" y="36.6423" width="23" height="3.69106" rx="1" stroke="colors.fill" stroke-width="2"/>
<rect x="24.3889" y="24.0555" width="1.22222" height="5.66667" rx="0.611111" stroke="colors.fill"/>
<rect x="23.8333" y="31.2642" width="2.33333" height="2.25203" rx="1.12602" stroke="colors.fill"/>
<rect x="7.9065" y="33.6098" width="0.813008" height="5.85366" rx="0.406504" transform="rotate(-90 7.9065 33.6098)" fill="black" stroke="colors.fill" stroke-width="0.813008"/>
<rect x="0.509539" y="-0.276883" width="0.814662" height="8.62406" rx="0.407331" transform="matrix(0.290121 -0.95699 0.9608 0.277241 6.46099 20.4159)" fill="black" stroke="colors.fill" stroke-width="0.814662"/>
<rect x="24.5834" y="8.41667" width="0.833333" height="8.39307" rx="0.416667" fill="black" stroke="colors.fill" stroke-width="0.833333"/>
<rect x="10.4065" y="27.9186" width="0.813008" height="3.35366" rx="0.406504" transform="rotate(-90 10.4065 27.9186)" fill="black" stroke="colors.fill" stroke-width="0.813008"/>
<rect x="0.580226" y="0.114847" width="0.827097" height="3.26966" rx="0.413548" transform="matrix(0.837243 -0.546831 0.565799 0.824543 15.3042 14.729)" fill="black" stroke="colors.fill" stroke-width="0.827097"/>
<rect x="5.4065" y="33.6098" width="0.813008" height="0.853658" rx="0.406504" transform="rotate(-90 5.4065 33.6098)" stroke="colors.fill" stroke-width="0.813008"/>
<rect x="-0.406504" y="-0.406504" width="0.813008" height="5.85366" rx="0.406504" transform="matrix(4.48042e-08 -1 -1 -4.26453e-08 41.687 33.2033)" fill="black" stroke="colors.fill" stroke-width="0.813008"/>
<rect x="-0.509539" y="-0.276883" width="0.814662" height="8.62406" rx="0.407331" transform="matrix(-0.290121 -0.95699 -0.9608 0.277241 43.2434 19.4406)" fill="black" stroke="colors.fill" stroke-width="0.814662"/>
<rect x="-0.406504" y="-0.406504" width="0.813008" height="3.35366" rx="0.406504" transform="matrix(4.48042e-08 -1 -1 -4.26453e-08 39.187 27.5121)" fill="black" stroke="colors.fill" stroke-width="0.813008"/>
<rect x="-0.580226" y="0.114847" width="0.827097" height="3.26966" rx="0.413548" transform="matrix(-0.837243 -0.546831 -0.565799 0.824543 33.7242 14.0944)" fill="black" stroke="colors.fill" stroke-width="0.827097"/>
<rect x="-0.406504" y="-0.406504" width="0.813008" height="0.853658" rx="0.406504" transform="matrix(4.48042e-08 -1 -1 -4.26453e-08 44.187 33.2033)" stroke="colors.fill" stroke-width="0.813008"/>
</svg>`

