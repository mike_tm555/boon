export default`<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="0.531691" y="-0.28892" width="0.850079" height="8.99898" rx="0.425039" transform="matrix(0.290121 -0.95699 0.9608 0.277241 5.65503 16.8042)" fill="colors.fill" stroke="colors.fill" stroke-width="0.850079"/>
<rect x="24.5031" y="4.37267" width="0.745344" height="8.94413" rx="0.372672" fill="colors.fill" stroke="colors.fill" stroke-width="0.745344"/>
<rect x="9.59618" y="24.497" width="0.745344" height="3.72672" rx="0.372672" transform="rotate(-90 9.59618 24.497)" fill="colors.fill" stroke="colors.fill" stroke-width="0.745344"/>
<rect x="0.605451" y="0.11984" width="0.863054" height="3.41181" rx="0.431527" transform="matrix(0.837243 -0.546831 0.565799 0.824543 14.8827 10.8701)" fill="colors.fill" stroke="colors.fill" stroke-width="0.863054"/>
<rect x="-0.531691" y="-0.28892" width="0.850079" height="8.99898" rx="0.425039" transform="matrix(-0.290121 -0.95699 -0.9608 0.277241 44.0365 15.7866)" fill="colors.fill" stroke="colors.fill" stroke-width="0.850079"/>
<rect x="-0.372672" y="-0.372672" width="0.745344" height="3.72672" rx="0.372672" transform="matrix(4.48042e-08 -1 -1 -4.26453e-08 39.7825 24.1243)" fill="colors.fill" stroke="colors.fill" stroke-width="0.745344"/>
<rect x="-0.605451" y="0.11984" width="0.863054" height="3.41181" rx="0.431527" transform="matrix(-0.837243 -0.546831 -0.565799 0.824543 34.1035 10.208)" fill="colors.fill" stroke="colors.fill" stroke-width="0.863054"/>
<circle cx="25.1243" cy="24.8696" r="8.68947" stroke="colors.fill" stroke-width="2"/>
<circle cx="25.1243" cy="24.8696" r="3.47206" stroke="colors.fill" stroke-width="2"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M34.2585 28.1106C33.9938 28.8566 33.6403 29.5605 33.2108 30.2097C33.9085 31.5042 34.3045 32.9854 34.3045 34.5591V43.7393H15.9442V34.5591C15.9442 32.9854 16.3401 31.5042 17.0379 30.2097C16.6083 29.5605 16.2548 28.8566 15.9901 28.1106C14.7014 29.9327 13.9442 32.1575 13.9442 34.5591V44.0829C13.9442 44.9977 14.6857 45.7393 15.6005 45.7393H34.6481C35.5629 45.7393 36.3045 44.9977 36.3045 44.0829V34.5591C36.3045 32.1575 35.5472 29.9328 34.2585 28.1106Z" fill="colors.fill"/>
</svg>`
