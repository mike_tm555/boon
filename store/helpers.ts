export function addItem(state: any[], action: any, identifier: string = 'id') {
    let newState = [...state];

    if (action.payload) {
        action.payload.forEach((payloadItem: any) => {
            const exist = newState.find((item: any) => {
                return item[identifier] === payloadItem[identifier];
            });

            if (!exist) {
                newState.unshift(payloadItem);
            } else {
                newState = newState.map((item) => {
                    if (item[identifier] === payloadItem[identifier]) {
                        item = {
                            ...item,
                            ...payloadItem,
                        }
                    }

                    return item;
                });
            }
        });
    }

    return newState;
}

export function deleteItem (state: any[], action: any) {
    let newState = [...state];

    if (action.payload) {
        newState = newState.filter((item) => {
            return item.id !== action.payload.id;
        })
    }

    return newState;
}
