import {
    CREATE_TOKEN,
    CREATE_USER,
    REMOVE_USER,
    REMOVE_TOKEN,
    SIGN_IN,
    SIGN_OUT,
    SET_THEME,
    ADD_NOTIFICATION,
    ADD_HISTORY,
    ADD_CAMERA,
    ADD_LOCK,
    DELETE_LOCK,
    ADD_PACKAGE,
    ADD_TOAST,
    DELETE_TOAST,
    ADD_PEOPLE,
    DELETE_PEOPLE,
    SET_APP_INFO,
    ADD_ADDRESS,
    DELETE_ADDRESS,
} from "./variables";

export const createUser = (user: any) => (
    {
        type: CREATE_USER,
        payload: user,
    }
);

export const removeUser = (user: any) => (
    {
        type: REMOVE_USER,
        payload: null,
    }
);

export const createToken = (token: string) => (
    {
        type: CREATE_TOKEN,
        payload: token,
    }
);

export const removeToken = (token: any) => (
    {
        type: REMOVE_TOKEN,
        payload: null,
    }
);

export const signIn = (token: string, user: any) => (
    {
        type: SIGN_IN,
        payload: null,
    }
);

export const signOut = () => (
    {
        type: SIGN_OUT,
        payload: null,
    }
);

export const setTheme = (theme: string = 'light') => (
    {
        type: SET_THEME,
        payload: theme,
    }
);

export const addLock = (items: any[]) => (
    {
        type: ADD_LOCK,
        payload: items,
    }
);

export const deleteLock = (id: string) => (
    {
        type: DELETE_LOCK,
        payload: {
            id: id
        },
    }
);

export const addCamera = (items: any[]) => (
    {
        type: ADD_CAMERA,
        payload: items,
    }
);

export const addPackage = (items: any[]) => (
    {
        type: ADD_PACKAGE,
        payload: items,
    }
);

export const addNotification = (items: any[]) => (
    {
        type: ADD_NOTIFICATION,
        payload: items,
    }
);

export const addHistory = (items: any[]) => (
    {
        type: ADD_HISTORY,
        payload: items,
    }
);

export const addToast = (items: any[]) => (
    {
        type: ADD_TOAST,
        payload: items,
    }
);

export const deleteToast = (id: string) => (
    {
        type: DELETE_TOAST,
        payload: {
            id
        },
    }
);

export const addPeople = (items: any[]) => (
    {
        type: ADD_PEOPLE,
        payload: items,
    }
);

export const deletePeople = (id: string) => (
    {
        type: DELETE_PEOPLE,
        payload: {
            id
        },
    }
);

export const setAppInfo = (data: any) => (
    {
        type: SET_APP_INFO,
        payload: data,
    }
);

export const deleteAddress = (id: string) => (
    {
        type: DELETE_ADDRESS,
        payload: {
            id
        },
    }
);

export const addAddress = (data: any) => (
    {
        type: ADD_ADDRESS,
        payload: data,
    }
);
