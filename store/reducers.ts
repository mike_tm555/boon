import { combineReducers } from 'redux';
import {
    CREATE_TOKEN,
    CREATE_USER,
    REMOVE_TOKEN,
    REMOVE_USER,
    SET_THEME,
    ADD_NOTIFICATION,
    ADD_HISTORY,
    ADD_CAMERA,
    DELETE_LOCK,
    ADD_LOCK,
    ADD_PACKAGE,
    ADD_TOAST,
    DELETE_TOAST,
    ADD_PEOPLE,
    DELETE_PEOPLE,
    SET_APP_INFO,
    ADD_ADDRESS,
    DELETE_ADDRESS,
} from "./variables";
import {addItem, deleteItem} from "./helpers";

const userReducer = (state: any = null, action: any) => {
    switch (action.type) {
        case CREATE_USER: {
            return {
                ...state,
                ...action.payload
            }
        }
        case REMOVE_USER: {
            return action.payload
        }
        default:
            return state
    }
};

const tokenReducer = (state: any = null, action: any) => {
    switch (action.type) {
        case CREATE_TOKEN: {
            return action.payload
        }
        case REMOVE_TOKEN: {
            return action.payload
        }
        default:
            return state
    }
};

const themeReducer = (state: any = null, action: any) => {
    switch (action.type) {
        case SET_THEME: {
            return action.payload
        }
        default:
            return state
    }
};

const locksReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_LOCK: {
            return addItem(state, action);
        }
        case DELETE_LOCK: {
            return deleteItem(state, action);
        }
        default:
            return state
    }
};

const packagesReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_PACKAGE: {
            return addItem(state, action, 'packageId');
        }
        default:
            return state
    }
};

const notificationsReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_NOTIFICATION: {
            return addItem(state, action);
        }
        default:
            return state
    }
};

const camerasReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_CAMERA: {
            return addItem(state, action);
        }
        default:
            return state
    }
};

const historyReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_HISTORY: {
            return addItem(state, action);
        }
        default:
            return state;
    }
};

const toastsReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_TOAST: {
            return addItem(state, action);
        }
        case DELETE_TOAST: {
            return deleteItem(state, action);
        }
        default:
            return state;
    }
};

const peopleReducer = (state: any[] = [], action: any) => {
    switch (action.type) {
        case ADD_PEOPLE: {
            return addItem(state, action, 'userId');
        }
        case DELETE_PEOPLE: {
            return deleteItem(state, action);
        }
        default:
            return state;
    }
};

const appInfoReducer = (state: any = {}, action: any) => {
    switch (action.type) {
        case SET_APP_INFO: {
            return {
                ...state,
                ...action.payload
            };
        }
        default:
            return state;
    }
};

const addressesReducer = (state: any = [], action: any) => {
    switch (action.type) {
        case ADD_ADDRESS: {
            return addItem(state, action);
        }
        case DELETE_ADDRESS: {
            return deleteItem(state, action);
        }
        default:
            return state;
    }
};

export default combineReducers({
    user: userReducer,
    token: tokenReducer,
    theme: themeReducer,
    locks: locksReducer,
    people: peopleReducer,
    appInfo: appInfoReducer,
    toasts: toastsReducer,
    packages: packagesReducer,
    notifications: notificationsReducer,
    history: historyReducer,
    cameras: camerasReducer,
    addresses: addressesReducer,
});
