export function getDate(date) {
    let date_ob = new Date(date);

    let day = ("0" + date_ob.getDate()).slice(-2);

    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    let year = date_ob.getFullYear();

    return year + "-" + month + "-" + day;
}

export function getTime(date) {
    let date_ob = new Date(date);

    let hours = ("0" + date_ob.getHours()).slice(-2);

    let minutes = ("0" + (date_ob.getMinutes() + 1)).slice(-2);


    return hours + ":" + minutes;
}