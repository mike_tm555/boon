export default {
  signUpBtn: 'Register',
  signInBtn: 'Sign In',
  getStarted: 'Get Started',
  selectLanguage: 'Select Language',
  language: 'Language',
  search: "Search",
  agreeWith: "I agree with",
  termsAndConditions: 'terms and conditions',
  phone: "Login number",
  next: 'Next',
  save: 'Save',
  cancel: 'Cancel',
  back: 'Back',
  resendCode: 'Send SMS again',
  name: 'Name',
  surname: 'Surname',
  email: 'Email Address',
  intercomHelp: 'Where do i find my boon intercom code?',
  city: 'City',
  street: 'Street',
  building: 'Building',
  yes: 'Yes',
  ok: 'Ok',
  no: 'no',
  or: 'Or',
  apartment: 'Apartment Number',
  flat: 'Flat',
  continueFB: 'Continue with facebook',
  finish: 'Finish',
  uploadFailed: 'Upload failed.',
  tryAgain: 'Please try again.',
  loginFailed: 'Login failed with error: %{message}',
  loginCanceled: 'Login was cancelled',
  loginSuccess: 'Login was successful with permissions: %{permissions}',
  loggedOut: "UserItem logged out",
  addLock: 'Add Lock',
  addDevice: 'Add Device',
  addPeople: 'Add People',
  locked: 'Locked',
  unlocked: 'UnLocked',
  enableNotifications: 'Notifications',
  enableAdds: 'Enable Adds',
  appVersion: 'Application Version %{version}',
  read: 'Read',
  unread: 'UnRead',
  changePhoto: 'Change Photo',
  addPhoto: 'Add Photo',
  "code.already.used": "Code already used",
  hi: 'Hi, %{name}!',
  fullAddress: '%{city}, %{street} str. %{building}',
  activeUsers: '%{users} Active users',
  open: 'Open',
  close: 'Close',
  edit: 'Edit',
  liveCam: 'Live Cam',
  delete: 'Delete',
  activity: 'Activity',
  perMonth: 'Per Month',
  gridView: 'Grid View',
  sliderView: 'Slider View',
  'The code is invalid': 'The code is invalid!',
  writeMessage: 'Write a Message',
  beDescriptive: 'Please be descriptive',
  sendMessage: 'Send Message',
  faq: 'FAQ',
  disable: 'Disable',
  enable: 'Enable',
  active: 'Active',
  inactive: 'Inactive',
  'invalid.code': 'The code is invalid',
  addresses: 'Addresses',
  live: 'Live',
  generateCode: 'Generate Code',
  shareCode: 'Share Code',
  openedBy: 'Opened by',
  intercomCode: 'Intercom Code: %{code}',
  editIntercomName: 'Edit Intercom Name',
  activePackage: 'Package: %{package}',
  noActivePackage: 'Inactive',
  //errors
  errors: {
    noSignal: 'No Signal',
  },
  orderChip: {
    title: 'Order Chip',
    description: 'Do You want to order chip for selected address?',
    success: 'The Chip has been ordered successfully!'
  },
  contactUs: {
    title: 'contactUs',
    success: 'Your message has been successfully sent!',
  },
  //screens
  login: {
    header: 'Sign In',
    title: 'Sign In',
    description: 'Fill up your phone number, please?'
  },
  register: {
    header: 'Register',
    title: 'Register',
    description: 'Fill up your phone number, please?'
  },
  authorize: {
    header: 'Authorize',
    title: 'Authorize',
    description: 'Fill up your phone number, please?'
  },
  activate: {
    header: 'Activation Code',
    title: 'Activation Code',
    description: 'Please type the activation code send to phone number'
  },
  welcome: {
    header: 'Welcome',
    title: 'Welcome!',
    description: 'Fill up required fields, please'
  },
  intercom: {
    header: 'Link Intercom',
    title: 'Let\'s link your first Intercom',
    description: 'Enter the 6 digit code of your boon intercom'
  },
  address: {
    header: 'Address Confirm',
    title: 'Is this your address?'
  },
  automation: {
    header: 'Automation'
  },
  cameras: {
    header: 'Cameras',
    empty: {
      title: 'No Cameras',
      description: 'You have no matching devices at  this time'
    }
  },
  devices: {
    header: 'Devices'
  },
  history: {
    header: 'History',
    empty: {
      title: 'No Activity',
      description: 'You have no activity logs from your devices'
    }
  },
  notifications: {
    header: 'Notifications',
    empty: {
      title: 'No Notifications',
      description: 'You have no notifications yet'
    }
  },
  locks: {
    header: 'Locks',
    empty: {
      title: 'No Locks',
      description: 'You have no matching devices at  this time'
    },
    delete: {
      title: 'Delete Device',
      description: 'Please confirm, you want to delete device?',
    }
  },
  people: {
    header: 'People',
    empty: {
      title: 'No People',
      description: 'You have no added devices at this time'
    },
    add: {
      title: 'Add New Member',
      description: 'Please press generate button'
    }
  },
  help: {
    header: 'Help & Support'
  },
  support: {
    header: 'Help & Support',
    title: 'Contact Us',
  },
  questions: {
    header: 'FAQ',
    title: 'Frequently Asked Questions',
  },
  security: {
    header: 'SecurityAlarm'
  },
  settings: {
    header: 'Settings'
  },
  profile: {
    header: 'Profile'
  },
  account: {
    header: 'Account'
  },
  payments: {
    header: 'Payments'
  },
  packages: {
    header: 'Packages',
    title: 'Select Your Plan',
    description: 'You can change current plan anytime',
    empty: {
      title: 'No Packages',
      description: 'You have no matching devices at  this time'
    },
    update: {
      title: 'Update Package',
      description: 'Are you sure, you wont to change to "%{package}" package'
    }
  },
  validations: {
    required: 'Field is required',
    invalidAlpha: 'Field must contain only alphabetical characters.',
    invalidNumeric: 'Field must contain only numeric characters.',
    invalidEmail: 'Field must contain valid email address.',
    invalidPhone: 'Field must contain valid phone number.',
  },
  profileMenu: {
    account: 'Account',
    packages: 'Packages',
    people: 'People',
    payments: 'Payment Methods',
    home: 'Home Management',
    history: 'History',
    invoices: 'My Invoices',
    settings: 'Settings',
    help: 'Help and Support',
    agreement: 'UserItem Agreement & Privacy Policy',
    signOut: 'Sign Out',
  }
};
