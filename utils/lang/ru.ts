export default {
    signUp: 'Register',
    signIn: 'Sign In',
    selectLanguage: 'Select Language',
    close: "CLOSE",
    search: "Search",
    agreeWith: "I agree with",
    termsAndConditions: 'terms and conditions',
    phone: "Login number",
    register: {
        title: 'Register',
        description: 'Fill up your phone number, please?'
    },
    AutomationScreen: 'AutomationScreen',
    CamerasScreen: 'CamerasScreen',
    DevicesScreen: 'DevicesScreen',
    HistoryScreen: 'HistoryScreen',
    LocksScreen: 'LocksScreen',
    PeopleScreen: 'PeopleScreen',
    SecurityAlarmScreen: 'SecurityAlarmScreen',
    SettingsScreen: 'SettingsScreen',
};
