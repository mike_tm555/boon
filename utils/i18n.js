import I18n from "i18n-js";
import * as Localization from 'expo-localization';
import en from "./lang/en";
import am from "./lang/am";
import ru from "./lang/ru";
import Assets from "../constants/definitions/Assets";

function setI18nConfig(lang) {
    I18n.fallbacks = true;
    I18n.languages = {
        en: {
            name: 'English',
            key: 'en',
            translation: en,
            image: Assets.languages.enIcon
        },
        ru: {
            name: 'Russian',
            key: 'ru',
            translation: ru,
            image: Assets.languages.ruIcon
        },
        am: {
            name: 'Armenian',
            key: 'am',
            translation: am,
            image: Assets.languages.amIcon
        }
    };
    I18n.translations = {};
    Object.keys(I18n.languages).forEach(key => {
        const language = I18n.languages[key];
        I18n.translations[language.key] = language.translation
    });

    I18n.locale = lang || Localization.locale;
}

setI18nConfig();

I18n.changeLanguage = (locale) => {
    setI18nConfig(locale);
};

export default I18n;
