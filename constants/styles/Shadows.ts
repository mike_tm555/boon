export default {
    default: {
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOffset: {
            width: 3,
            height: 3,
        },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 5,
        zIndex: 1
    },
    mild: {
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOffset: {
            width: 3,
            height: 3,
        },
        shadowOpacity: 0.7,
        shadowRadius: 5,
        elevation: 3,
        zIndex: 1
    },
    dark: {
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 3,
        },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        elevation: 10,
        zIndex: 1
    }
}
