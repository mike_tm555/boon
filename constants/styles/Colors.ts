const tintColorLight = "#DA0463";
const tintColorDark = "#aaaaaa";

export default {
  light: {
    screen: "#fff",
    primary: "#23999d",
    secondary: "#36C98D",
    main: ["#23999d", "#36C98D"],
    content: "#ffffff",
    wrapper: ["rgba(255, 255, 255, 0.35)", "#F2F2FD"],
    headerLayer: ["#23999d", "#36C98D"],
    headerIcon: "#fff",
    headerTitle: "#fff",
    field: "#000",
    error: "#EE3706",
    success: "#19de6c",
    note: "#36C98D",
    border: "#eee",
    separator: "#eee",
    indicator: "#23999d",
    focused: "rgba(58, 151, 212, 0.28)",
    overlay: "rgba(0, 0, 0, 0.7)",
    sidebar: "rgba(40, 49, 73, 0.98)",
    tooltip: "rgba(40,49,73,0.9)",
    label: "#888",
    text: "#404B69",
    lightText: "#fff",
    caption: "#777777",
    button: "#000",
    dot: "#ddd",
    activeDot: "#23999d",
    toast: {
      error: "#d74a49",
      success: "#5bc3a2",
    },
    actionButton: {
      bg: "#23999d",
      color: "#fff"
    },
    darkButton: {
      bg: "#000000",
      color: "#fff"
    },
    lightButton: {
      bg: "#fff",
      color: "#000"
    },
    simpleButton: {
      bg: "transparent",
      color: "#000"
    },
    disabledButton: {
      bg: "#ccc",
      color: "#000"
    },
    borderButton: {
      bg: "#fff",
      border: "#23999d",
      color: "#23999d"
    },
    squareButton: {
      bg: "#fff",
      border: "#eee",
      color: "#000"
    },
    draggable: "#888",
    background: "transparent",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
    logo: "/assets/images.logo-light.png",
  },
  dark: {
    screen: "#fff",
    primary: "#DA0463",
    secondary: "#AF007E",
    main: ["#AF007E", "#DA0463"],
    content: "#ffffff",
    wrapper: ["rgba(255, 255, 255, 0.35)", "#F2F2FD"],
    headerLayer: ["#AF007E", "#DA0463"],
    headerIcon: "#fff",
    headerTitle: "#fff",
    field: "#000",
    error: "#EE3706",
    success: "#19de6c",
    note: "#da2508",
    border: "#eee",
    separator: "#eee",
    indicator: "#AF007E",
    focused: "rgba(58, 151, 212, 0.28)",
    overlay: "rgba(0, 0, 0, 0.9)",
    sidebar: "rgba(40, 49, 73, 0.98)",
    tooltip: "rgba(40,49,73,0.9)",
    label: "#888",
    text: "#000",
    lightText: "#fff",
    caption: "#aaa",
    button: "#000",
    dot: "#ddd",
    activeDot: "#AF007E",
    toast: {
      error: "#d74a49",
      success: "#5bc3a2",
    },
    actionButton: {
      bg: "#AF007E",
      color: "#fff"
    },
    darkButton: {
      bg: "#000000",
      color: "#fff"
    },
    lightButton: {
      bg: "#fff",
      color: "#000"
    },
    simpleButton: {
      bg: "transparent",
      color: "#000"
    },
    disabledButton: {
      bg: "#ccc",
      color: "#000"
    },
    borderButton: {
      bg: "#eee",
      border: "#AF007E",
      color: "#ccc"
    },
    draggable: "#888",
    background: "transparent",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark,
    logo: "/assets/images.logo-light.png",
  },
} as any;
