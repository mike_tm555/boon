import useTheme from "../../hooks/useTheme";
import Shadows from "./Shadows";

const theme = useTheme();

export default {
    simple: {
        backgroundColor: theme.tooltip,
        borderRadius: 5,
        height: 'auto',
        ...Shadows.default,
    }
}
