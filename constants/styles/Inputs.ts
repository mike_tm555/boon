import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    field: {
        height: 40,
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        borderColor: theme.border,
        labelColor: theme.label,
        focused: theme.focused,
        lineHeight: 36,
        fontSize: 13,
        fontSizeLarge: 15,
        color: theme.field,
        errorColor: theme.error,
    },
    icon: {
        size: 22
    }
}
