import useTheme from "../../hooks/useTheme";

const theme = useTheme();

export default {
    p: {
        fontSize: 13,
        color: theme.text,
        lineHeight: 20,
    },
    caption: {
        fontSize: 13,
        color: theme.caption,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: theme.text,
        marginBottom: 10,
    },
    h1: {
        fontWeight: 'bold',
        fontSize: 22,
        color: theme.text,
        marginBottom: 10,
        textAlign: 'center',
    },
    h2: {
        fontWeight: 'bold',
        fontSize: 20,
        color: theme.text,
        marginBottom: 10,
        textAlign: 'center',
    },
    h3: {
        fontWeight: 'bold',
        fontSize: 18,
        color: theme.text,
        marginBottom: 10,
        textAlign: 'center',
    },
    h4: {
        fontWeight: 'bold',
        fontSize: 16,
        color: theme.text,
        marginBottom: 10,
        textAlign: 'center',
    },
    h5: {
        fontSize: 13,
        color: theme.text,
        lineHeight: 20,
        marginBottom: 10,
        textAlign: 'center',
    }
}
