import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const scrollSpace = 10;
const contentSpace = 20;
const wrapperSpace = 20;

export default {
  window: {
    width,
    height,
  },
  scroll: {
    space: scrollSpace
  },
  wrapper: {
    width: width - (2 * wrapperSpace),
    space: wrapperSpace,
  },
  content: {
    width: width - (2 * contentSpace - 2 * scrollSpace),
    space: contentSpace,
  },
  isSmallDevice: width < 375,
  listItem: {
    marginBottom: 15,
    borderRadius: 10,
    padding: 15,
  }
};
