//menu
import menuAutomation from '../../assets/images/menu/automation';
import menuCameras from '../../assets/images/menu/cameras';
import menuHistory from '../../assets/images/menu/history';
import menuLocks from '../../assets/images/menu/locks';
import menuSecurity from '../../assets/images/menu/security';
import menuPeople from '../../assets/images/menu/people';
import menuDevices from '../../assets/images/menu/devices';
import menuSettings from '../../assets/images/menu/settings';
import menuPayments from '../../assets/images/menu/payments';
//-------
//profile
import profileAccount from '../../assets/images/profile/account';
import profilePackages from '../../assets/images/profile/services';
import profilePeople from '../../assets/images/profile/people';
import profilePayments from '../../assets/images/profile/payments';
import profileHome from '../../assets/images/profile/home';
import profileHistory from '../../assets/images/profile/history';
import profileInvoices from '../../assets/images/profile/invoices';
import profileSettings from '../../assets/images/profile/settings';
import profileHelp from '../../assets/images/profile/help';
import profileAgreement from '../../assets/images/profile/agreement';
import profileSignOut from '../../assets/images/profile/signOut';
//-------
//devices
import devicesAddress from '../../assets/images/devices/address';
import devicesIntercom from '../../assets/images/devices/intercom';
import devicesApartment from '../../assets/images/devices/apartment';
//-------
//people
import emptyPeople from '../../assets/images/people/empty-people';
import peopleAddress from '../../assets/images/people/address';
//-------
//notifications
import emptyNotifications from '../../assets/images/notifications/empty-notifications';
//-------
//history
import emptyHistory from '../../assets/images/history/empty-history';
//-------
//locks
import emptyLocks from '../../assets/images/locks/empty-locks';
import locked from "../../assets/images/locks/locked";
import unlocked from "../../assets/images/locks/unlocked";
import locksIntercom from '../../assets/images/locks/intercom';
//-------

//patterns
import logoLight from '../../assets/images/patterns/logo-light';
import logoDark from '../../assets/images/patterns/logo-dark';
import arc from "../../assets/images/patterns/arc";
//--------

// autorize
import smsIcon from '../../assets/images/icons/sms';
//---------

//icons
import dotsIcon from "../../assets/images/icons/dots";
import messagesIcon from "../../assets/images/icons/messages";
import avatarIcon from "../../assets/images/icons/avatar";
//---------

export default {
  languages: {
    enIcon: require('../../assets/images/languages/en.png'),
    amIcon: require('../../assets/images/languages/am.png'),
    ruIcon: require('../../assets/images/languages/ru.png'),
  },
  icons: {
    avatar: require('../../assets/images/icons/avatar.png'),
    generator: require('../../assets/images/icons/generator.png'),
    messages: messagesIcon,
    dots: dotsIcon,
    intercom: locksIntercom
  },
  authorize: {
    sms: smsIcon,
  },
  patterns: {
    arc: arc,
    logoLight: logoLight,
    logoDark: logoDark,
  },
  cameras: {
    emptyCameras: emptyLocks,
  },
  locks: {
    locked: locked,
    unlocked: unlocked,
    emptyLocks: emptyLocks,
    icons: {
      intercom: locksIntercom
    }
  },
  people: {
    emptyPeople: emptyPeople,
    icons: {
      address: peopleAddress
    }
  },
  devices: {
    intercom: devicesIntercom,
    address: devicesAddress,
    apartment: devicesApartment,
  },
  notifications: {
    emptyNotifications: emptyNotifications,
  },
  history: {
    emptyHistory: emptyHistory,
    screenshot: require('../../assets/images/history/screenshot.png'),
  },
  profile: {
    account: profileAccount,
    packages: profilePackages,
    people: profilePeople,
    payments: profilePayments,
    home: profileHome,
    history: profileHistory,
    invoices: profileInvoices,
    settings: profileSettings,
    help: profileHelp,
    agreement: profileAgreement,
    signOut: profileSignOut,
  },
  menu: {
    cameras: menuCameras,
    locks: menuLocks,
    automation: menuAutomation,
    history: menuHistory,
    people: menuPeople,
    security: menuSecurity,
    devices: menuDevices,
    settings: menuSettings,
    payments: menuPayments,
  },
  sounds: {
    call: require('../../assets/sounds/call.mp3')
  }
};
