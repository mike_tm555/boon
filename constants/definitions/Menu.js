import Assets from './Assets';
import I18n from '../../utils/i18n';
import Routing from "../../navigation/Routing";

export default {
    locks: {
        title: I18n.t("locks.header"),
        icon: `${Assets.menu.locks}`,
        visible: true,
        navigation: {
            route: Routing.locks.name,
            screen: Routing.locks.screens.list.name
        }
    },
    cameras: {
        title: I18n.t("cameras.header"),
        icon: `${Assets.menu.cameras}`,
        visible: true,
        navigation: {
            route: Routing.cameras.name,
            screen: Routing.cameras.screens.list.name
        }
    },
    security: {
        title: I18n.t("security.header"),
        icon: `${Assets.menu.security}`,
        visible: false,
        navigation: {
            route: '',
        }
    },
    devices: {
        title: I18n.t("devices.header"),
        icon: `${Assets.menu.devices}`,
        visible: false,
        navigation: {
            route: '',
        }
    },
    people: {
        title: I18n.t("people.header"),
        icon: `${Assets.menu.people}`,
        visible: false,
        navigation: {
            route: Routing.people.name,
        }
    },
    history: {
        title: I18n.t("history.header"),
        icon: `${Assets.menu.history}`,
        visible: true,
        navigation: {
            route: Routing.history.name,
            screen: Routing.history.screens.list.name
        }
    },
    automation: {
        title: I18n.t("automation.header"),
        icon: `${Assets.menu.automation}`,
        visible: false,
        navigation: {
            route: '',
        }
    },
    payments: {
        title: I18n.t("payments.header"),
        icon: `${Assets.menu.payments}`,
        visible: false,
        navigation: {
            route: Routing.payments.name,
        }
    },
    settings: {
        title: I18n.t("settings.header"),
        icon: `${Assets.menu.settings}`,
        visible: true,
        navigation: {
            route: Routing.settings.name,
        }
    },
}
