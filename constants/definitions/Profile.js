import Assets from './Assets';
import I18n from '../../utils/i18n';
import Routing from "../../navigation/Routing";

export default {
    section1: {
        account: {
            title: I18n.t('profileMenu.account'),
            icon: `${Assets.profile.account}`,
            visible: true,
            navigation: {
                route: Routing.profile.name,
                screen: Routing.profile.screens.account.name
            }
        },
        packages: {
            title: I18n.t('profileMenu.packages'),
            icon: `${Assets.profile.packages}`,
            visible: true,
            navigation: {
                route: Routing.packages.name,
                screen: Routing.packages.screens.addresses.name
            }
        },
        people: {
            title: I18n.t('profileMenu.people'),
            icon: `${Assets.profile.people}`,
            visible: true,
            navigation: {
                route: Routing.people.name,
                screen: Routing.people.screens.addresses.name
            }
        },
        payments: {
            title: I18n.t('profileMenu.payments'),
            icon: `${Assets.profile.payments}`,
            visible: true,
            navigation: {
                route: Routing.payments.name
            }
        },
        home: {
            title: I18n.t('profileMenu.home'),
            icon: `${Assets.profile.home}`,
            visible: false,
            navigation: ''
        },
        history: {
            title: I18n.t('profileMenu.history'),
            icon: `${Assets.profile.history}`,
            visible: false,
            navigation: {
                route: Routing.history.name,
                screen: Routing.history.screens.list.name
            }
        },
        invoices: {
            title: I18n.t('profileMenu.invoices'),
            icon: `${Assets.profile.invoices}`,
            visible: false,
            navigation: ''
        },
        settings: {
            title: I18n.t('profileMenu.settings'),
            icon: `${Assets.profile.settings}`,
            visible: true,
            navigation: {
                route: Routing.settings.name
            }
        },
    },
    section2: {
        help: {
            title: I18n.t('profileMenu.help'),
            icon: `${Assets.profile.help}`,
            visible: true,
            navigation: {
                route: Routing.help.name,
                screen: Routing.help.screens.support.name
            }
        },
        agreement: {
            title: I18n.t('profileMenu.agreement'),
            icon: `${Assets.profile.agreement}`,
            visible: true,
            navigation: ''
        },
        signOut: {
            title: I18n.t('profileMenu.signOut'),
            icon: `${Assets.profile.signOut}`,
            visible: true,
            navigation: {
                action: 'signOut'
            }
        }
    }
}
