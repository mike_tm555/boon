export const AUTHORIZED = 'authorized';
export const NOT_AUTHORIZED = 'notAuthorized';
export const REGISTERED = 'registered';
export const NOT_REGISTERED = 'notRegistered';
