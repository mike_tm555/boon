import I18n from '../../utils/i18n';

export const SIGN_UP_FORM = {
    fields: {
        phone: {
            label: I18n.t('phone'),
            component: 'phone',
            type: 'field',
            name: 'phone',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'phone'
                }
            ],
            params: {
                defaultCountry: "AM",
            }
        },
        terms: {
            component: 'checkbox',
            type: 'field',
            name: 'terms',
            action: 'change',
            validation: [
                {
                    type: 'checked'
                }
            ],
            params: {
                title: `${I18n.t('agreeWith')} ${I18n.t('termsAndConditions')}`,
                center: true,
            }
        }
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('next')
            }
        }
    ]
};

export const AUTHORIZE_FORM = {
    fields: {
        phone: {
            label: I18n.t('phone'),
            component: 'phone',
            type: 'field',
            name: 'phone',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'phone'
                }
            ],
            params: {
                defaultCountry: "AM",
            }
        },
        terms: {
            component: 'checkbox',
            type: 'field',
            name: 'terms',
            action: 'change',
            validation: [
                {
                    type: 'checked'
                }
            ],
            params: {
                title: `${I18n.t('agreeWith')} ${I18n.t('termsAndConditions')}`,
                center: true,
            }
        }
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('next')
            }
        }
    ]
};

export const SIGN_IN_FORM = {
    fields: {
        phone: {
            label: I18n.t('phone'),
            component: 'phone',
            type: 'field',
            name: 'phone',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'phone'
                }
            ],
            params: {
                defaultCountry: "AM",
            }
        },
        terms: {
            component: 'checkbox',
            type: 'field',
            name: 'terms',
            action: 'change',
            validation: [
                {
                    type: 'checked'
                }
            ],
            params: {
                title: `${I18n.t('agreeWith')} ${I18n.t('termsAndConditions')}`,
                center: true,
            }
        }
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('next')
            }
        }
    ]
};

export const ACTIVATE_FORM = {
    fields: {
        code: {
            component: 'code',
            type: 'field',
            name: 'code',
            action: 'change',
            validation: [
                {
                    type: 'required'
                }
            ],
            params: {
                length: 5,
            }
        },
        resend: {
            component: 'resend',
            type: 'field',
            name: 'resend',
            action: 'resend',
            params: {
                title: I18n.t('resendCode'),
                path: 'phone'
            }
        }
    },
    buttons: [
        {
            name: 'submitSignUp',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('next')
            }
        }
    ]
};

export const WELCOME_FORM = {
    fields: {
        name: {
            component: 'name',
            type: 'field',
            name: 'name',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'alpha'
                },
            ],
            params: {
                placeholder: I18n.t('name')
            }
        },
        email: {
            component: 'email',
            type: 'field',
            name: 'email',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'email'
                }
            ],
            params: {
                placeholder: I18n.t('email')
            }
        },
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            validation: [
                {
                    type: 'required'
                }
            ],
            params: {
                type: 'action',
                title: I18n.t('next')
            }
        }
    ]
};

export const ACCOUNT_FORM = {
    fields: {
        name: {
            component: 'name',
            type: 'field',
            name: 'name',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'alpha'
                },
            ],
            params: {
                placeholder: I18n.t('name')
            }
        },
        email: {
            component: 'email',
            type: 'field',
            name: 'email',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'email'
                }
            ],
            params: {
                placeholder: I18n.t('email')
            }
        },
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            validation: [
                {
                    type: 'required'
                }
            ],
            params: {
                type: 'action',
                title: I18n.t('save')
            }
        }
    ]
};

export const INTERCOM_NAME_FORM = {
    fields: {
        name: {
            component: 'name',
            type: 'field',
            name: 'name',
            action: 'change',
            params: {
                value: ''
            }
        }
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('save')
            }
        }
    ]
};

export const LINK_DEVICE_FORM = {
    fields: {
        code: {
            component: 'code',
            type: 'field',
            name: 'code',
            action: 'change',
            params: {
                length: 6,
            }
        },
        help: {
            component: 'help',
            type: 'field',
            name: 'help',
            action: 'help',
            params: {
                title: I18n.t('intercomHelp'),
            }
        }
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('next')
            }
        }
    ]
};

export const ADDRESS_CONFIRM_FORM = {
    fields: {
        city: {
            label: I18n.t('city'),
            component: 'name',
            type: 'field',
            name: 'city',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'alpha'
                },
            ],
            params: {

            }
        },
        street: {
            label: I18n.t('street'),
            component: 'name',
            type: 'field',
            name: 'street',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'alpha'
                },
            ],
            params: {

            }
        },
        building: {
            label: I18n.t('building'),
            component: 'number',
            type: 'field',
            name: 'building',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'numeric'
                },
            ],
            params: {

            }
        },
        flatNumber: {
            label: I18n.t('flat'),
            component: 'number',
            type: 'field',
            name: 'flatNumber',
            action: 'change',
            validation: [
                {
                    type: 'required'
                },
                {
                    type: 'numeric'
                },
            ],
            params: {

            }
        },
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('yes')
            }
        },
        {
            name: 'cancel',
            action: 'cancel',
            params: {
                type: 'light',
                title: I18n.t('no')
            }
        }
    ]
};

export const SETTINGS_FORM = {
    fields: {
        notifications: {
            component: 'switch',
            type: 'field',
            name: 'notifications',
            action: 'change',
            params: {
                title: I18n.t('enableNotifications'),
                circleSize: 20,
            }
        },
        language: {
            component: 'language',
            type: 'field',
            name: 'language',
            action: 'change',
            params: {
                title: I18n.t('language'),
                value: I18n.locale.split('-')[0]
            }
        },
    }
};

export const SUPPORT_FORM = {
    fields: {
        message: {
            label: I18n.t('writeMessage'),
            component: 'text',
            type: 'field',
            name: 'message',
            action: 'change',
            validation: [
                {
                    type: 'required'
                }
            ],
            params: {
                title: I18n.t('beDescriptive'),
                circleSize: 20,
            }
        },
    },
    buttons: [
        {
            name: 'submit',
            action: 'submit',
            params: {
                type: 'action',
                title: I18n.t('sendMessage')
            }
        }
    ]
};
