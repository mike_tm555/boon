export default {
    data: [
        {
            id: '1',
            name: 'Basic',
            price: '֏ 750',
            image: require('../../assets/images/packages/basic.png'),
            active: true,
        },
        {
            id: '2',
            name: 'Premium',
            price: '֏ 1000',
            image: require('../../assets/images/packages/premium.png'),
            active: false,
        },
    ]
}
