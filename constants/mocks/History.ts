export default {
    data: [
        {
            id: '1',
            type: 'intercom',
            date: '12/10/2020',
            data: {
                action: 'answered',
                name: 'Intercom Home',
                address: {
                    id: '2',
                    city: 'Yerevan',
                    street: 'Abovyan',
                    building: '12',
                    apartmentNumber: '31'
                },
                date: '12/10/2020',
                time: '12:00',
                user: {
                    name: 'Poghos Poghosyan'
                }
            }
        },
        {
            id: '2',
            type: 'intercom',
            date: '14/10/2020',
            data: {
                action: 'answered',
                name: 'Intercom Home',
                address: {
                    id: '2',
                    city: 'Yerevan',
                    street: 'Abovyan',
                    building: '12',
                    apartmentNumber: '31'
                },
                date: '12/10/2020',
                time: '12:00',
                user: {
                    name: 'Poghos Poghosyan'
                }
            }
        },
        {
            id: '3',
            type: 'intercom',
            date: '13/10/2020',
            data: {
                action: 'answered',
                name: 'Intercom Home',
                address: {
                    id: '2',
                    city: 'Yerevan',
                    street: 'Abovyan',
                    building: '12',
                    apartmentNumber: '31'
                },
                date: '14/10/2020',
                time: '13:00',
                user: {
                    name: 'Poghos Poghosyan'
                }
            }
        },
        {
            id: '4',
            type: 'intercom',
            date: '12/10/2020',
            data: {
                action: 'declined',
                name: 'Intercom Home',
                address: {
                    id: '2',
                    city: 'Yerevan',
                    street: 'Abovyan',
                    building: '12',
                    apartmentNumber: '31'
                },
                date: '12/10/2020',
                time: '12:00',
                user: {
                    name: 'Poghos Poghosyan'
                }
            }
        },
    ]
}
