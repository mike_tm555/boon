export default {
    data: [
        {
            id: '1',
            address: {
                id: '2',
                city: 'Yerevan',
                street: 'Abovyan',
                building: '12',
                apartmentNumber: '31'
            },
            name: 'Jain',
            email: 'jain@gmail.com',
            phoneNumber: '+374 77  878 349',
            avatarUrl: null,
            isAdmin: false,
            active: true,
        },
        {
            id: '2',
            address: {
                id: '2',
                city: 'Yerevan',
                street: 'Abovyan',
                building: '12',
                apartmentNumber: '31'
            },
            name: 'Jain',
            email: 'jain@gmail.com',
            phoneNumber: '+374 77  878 349',
            avatarUrl: null,
            isAdmin: false,
            active: true,
        },
        {
            id: '3',
            address: {
                id: '2',
                city: 'Yerevan',
                street: 'Abovyan',
                building: '12',
                apartmentNumber: '31'
            },
            name: 'Jain',
            email: 'jain@gmail.com',
            phoneNumber: '+374 77  878 349',
            avatarUrl: null,
            isAdmin: false,
            active: true,
        },
        {
            id: '4',
            address: {
                id: '1',
                city: 'Yerevan',
                street: 'Saryan',
                building: '15',
                apartmentNumber: '31',
            },
            name: 'Jain',
            email: 'jain@gmail.com',
            phoneNumber: '+374 77  878 349',
            avatarUrl: null,
            isAdmin: false,
            active: true,
        }
    ]
}
