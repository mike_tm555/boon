export default {
    data: [
        {
            address: {
                id: '2',
                city: 'Yerevan',
                street: 'Abovyan',
                building: '12',
            },
            id: '1',
            type: 'intercom',
            name: 'Intercom Door',
        }
    ]
}
