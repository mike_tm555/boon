export default {
    data: {
        address: 'Yerevan, Saryan st., apt. 12',
        phone: '+374 55 555 555',
        email: 'info@boon.am',
        questions: [
            {
                id: '1',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            },
            {
                id: '2',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            },
            {
                id: '3',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            },
            {
                id: '4',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            },
            {
                id: '5',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            },
            {
                id: '6',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            },
            {
                id: '7',
                title: 'Text Title',
                answer: 'Answer text Answer text Answer text Answer text Answer text Answer text Answer text  Answer text  Answer text Answer text  Answer text Answer text Answer text',
            }
        ],
        tips: [
            {
                url: 'https://www.google.com/search?q=slider%20images&tbm=isch&hl=en&hl=en&tbs=isz:m&sa=X&ved=0CAMQpwVqFwoTCPCs0uf2vewCFQAAAAAdAAAAABAI&biw=1660&bih=837#imgrc=MAQfIXraT5UbaM',
                image: 'https://assets.website-files.com/5d2e14c399a69024ed417475/5efa6ef7acf4812a4ac0c809_5d9c50a459eb4750cdd52e3b_intercom%2520ebook.png',
            },
            {
                url: 'https://www.google.com/search?q=slider%20images&tbm=isch&hl=en&hl=en&tbs=isz:m&sa=X&ved=0CAMQpwVqFwoTCPCs0uf2vewCFQAAAAAdAAAAABAI&biw=1660&bih=837#imgrc=MAQfIXraT5UbaM',
                image: 'https://assets.website-files.com/5d2e14c399a69024ed417475/5efa6ef7acf4812a4ac0c809_5d9c50a459eb4750cdd52e3b_intercom%2520ebook.png',
            },
            {
                url: 'https://www.google.com/search?q=slider%20images&tbm=isch&hl=en&hl=en&tbs=isz:m&sa=X&ved=0CAMQpwVqFwoTCPCs0uf2vewCFQAAAAAdAAAAABAI&biw=1660&bih=837#imgrc=MAQfIXraT5UbaM',
                image: 'https://assets.website-files.com/5d2e14c399a69024ed417475/5efa6ef7acf4812a4ac0c809_5d9c50a459eb4750cdd52e3b_intercom%2520ebook.png',
            }
        ]
    }
};
